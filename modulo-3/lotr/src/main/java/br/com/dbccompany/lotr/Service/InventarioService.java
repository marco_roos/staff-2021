package br.com.dbccompany.lotr.Service;
import br.com.dbccompany.lotr.DTO.InventarioDTO;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.Inventario_x_Item;
import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import br.com.dbccompany.lotr.LotrApplication;
import br.com.dbccompany.lotr.Repository.InventarioRepository;
import br.com.dbccompany.lotr.Tools.Exceptions.NotFoundException_Inventario;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class InventarioService {

    private Logger logger = LoggerFactory.getLogger( LotrApplication.class );

    @Autowired
    private InventarioRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public InventarioDTO salvar(InventarioEntity inventario ){
        InventarioEntity novoInventario = this.salvarEEditar(inventario);
        return new InventarioDTO(inventario);
    }

    @Transactional(rollbackFor = Exception.class)
    public InventarioDTO editar ( InventarioEntity inventario, Integer id) throws NotFoundException_Inventario
    {
        try {
            inventario.setId(id);
            InventarioEntity novoInventario = this.salvarEEditar(inventario);
            return new InventarioDTO(inventario);
        } catch( Exception e ) {
            logger.info( e.getMessage() );
            throw new NotFoundException_Inventario();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    protected InventarioEntity salvarEEditar(InventarioEntity inventario){
        return repository.save (inventario);
    }

    private List<InventarioDTO> listaEntityParaDBO(List<InventarioEntity> listaEntity){
        List<InventarioDTO> inventariosDTO = new ArrayList<>();
        for (InventarioEntity inventario : listaEntity){
            inventariosDTO.add(new InventarioDTO(inventario));
        }
        return inventariosDTO;
    }

    public List<InventarioDTO> buscarTodos(){
        return listaEntityParaDBO(repository.findAll());
    }

    public InventarioDTO buscarPorId(Integer id) throws NotFoundException_Inventario {
        try {
            return new InventarioDTO(repository.findById(id).get());
        } catch ( Exception e ) {
            logger.info( e.getMessage() );
            throw new NotFoundException_Inventario();
        }
    }

    public List<InventarioDTO> buscarTodosPorId(List<Integer> id){
        return listaEntityParaDBO((List<InventarioEntity>) repository.findAllById(id));
    }

    public InventarioDTO buscarPorPersonagem(PersonagemEntity personagem){
        return new InventarioDTO(repository.findByPersonagem(personagem));
    }

    public List<InventarioDTO> buscarPorPersonagemIn (List<PersonagemEntity> personagens){
        return listaEntityParaDBO(repository.findByPersonagemIn(personagens));
    }

    public List<InventarioDTO> buscarPorInventarioItem( Inventario_x_Item inventarioItem){
        return listaEntityParaDBO(repository.findAllByInventarioItem(inventarioItem));
    }
}
