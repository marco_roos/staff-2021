package br.com.dbccompany.lotr.Tools.Exceptions;

public class ElfoException extends Exception {

    public ElfoException( String errorMessage ) {
        super( errorMessage );
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }

}
