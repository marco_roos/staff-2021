package br.com.dbccompany.lotr.Security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Entity
public class UserEntity implements Serializable, UserDetails {

    @Id
    @SequenceGenerator( name = "USUARIO_SEQ", sequenceName = "USUARIO_SEQ" )
    @GeneratedValue( generator = "USUARIO_SEQ", strategy = GenerationType.SEQUENCE)
    private long id;

    @Column(  unique = true, nullable = false )
    private String username;
    @Column( nullable = false )
    private String password;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) { this.password = password; }

    @Override
    public String getPassword() { return password; }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

}
