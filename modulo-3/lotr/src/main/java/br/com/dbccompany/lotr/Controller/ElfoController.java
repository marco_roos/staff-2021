package br.com.dbccompany.lotr.Controller;
import br.com.dbccompany.lotr.DTO.ElfoDTO;
import br.com.dbccompany.lotr.Entity.ElfoEntity;
import br.com.dbccompany.lotr.Enum.StatusEnum;
import br.com.dbccompany.lotr.Service.ElfoService;
import br.com.dbccompany.lotr.Tools.Exceptions.NotFoundException_Elfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/api/elfo")
public class ElfoController {

    @Autowired
    private ElfoService service;

    @GetMapping(value = "/salvar/{nome}")
    @ResponseBody
    public ElfoDTO salvarElfo (@PathVariable String nome){
        ElfoEntity elfo = new ElfoEntity(nome);
        return service.salvarElfo(elfo);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<Object> editarElfo (@RequestBody ElfoDTO elfo, @PathVariable Integer id){
        try{
            return new ResponseEntity<>( service.editarElfo(elfo.converter(), id), HttpStatus.ACCEPTED );
        } catch( NotFoundException_Elfo e ) {
            System.err.println( e.getMessage() );
            return new ResponseEntity<>( e.getMessage(), HttpStatus.NOT_FOUND );
        }
    }

    @GetMapping(value = "/")
    @ResponseBody
    public List<ElfoDTO> buscarTodosElfos(){
        return service.findAllElfo();
    }

    @GetMapping(value = "/{nome}")
    @ResponseBody
    public ElfoDTO buscarElfoPorNome (@PathVariable String nome){
        return service.findElfoByNome(nome);
    }

    @GetMapping(value = "/{experiencia}")
    @ResponseBody
    public ElfoDTO buscarElfoPorExperiencia (@PathVariable Integer experiencia){
        return service.findElfoByExperiencia(experiencia);
    }

    @GetMapping(value = "/buscarTodos/{experiencia}")
    @ResponseBody
    public List<ElfoDTO> buscarTodosElfosPorExperiencia (@PathVariable Integer experiencia){
        return service.findAllElfoByExperiencia(experiencia);
    }

    @GetMapping(value = "/{vida}")
    @ResponseBody
    public ElfoDTO buscarElfoPorVida (@PathVariable Double vida){
        return service.findElfoByVida(vida);
    }

    @GetMapping(value = "/buscarTodos/{vida}")
    @ResponseBody
    public List<ElfoDTO> buscarTodosElfosPorVida (@PathVariable Double vida){
        return service.findAllElfoByVida(vida);
    }

    @GetMapping(value = "/qntDano/{qntDano}")
    @ResponseBody
    public ElfoDTO buscarElfoPorQntDano (@PathVariable Double qntDano){
        return service.findElfoBydanoRecebido(qntDano);
    }

    @GetMapping(value = "/buscarTodos/qntDano/{qntDano}")
    @ResponseBody
    public List<ElfoDTO> buscarTodosElfosPorQntDano (@PathVariable Double qntDano){
        return service.findAllElfoByDanoRecebido(qntDano);
    }

    @GetMapping(value = "/qntExpAtaque/{qntExpAtaque}")
    @ResponseBody
    public ElfoDTO buscarElfoPorQntExperienciaPorAtaque (@PathVariable Integer qntExpAtaque){
        return service.findElfoByExpPorAtaq(qntExpAtaque);
    }

    @GetMapping(value = "/buscarTodos/qntExpAtaque/{qntExpAtaque}")
    @ResponseBody
    public List<ElfoDTO> buscarTodosElfosPorQntExperienciaPorAtaque (@PathVariable Integer qntExpAtaque){
        return service.findAllElfoByExpPorAtaq(qntExpAtaque);
    }

    @GetMapping(value = "/{status}")
    @ResponseBody
    public ElfoDTO buscarElfoPorStatus (@PathVariable StatusEnum status){
        return service.findElfoByStatus(status);
    }

    @GetMapping(value = "/buscarTodos/{status}")
    @ResponseBody
    public List<ElfoDTO> buscarTodosElfosPorStatus (@PathVariable StatusEnum status){
        return service.findAllElfoByStatus(status);
    }
}
