package br.com.dbccompany.lotr.Repository;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.Inventario_x_Item;
import br.com.dbccompany.lotr.Entity.Inventario_x_ItemId;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;

@Repository
public interface Inventario_x_ItemRepository extends CrudRepository<Inventario_x_Item, Inventario_x_ItemId> {
    List<Inventario_x_Item> findAll();
    Optional<Inventario_x_Item> findById( Inventario_x_ItemId id);

    Inventario_x_Item findByQuantidade (Integer quantidade);
    List<Inventario_x_Item> findAllByQuantidade (Integer quantidade);
    List<Inventario_x_Item> findAllByQuantidadeIn (List<Integer> quantidades);

    Inventario_x_Item findByInventario (InventarioEntity inventario);
    List<Inventario_x_Item> findAllByInventario (InventarioEntity inventario);
    List<Inventario_x_Item> findByInventarioIn (List<InventarioEntity> inventarios);

    Inventario_x_Item findByItem (ItemEntity item);
    List<Inventario_x_Item> findAllByItem (ItemEntity item);
    List<Inventario_x_Item> findByItemIn (List<ItemEntity> itens);
}
