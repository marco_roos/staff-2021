package br.com.dbccompany.lotr.Controller;
import br.com.dbccompany.lotr.DTO.InventarioDTO;
import br.com.dbccompany.lotr.DTO.Inventario_x_ItemDTO;
import br.com.dbccompany.lotr.DTO.ItemDTO;
import br.com.dbccompany.lotr.Entity.Inventario_x_ItemId;
import br.com.dbccompany.lotr.Service.Inventario_x_ItemService;
import br.com.dbccompany.lotr.Tools.Exceptions.NotFoundException_Inventario_x_Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/api/inventarioItem")
public class Inventario_X_ItemController {

    @Autowired
    private Inventario_x_ItemService service;


    @PostMapping(value = "/salvar")
    @ResponseBody
    public Inventario_x_ItemDTO salvarInventarioItem(@RequestBody Inventario_x_ItemDTO inventarioItem){
        return service.salvar(inventarioItem.converter());
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<Object> editarInventarioItem (@RequestBody Inventario_x_ItemDTO inventarioItem, @PathVariable Inventario_x_ItemId id) {
        try {
            return new ResponseEntity<>( service.editar(inventarioItem.converter(), id), HttpStatus.ACCEPTED );
        } catch( NotFoundException_Inventario_x_Item e ) {
            return new ResponseEntity<>( e.getMessage(), HttpStatus.NOT_FOUND );
        }
    }

    @GetMapping(value = "/")
    @ResponseBody
    public List<Inventario_x_ItemDTO> buscarTodosInventarioItem (){
        return service.buscarTodos();
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public Inventario_x_ItemDTO buscarInventarioItemPorId (@PathVariable Inventario_x_ItemId id){
        return service.buscarPorId(id);
    }

    @GetMapping(value = "/{quantidade}")
    @ResponseBody
    public Inventario_x_ItemDTO buscarInventarioItemPorQuantidade(@PathVariable Integer quantidade){
        return service.buscarPorQuantidade(quantidade);
    }

    @GetMapping(value = "/buscarTodos/{quantidade}")
    @ResponseBody
    public List<Inventario_x_ItemDTO> buscarTodosInventarioItemPorQuantidade(@PathVariable Integer quantidade){
        return service.buscarTodosPorQuantidade(quantidade);
    }

    @PostMapping(value = "/buscarInventario")
    @ResponseBody
    public Inventario_x_ItemDTO buscarInventarioItemPorInventario(@RequestBody InventarioDTO inventario){
        return service.buscarPorInventario(inventario.converter());
    }

    @PostMapping(value = "/buscarTodosInventario")
    @ResponseBody
    public List<Inventario_x_ItemDTO> buscarTodosInventarioItemPorInventario(@RequestBody InventarioDTO inventario){
        return service.buscarTodosPorInventario(inventario.converter());
    }

    @PostMapping(value = "/buscarItem")
    @ResponseBody
    public Inventario_x_ItemDTO buscarInventarioItemPorItem(@RequestBody ItemDTO item){
        return service.buscarPorItem(item.converter());
    }

    @PostMapping(value = "/buscarTodosItem")
    @ResponseBody
    public List<Inventario_x_ItemDTO> buscarTodosInventarioItemPorItem(@RequestBody ItemDTO item){
        return service.buscarTodosPorItem(item.converter());
    }
}
