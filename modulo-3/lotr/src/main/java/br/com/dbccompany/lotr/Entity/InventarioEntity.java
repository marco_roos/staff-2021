package br.com.dbccompany.lotr.Entity;
import javax.persistence.*;
import java.util.List;

@Entity
public class InventarioEntity {

    @Id
    @SequenceGenerator( name = "INVENTARIO_SEQ", sequenceName = "INVENTARIO_SEQ" )
    @GeneratedValue( generator = "INVENTARIO_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @OneToMany( mappedBy = "inventario" )
    private List<Inventario_x_Item> inventarioItem;

    @OneToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "id_personagem" )
    private PersonagemEntity personagem;

    public int getId() {
        return id;
    }

    public void setId(Integer id_inventario) {
        this.id = id;
    }

    public List<Inventario_x_Item> getInventario_item() {
        return inventarioItem;
    }

    public void setInventario_item(List<Inventario_x_Item> inventarioItem) {
        this.inventarioItem = inventarioItem;
    }

    public PersonagemEntity getPersonagem() {
        return personagem;
    }

    public void setPersonagem(PersonagemEntity personagem) {
        this.personagem = personagem;
    }
}
