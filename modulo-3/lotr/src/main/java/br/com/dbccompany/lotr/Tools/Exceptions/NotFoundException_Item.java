package br.com.dbccompany.lotr.Tools.Exceptions;

public class NotFoundException_Item extends ItemException {

    public NotFoundException_Item()
    {
        super( "Item Not Found, should be here somewhere" );
    }

}
