package br.com.dbccompany.lotr.Repository;
import br.com.dbccompany.lotr.Entity.Inventario_x_Item;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;


@Repository
public interface ItemRepository extends CrudRepository<ItemEntity, Integer> {

    Optional<ItemEntity> findById( Integer id );
    List<ItemEntity> findByIdIn( List<Integer> id );
    ItemEntity findByDescricao(String descricao );
    List<ItemEntity> findAllByDescricao( String descricao );
    List<ItemEntity> findAllByDescricaoIn( List<String> descricao );
    ItemEntity findByinventarioItem( Inventario_x_Item inventarioItem );
    List<ItemEntity> findAllByinventarioItem( Inventario_x_Item inventarioItem );
}
