package br.com.dbccompany.lotr.Tools.Exceptions;

public class AnaoException extends Exception {

    public AnaoException( String errorMessage ) {
        super( errorMessage );
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
