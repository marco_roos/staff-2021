package br.com.dbccompany.lotr.Tools.Exceptions;

public class ItemException extends Exception{

    private String errorMessage;

    public ItemException(String errorMessage) {
        super( errorMessage );
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

}
