package br.com.dbccompany.lotr.Security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository repository;

    public List<UserEntity> BuscarTodosUsuarios( ) {
        return (List<UserEntity>) repository.findAll();
    }

    @Transactional( rollbackFor = Exception.class )
    public UserEntity salvar( UserEntity user ) {
        return repository.save(user);
    }

}
