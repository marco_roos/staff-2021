package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.Inventario_x_Item;
import br.com.dbccompany.lotr.Entity.ItemEntity;

import java.util.List;

public class ItemDTO {
    private Integer id;
    private String descricao;
    private List<Inventario_x_Item> inventarioItem;


    public ItemDTO( ItemEntity item ) {
        this.id = item.getId();
        this.descricao = item.getDescricao();
        this.inventarioItem = item.getInventarioItem();
    }

    public ItemEntity converter(){
        ItemEntity item = new ItemEntity();
        item.setId(this.id);
        item.setDescricao(this.descricao);
        item.setInventarioItem(this.inventarioItem);
        return item;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public List<Inventario_x_Item> getInventarioItem() {
        return inventarioItem;
    }

    public void setInventarioItem(List<Inventario_x_Item> inventarioItem) {
        this.inventarioItem = inventarioItem;
    }
}
