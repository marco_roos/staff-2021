package br.com.dbccompany.lotr.Tools.Exceptions;

public class Inventario_x_ItemException extends Exception {

    public Inventario_x_ItemException( String errorMessage ) {
        super( errorMessage );
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }

}
