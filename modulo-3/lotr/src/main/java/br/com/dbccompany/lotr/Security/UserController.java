package br.com.dbccompany.lotr.Security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/usuario" )
public class UserController {

    @Autowired
    private UserService service;

    @GetMapping( value = "/")
    @ResponseBody
    public List<UserEntity> BuscarTodosUsuarios() {
        return service.BuscarTodosUsuarios();
    }

    @PostMapping( value = "/salvar")
    @ResponseBody
    public UserEntity salvarUser( @RequestBody UserEntity usuario ) {
        return service.salvar( usuario );
    }
}
