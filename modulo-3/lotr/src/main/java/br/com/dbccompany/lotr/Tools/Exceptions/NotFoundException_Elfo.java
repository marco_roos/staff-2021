package br.com.dbccompany.lotr.Tools.Exceptions;

public class NotFoundException_Elfo extends ElfoException{

    public NotFoundException_Elfo(){
        super( "No Elf here, must have gone for a dwarfs hunting" );
    }

}
