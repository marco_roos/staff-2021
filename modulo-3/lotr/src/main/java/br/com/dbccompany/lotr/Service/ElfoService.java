package br.com.dbccompany.lotr.Service;
import br.com.dbccompany.lotr.DTO.ElfoDTO;
import br.com.dbccompany.lotr.Entity.ElfoEntity;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Enum.StatusEnum;
import br.com.dbccompany.lotr.LotrApplication;
import br.com.dbccompany.lotr.Repository.ElfoRepository;
import br.com.dbccompany.lotr.Tools.Exceptions.NotFoundException_Anao;
import br.com.dbccompany.lotr.Tools.Exceptions.NotFoundException_Elfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class ElfoService extends PersonagemService<ElfoRepository, ElfoEntity> {

    private Logger logger = LoggerFactory.getLogger( LotrApplication.class );

    @Transactional(rollbackFor = Exception.class)
    public ElfoDTO salvarElfo(ElfoEntity e) {
        return new ElfoDTO(super.salvar(e));
    }

    @Transactional(rollbackFor = Exception.class)
    public ElfoDTO editarElfo(ElfoEntity elfoEntity, Integer id) throws NotFoundException_Elfo  {
        try{
            elfoEntity.setId(id);
            return new ElfoDTO(super.editar( elfoEntity, id ));
        } catch( Exception e ) {
            logger.info( e.getMessage() );
            throw new NotFoundException_Elfo();
        }
    }

    public ElfoDTO buscarElfoPorId(Integer id) throws NotFoundException_Elfo {
        try {
            return new ElfoDTO(super.buscarPorId(id));
        } catch ( Exception e ) {
            logger.info( e.getMessage() );
            throw new NotFoundException_Elfo();
        }

    }

    public ElfoDTO findElfoByNome( String nome )
    {
        return new ElfoDTO(super.findByNome(nome));
    }

    public ElfoDTO findElfoByVida( Double vida )
    {
        return new ElfoDTO(super.findByVida(vida));
    }

    private List<ElfoDTO> listaEntityParaDBO(List<ElfoEntity> listaEntity){
        List<ElfoDTO> elfosDTO = new ArrayList<>();
        for(ElfoEntity elfo : listaEntity){
            elfosDTO.add(new ElfoDTO(elfo));
        }
        return elfosDTO;
    }

    public List<ElfoDTO> findAllElfo()
    {
        return listaEntityParaDBO( super.findAll() );
    }


    public List<ElfoDTO> findAllElfoByVida(Double vida )
    {
        return listaEntityParaDBO(super.findAllByVida(vida));
    }

    public ElfoDTO findElfoByExperiencia( Integer experiencia )
    {
        return new ElfoDTO(super.findByExperiencia(experiencia));
    }

    public List<ElfoDTO> findAllElfoByExperiencia( Integer experiencia )
    {
        return listaEntityParaDBO(super.findAllByExperiencia(experiencia));
    }

    public ElfoDTO findElfoBydanoRecebido( Double dano )
    {
        return new ElfoDTO(super.findBydanoRecebido(dano));
    }

    public List<ElfoDTO> findAllElfoByDanoRecebido( Double dano )
    {
        return listaEntityParaDBO(super.findAllByDanoRecebido(dano));
    }

    public ElfoDTO findElfoByExpPorAtaq( Integer experiencia )
    {
        return new ElfoDTO(super.findByExpPorAtaq(experiencia));
    }

    public List<ElfoDTO> findAllElfoByExpPorAtaq( Integer experiencia )
    {
        return listaEntityParaDBO(super.findAllByExpPorAtaq(experiencia));
    }

    public ElfoDTO findElfoByStatus( StatusEnum status )
    {
        return new ElfoDTO(super.findByStatus(status));
    }

    public List<ElfoDTO> findAllElfoByStatus( StatusEnum status )
    {
        return listaEntityParaDBO(super.findAllByStatus(status));
    }

    public ElfoDTO findElfoByInventario( InventarioEntity inventario )
    {
        return new ElfoDTO(super.findByInventario(inventario));
    }
}
