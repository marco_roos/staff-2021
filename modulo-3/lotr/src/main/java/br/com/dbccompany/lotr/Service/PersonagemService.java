package br.com.dbccompany.lotr.Service;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import br.com.dbccompany.lotr.Enum.StatusEnum;
import br.com.dbccompany.lotr.Repository.PersonagemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public abstract class PersonagemService<R extends PersonagemRepository<E, Integer>, E extends PersonagemEntity> {
    @Autowired
    private R repository;

    @Transactional(rollbackFor = Exception.class)
    public E salvar(E e ){
        return this.salvarEEditar(e);
    }
    @Transactional(rollbackFor = Exception.class)
    public E editar( E e, Integer id){
        e.setId(id);
        return this.salvarEEditar(e);
    }
    @Transactional(rollbackFor = Exception.class)
    protected E salvarEEditar(E e){
        return repository.save (e);
    }

    public E buscarPorId(Integer id) {
        Optional<E> personagem = repository.findById(id);
        if (personagem.isPresent()) {
            return repository.findById(id).get();
        }
        return null;
    }

    public List<E> findAll()
    {
        return (List<E>)repository.findAll();
    }

    public E findByNome( String nome )
    {
        return repository.findByNome(nome);
    }

    public E findByVida( Double vida )
    {
        return repository.findByVida(vida);
    }

    public List<E> findAllByVida( Double vida )
    {
        return repository.findAllByVida(vida);
    }

    public E findByExperiencia( Integer experiencia )
    {
        return repository.findByExperiencia(experiencia);
    }

    public List<E> findAllByExperiencia( Integer experiencia )
    {
        return repository.findAllByExperiencia(experiencia);
    }

    public E findBydanoRecebido( Double dano )
    {
        return repository.findBydanoRecebido(dano);
    }

    public List<E> findAllByDanoRecebido( Double dano )
    {
        return repository.findAllByDanoRecebido(dano);
    }

    public E findByExpPorAtaq( Integer experiencia )
    {
        return repository.findByExpPorAtaq(experiencia);
    }

    public List<E> findAllByExpPorAtaq( Integer experiencia )
    {
        return repository.findAllByExpPorAtaq(experiencia);
    }

    public E findByStatus( StatusEnum status )
    {
        return repository.findByStatus(status);
    }

    public List<E> findAllByStatus( StatusEnum status )
    {
        return repository.findAllByStatus(status);
    }

    public E findByInventario( InventarioEntity inventario )
    {
        return repository.findByInventario(inventario);
    }
}
