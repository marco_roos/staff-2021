package br.com.dbccompany.lotr.Controller;
import br.com.dbccompany.lotr.DTO.AnaoDTO;
import br.com.dbccompany.lotr.DTO.ElfoDTO;
import br.com.dbccompany.lotr.DTO.InventarioDTO;
import br.com.dbccompany.lotr.Service.InventarioService;
import br.com.dbccompany.lotr.Tools.Exceptions.InventarioException;
import br.com.dbccompany.lotr.Tools.Exceptions.NotFoundException_Inventario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@Controller
@RequestMapping(value = "api/inventario")
public class InventarioController {

    @Autowired
    private InventarioService service;

    @PostMapping(value = "/salvar")
    @ResponseBody
    public InventarioDTO salvarInventario(@RequestBody InventarioDTO inventario){
        return service.salvar(inventario.converter());
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<Object> editarInventario(@RequestBody InventarioDTO inventario, @PathVariable Integer id ){
        try{
            return new ResponseEntity<>( service.editar(inventario.converter(), id), HttpStatus.ACCEPTED );
        } catch( InventarioException e ) {
            System.err.println( e.getMessage() );
            return new ResponseEntity<>( e.getMessage(), HttpStatus.NOT_FOUND );
        }
    }

    @GetMapping(value = "/")
    @ResponseBody
    public List<InventarioDTO> buscarTodosInventarios(){
        return service.buscarTodos();
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<Object> buscarInventarioPorId(@PathVariable Integer id){
        try {
            return new ResponseEntity<>( service.buscarPorId(id), HttpStatus.ACCEPTED );
        } catch( NotFoundException_Inventario e ) {
            System.err.println( e.getMessage() );
            return new ResponseEntity<>( e.getMessage(), HttpStatus.NOT_FOUND );
        }
    }

    @PostMapping(value = "/buscarAnao")
    @ResponseBody
    public InventarioDTO buscarInventarioPorAnao (@RequestBody AnaoDTO anao){
        return service.buscarPorPersonagem(anao.converter());
    }

    @PostMapping(value = "/buscarElfo")
    @ResponseBody
    public InventarioDTO buscarInventarioPorElfo(@RequestBody ElfoDTO elfo){
        return service.buscarPorPersonagem(elfo.converter());
    }
}
