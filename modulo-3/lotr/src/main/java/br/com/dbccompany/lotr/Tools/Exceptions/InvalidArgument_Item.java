package br.com.dbccompany.lotr.Tools.Exceptions;

public class InvalidArgument_Item extends ItemException {

    public InvalidArgument_Item(){
        super( "Wrong type of data inserted on Item Edit" );
    }

}
