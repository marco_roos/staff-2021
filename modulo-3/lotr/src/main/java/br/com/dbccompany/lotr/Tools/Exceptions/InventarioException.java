package br.com.dbccompany.lotr.Tools.Exceptions;

public class InventarioException extends Exception {

    public InventarioException( String errorMessage ) {
        super( errorMessage );
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }

}
