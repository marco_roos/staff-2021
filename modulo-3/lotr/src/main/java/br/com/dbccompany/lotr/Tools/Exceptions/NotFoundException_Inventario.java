package br.com.dbccompany.lotr.Tools.Exceptions;

public class NotFoundException_Inventario extends InventarioException{

    public NotFoundException_Inventario(){
        super( "This inventario doesn't exist, maybe you should buy a less expensive bag?" );
    }

}
