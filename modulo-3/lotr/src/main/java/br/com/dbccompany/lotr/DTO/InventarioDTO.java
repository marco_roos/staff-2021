package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.Inventario_x_Item;
import br.com.dbccompany.lotr.Entity.Inventario_x_ItemId;
import br.com.dbccompany.lotr.Entity.PersonagemEntity;

import java.util.List;

public class InventarioDTO {
    private Integer id;
    private List<Inventario_x_Item> inventarioItem;
    private PersonagemEntity personagem;

    public InventarioDTO (InventarioEntity inventario){
        this.id = inventario.getId();
        this.inventarioItem = inventario.getInventario_item();
        this.personagem = inventario.getPersonagem();
    }

    public InventarioEntity converter(){
        InventarioEntity inventario = new InventarioEntity();
        inventario.setId(this.id);
        inventario.setInventario_item(this.inventarioItem);
        inventario.setPersonagem(this.personagem);
        return inventario;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Inventario_x_Item> getInventarioItem() {
        return inventarioItem;
    }

    public void setInventarioItem(List<Inventario_x_Item> inventarioItem) {
        this.inventarioItem = inventarioItem;
    }

    public PersonagemEntity getPersonagem() {
        return personagem;
    }

    public void setPersonagem(PersonagemEntity personagem) {
        this.personagem = personagem;
    }
}
