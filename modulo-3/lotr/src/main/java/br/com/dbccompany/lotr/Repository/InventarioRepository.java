package br.com.dbccompany.lotr.Repository;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.Inventario_x_Item;
import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;

@Repository
public interface InventarioRepository extends CrudRepository<InventarioEntity, Integer> {
    Optional<InventarioEntity> findById( Integer id );
    List<InventarioEntity> findAllById( Integer id );
    InventarioEntity findByPersonagem( PersonagemEntity personagem );
    List<InventarioEntity> findAll();
    List<InventarioEntity> findByPersonagemIn( List<PersonagemEntity> personagem );
    List<InventarioEntity> findAllByInventarioItem( Inventario_x_Item inventarioItem );
}
