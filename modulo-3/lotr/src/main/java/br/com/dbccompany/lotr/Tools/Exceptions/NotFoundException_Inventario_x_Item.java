package br.com.dbccompany.lotr.Tools.Exceptions;

public class NotFoundException_Inventario_x_Item extends Inventario_x_ItemException {

    public NotFoundException_Inventario_x_Item(){
        super( "This relation Item-Invetario does not exist, maybe you should look somewhere else" );
    }

}
