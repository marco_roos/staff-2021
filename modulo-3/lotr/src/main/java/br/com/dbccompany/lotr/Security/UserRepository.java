package br.com.dbccompany.lotr.Security;

import org.springframework.data.repository.CrudRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository  extends CrudRepository< UserEntity, Long> {
    Optional<UserEntity> findByUsername(String username);
}
