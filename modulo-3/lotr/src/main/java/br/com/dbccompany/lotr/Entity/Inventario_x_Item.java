package br.com.dbccompany.lotr.Entity;
import javax.persistence.*;

@Entity
public class Inventario_x_Item {
    @EmbeddedId
    private Inventario_x_ItemId id;
    private Integer quantidade;

    @ManyToOne( cascade = CascadeType.ALL)
    @MapsId("id_inventario")
    private InventarioEntity inventario;

    @ManyToOne( cascade = CascadeType.ALL)
    @MapsId("id_item")
    private ItemEntity item;

    public Inventario_x_ItemId getId() {
        return id;
    }

    public void setId(Inventario_x_ItemId id) {
        this.id = id;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public InventarioEntity getInventario() {
        return inventario;
    }

    public void setInventario(InventarioEntity inventario) {
        this.inventario = inventario;
    }

    public ItemEntity getItem() {
        return item;
    }

    public void setItem(ItemEntity item) {
        this.item = item;
    }
}
