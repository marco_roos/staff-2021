package br.com.dbccompany.lotr.Entity;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class Inventario_x_ItemId implements Serializable {
    private Integer id_inventario;
    private Integer id_item;

    public Inventario_x_ItemId( Integer id_inventario, Integer id_item ){
        this.id_inventario = id_inventario;
        this.id_item = id_item;
    }
}
