package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.Inventario_x_Item;
import br.com.dbccompany.lotr.Entity.Inventario_x_ItemId;
import br.com.dbccompany.lotr.Entity.ItemEntity;

public class Inventario_x_ItemDTO {
    private Inventario_x_ItemId id;
    private Integer quantidade;
    private InventarioEntity inventario;
    private ItemEntity item;

    public Inventario_x_ItemDTO (Inventario_x_Item inventarioItem){
        this.id = inventarioItem.getId();
        this.quantidade = inventarioItem.getQuantidade();
        this.inventario = inventarioItem.getInventario();
        this.item = inventarioItem.getItem();
    }

    public Inventario_x_Item converter(){
        Inventario_x_Item inventarioItem = new Inventario_x_Item();
        inventarioItem.setId(this.id);
        inventarioItem.setQuantidade(this.quantidade);
        inventarioItem.setInventario(this.inventario);
        inventarioItem.setItem(this.item);
        return inventarioItem;
    }

    public Inventario_x_ItemId getId() {
        return id;
    }

    public void setId(Inventario_x_ItemId id) {
        this.id = id;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public InventarioEntity getInventario() {
        return inventario;
    }

    public void setInventario(InventarioEntity inventario) {
        this.inventario = inventario;
    }

    public ItemEntity getItem() {
        return item;
    }

    public void setItem(ItemEntity item) {
        this.item = item;
    }
}
