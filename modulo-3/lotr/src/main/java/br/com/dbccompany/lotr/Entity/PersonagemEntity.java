package br.com.dbccompany.lotr.Entity;
import br.com.dbccompany.lotr.Enum.StatusEnum;
import javax.persistence.*;

@Entity
@Inheritance( strategy = InheritanceType.SINGLE_TABLE )
@DiscriminatorValue( value = "personagem" )
public abstract class PersonagemEntity {

    @Id
    @SequenceGenerator( name = "PERSONAGEM_SEQ", sequenceName = "PERSONAGEM_SEQ" )
    @GeneratedValue( generator = "PERSONAGEM_SEQ", strategy = GenerationType.SEQUENCE )
    protected Integer id;
    @Column( nullable = false, unique = true, updatable = false )
    protected String nome;
    @Column( nullable = false, unique = false, updatable = true, precision = 4, scale = 2 )
    protected Double vida;
    @Enumerated( EnumType.STRING )
    protected StatusEnum status;
    @Column( nullable = false, unique = false, updatable = true, columnDefinition = "int default 1" )
    protected int experiencia = 0;
    @Column( nullable = false, unique = false, updatable = true, columnDefinition = "int default 1" )
    protected int expPorAtaq = 1;
    @Column( nullable = false, unique = false, updatable = true, columnDefinition = "double default 0." )
    protected Double danoRecebido = 0.0;

    @OneToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "id" )
    private InventarioEntity inventario;

    public PersonagemEntity( String nome )
    {
        this.nome = nome;
    }

    public int getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getVida() {
        return vida;
    }

    public void setVida(Double vida) {
        this.vida = vida;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    public int getExperiencia() {
        return experiencia;
    }

    public void setExperiencia(Integer experiencia) {
        this.experiencia = experiencia;
    }

    public int getExpPorAtaq() {
        return expPorAtaq;
    }

    public void setExpPorAtaq(Integer expPorAtaq) {
        this.expPorAtaq = expPorAtaq;
    }

    public Double getDanoRecebido() {
        return danoRecebido;
    }

    public void setDanoRecebido(Double danoRecebido) {
        this.danoRecebido = danoRecebido;
    }

    public void setExperiencia(int experiencia) {
        this.experiencia = experiencia;
    }

    public void setExpPorAtaq(int expPorAtaq) {
        this.expPorAtaq = expPorAtaq;
    }

    public InventarioEntity getInventario() {
        return inventario;
    }

    public void setInventario(InventarioEntity inventario) {
        this.inventario = inventario;
    }
}
