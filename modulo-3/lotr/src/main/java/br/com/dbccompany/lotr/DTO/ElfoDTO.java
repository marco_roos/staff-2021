package br.com.dbccompany.lotr.DTO;
import br.com.dbccompany.lotr.Entity.ElfoEntity;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Enum.StatusEnum;

public class ElfoDTO {
    private Integer id;
    private String nome;
    private Double vida;
    private Integer experiencia;
    private Double qntDano;
    private Integer qntExperienciaPorAtaque;
    private StatusEnum status;
    private InventarioEntity inventario;

    public ElfoDTO (ElfoEntity elfo){
        this.id = elfo.getId();
        this.nome = elfo.getNome();
        this.vida = elfo.getVida();
        this.experiencia = elfo.getExperiencia();
        this.qntDano = elfo.getDanoRecebido();
        this.qntExperienciaPorAtaque = elfo.getExpPorAtaq();
        this.status = elfo.getStatus();
        this.inventario = elfo.getInventario();
    }

    public ElfoDTO(){}

    public ElfoEntity converter(){
        ElfoEntity elfo = new ElfoEntity(this.nome);
        elfo.setId(this.id);
        elfo.setNome(this.nome);
        elfo.setVida(this.vida);
        elfo.setExperiencia(this.experiencia);
        elfo.setDanoRecebido(this.qntDano);
        elfo.setExpPorAtaq(this.qntExperienciaPorAtaque);
        elfo.setStatus(this.status);
        elfo.setInventario(this.inventario);
        return elfo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getVida() {
        return vida;
    }

    public void setVida(Double vida) {
        this.vida = vida;
    }

    public Integer getExperiencia() {
        return experiencia;
    }

    public void setExperiencia(Integer experiencia) {
        this.experiencia = experiencia;
    }

    public Double getQntDano() {
        return qntDano;
    }

    public void setQntDano(Double qntDano) {
        this.qntDano = qntDano;
    }

    public Integer getQntExperienciaPorAtaque() {
        return qntExperienciaPorAtaque;
    }

    public void setQntExperienciaPorAtaque(Integer qntExperienciaPorAtaque) {
        this.qntExperienciaPorAtaque = qntExperienciaPorAtaque;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    public InventarioEntity getInventario() {
        return inventario;
    }

    public void setInventario(InventarioEntity inventario) {
        this.inventario = inventario;
    }
}
