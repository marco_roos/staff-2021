package br.com.dbccompany.lotr.Service;
import br.com.dbccompany.lotr.DTO.AnaoDTO;
import br.com.dbccompany.lotr.Entity.AnaoEntity;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Enum.StatusEnum;
import br.com.dbccompany.lotr.LotrApplication;
import br.com.dbccompany.lotr.Repository.AnaoRepository;
import br.com.dbccompany.lotr.Tools.Exceptions.NotFoundException_Anao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class AnaoService extends PersonagemService<AnaoRepository, AnaoEntity> {

    private Logger logger = LoggerFactory.getLogger( LotrApplication.class );

    @Transactional(rollbackFor = Exception.class)
    public AnaoDTO salvarAnao(AnaoEntity e) {
        return new AnaoDTO(super.salvarEEditar(e));
    }

    @Transactional(rollbackFor = Exception.class)
    public AnaoDTO editarAnao(AnaoEntity anaoEntity, Integer id) throws NotFoundException_Anao {
        try{
            anaoEntity.setId(id);
            return new AnaoDTO( super.editar( anaoEntity, id ) );
        } catch( Exception e ) {
            logger.info( e.getMessage() );
            throw  new NotFoundException_Anao();
        }
    }

    public AnaoDTO buscarAnaoPorId(Integer id) throws NotFoundException_Anao {
        try {
            return new AnaoDTO(super.buscarPorId(id));
        } catch( Exception e ) {
            logger.info( e.getMessage() );
            throw new NotFoundException_Anao();
        }
    }

    private List<AnaoDTO> listaEntityParaDTO(List<AnaoEntity> listaEntity){
        List<AnaoDTO> anoesDTO = new ArrayList<>();
        for(AnaoEntity anao : listaEntity){
            anoesDTO.add(new AnaoDTO(anao));
        }
        return anoesDTO;
    }

    public List<AnaoDTO> findAllAnao()
    {
        return listaEntityParaDTO( super.findAll() );
    }

    public AnaoDTO findAnaoByNome( String nome )
    {
        return new AnaoDTO(super.findByNome(nome));
    }

    public AnaoDTO findAnaoByVida( Double vida )
    {
        return new AnaoDTO(super.findByVida(vida));
    }

    public List<AnaoDTO> findAllAnaoByVida( Double vida )
    {
        return listaEntityParaDTO(super.findAllByVida(vida));
    }

    public AnaoDTO findAnaoByExperiencia( Integer experiencia )
    {
        return new AnaoDTO(super.findByExperiencia(experiencia));
    }

    public List<AnaoDTO> findAllAnaoByExperiencia( Integer experiencia )
    {
        return listaEntityParaDTO(super.findAllByExperiencia(experiencia));
    }

    public AnaoDTO findAnaoBydanoRecebido( Double dano )
    {
        return new AnaoDTO(super.findBydanoRecebido(dano));
    }

    public List<AnaoDTO> findAllAnaoByDanoRecebido( Double dano )
    {
        return listaEntityParaDTO(super.findAllByDanoRecebido(dano));
    }

    public AnaoDTO findAnaoByExpPorAtaq( Integer experiencia )
    {
        return new AnaoDTO(super.findByExpPorAtaq(experiencia));
    }

    public List<AnaoDTO> findAnaoAllByExpPorAtaq( Integer experiencia )
    {
        return listaEntityParaDTO(super.findAllByExpPorAtaq(experiencia));
    }

    public AnaoDTO findAnaoByStatus( StatusEnum status )
    {
        return new AnaoDTO(super.findByStatus(status));
    }

    public List<AnaoDTO> findAllAnaoByStatus( StatusEnum status )
    {
        return listaEntityParaDTO(super.findAllByStatus(status));
    }

    public AnaoDTO findAnaoByInventario( InventarioEntity inventario )
    {
        return new AnaoDTO(super.findByInventario(inventario));
    }
}
