package br.com.dbccompany.lotr.Service;
import br.com.dbccompany.lotr.DTO.Inventario_x_ItemDTO;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.Inventario_x_Item;
import br.com.dbccompany.lotr.Entity.Inventario_x_ItemId;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import br.com.dbccompany.lotr.LotrApplication;
import br.com.dbccompany.lotr.Repository.Inventario_x_ItemRepository;
import br.com.dbccompany.lotr.Tools.Exceptions.NotFoundException_Anao;
import br.com.dbccompany.lotr.Tools.Exceptions.NotFoundException_Inventario_x_Item;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class Inventario_x_ItemService {

    private Logger logger = LoggerFactory.getLogger( LotrApplication.class );

    @Autowired
    private Inventario_x_ItemRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public Inventario_x_ItemDTO salvar(Inventario_x_Item inventarioItem ){
        Inventario_x_Item novoInventarioItem = this.salvarEEditar(inventarioItem);
        return new Inventario_x_ItemDTO(novoInventarioItem);
    }
    @Transactional(rollbackFor = Exception.class)
    public Inventario_x_ItemDTO editar( Inventario_x_Item inventarioItem, Inventario_x_ItemId id) throws NotFoundException_Inventario_x_Item
    {
        try {
            inventarioItem.setId(id);
            Inventario_x_Item novoInventarioItem = this.salvarEEditar(inventarioItem);
            return new Inventario_x_ItemDTO(novoInventarioItem);
        } catch ( Exception e ) {
            logger.info( e.getMessage() );
            throw new NotFoundException_Inventario_x_Item();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    protected Inventario_x_Item salvarEEditar(Inventario_x_Item inventarioItem) {
        return repository.save (inventarioItem);
    }

    private List<Inventario_x_ItemDTO> listaEntityParaDBO(List<Inventario_x_Item> listaEntity){
        List<Inventario_x_ItemDTO> inventarioItensDTO = new ArrayList<>();
        for (Inventario_x_Item inventarioItem : listaEntity){
            inventarioItensDTO.add(new Inventario_x_ItemDTO(inventarioItem));
        }
        return inventarioItensDTO;
    }
    public List<Inventario_x_ItemDTO> buscarTodos(){
        return listaEntityParaDBO(repository.findAll());
    }
    public Inventario_x_ItemDTO buscarPorId(Inventario_x_ItemId id){
        Optional<Inventario_x_Item> inventarioItem = repository.findById(id);
        if(inventarioItem.isPresent()){
            return new Inventario_x_ItemDTO(repository.findById(id).get());
        }
        return null;
    }
    public Inventario_x_ItemDTO buscarPorQuantidade(Integer quantidade){
        return new Inventario_x_ItemDTO(repository.findByQuantidade(quantidade));
    }
    public List<Inventario_x_ItemDTO> buscarTodosPorQuantidade(Integer quantidade){
        return listaEntityParaDBO(repository.findAllByQuantidade(quantidade));
    }
    public List<Inventario_x_ItemDTO> buscarTodosPorQuantidades(List<Integer> quantidades){
        return listaEntityParaDBO(repository.findAllByQuantidadeIn(quantidades));
    }

    public Inventario_x_ItemDTO buscarPorInventario(InventarioEntity inventario){
        return new Inventario_x_ItemDTO(repository.findByInventario(inventario));
    }

    public List<Inventario_x_ItemDTO> buscarTodosPorInventario (InventarioEntity inventario){
        return listaEntityParaDBO(repository.findAllByInventario(inventario));
    }

    public List<Inventario_x_ItemDTO> buscarPorInventarioIn (List<InventarioEntity> inventarios){
        return listaEntityParaDBO(repository.findByInventarioIn(inventarios));
    }

    public Inventario_x_ItemDTO buscarPorItem (ItemEntity item){
        return new Inventario_x_ItemDTO(repository.findByItem(item));
    }

    public List<Inventario_x_ItemDTO> buscarTodosPorItem (ItemEntity item){
        return listaEntityParaDBO(repository.findAllByItem(item));
    }

    public List<Inventario_x_ItemDTO> buscarPorItemIn(List<ItemEntity> itens){
        return listaEntityParaDBO(repository.findByItemIn(itens));
    }
}

