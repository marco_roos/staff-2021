package br.com.dbccompany.lotr.Repository;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import br.com.dbccompany.lotr.Enum.StatusEnum;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;

@Repository
public interface PersonagemRepository < T extends PersonagemEntity, Integer > extends CrudRepository< T, Integer > {
    Optional<T> findById( Integer id );
    T findByNome( String nome );
    T findByVida( Double vida );
    List<T> findAllByVida( Double vida );
    T findByExperiencia( Integer experiencia );
    List<T> findAllByExperiencia( Integer experiencia );
    T findBydanoRecebido( Double danoRecebido );
    List<T> findAllByDanoRecebido( Double danoRecebido );
    T findByExpPorAtaq( Integer expPorAtaq );
    List<T> findAllByExpPorAtaq( Integer expPorAtaq );
    T findByStatus( StatusEnum status );
    List<T> findAllByStatus( StatusEnum status );
    T findByInventario( InventarioEntity inventario );
}