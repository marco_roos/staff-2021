package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.ItemDTO;
import br.com.dbccompany.lotr.Service.ItemService;
import br.com.dbccompany.lotr.Tools.Exceptions.InvalidArgument_Item;
import br.com.dbccompany.lotr.Tools.Exceptions.NotFoundException_Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/item" )
public class itemController {

    @Autowired
    private ItemService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<ItemDTO> trazerTodosItens(){
        return service.trazerTodosOsItens();
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public ResponseEntity<ItemDTO> trazerItemEspecifico(@PathVariable Integer id ) {
        try {
            return new ResponseEntity<>( service.buscarPorId(id), HttpStatus.ACCEPTED);
        }catch( NotFoundException_Item e ){
            System.err.println(e.getErrorMessage());
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

    }

    @PostMapping( value = "/salvar")
    @ResponseBody
    public ItemDTO salvarItem(@RequestBody ItemDTO item ){
        try {
            return service.save( item );
        }
        catch( InvalidArgument_Item e ){
            System.err.println(e.getErrorMessage());
            return null;
        }
    }

    @PutMapping ( value = "/editar/ { id }")
    @ResponseBody
    public ItemDTO editarItem( @RequestBody ItemDTO item, @PathVariable Integer id){
        try {
            return service.edit(item, id);
        }
        catch( InvalidArgument_Item e ){
            System.err.println(e.getErrorMessage());
            return null;
        }
    }

    @GetMapping ( value = "/encontrarPorDescricao" )
    @ResponseBody
    public ItemDTO encontrarPorDescricao( @RequestBody String descricao ) {
        return service.findByDescricao(descricao);
    }

    @GetMapping ( value = "/encontrarTodosPorDescricao" )
    @ResponseBody
    public List<ItemDTO> encontrarTodosPorDescricao( @RequestBody String descricao ){
        return service.findAllByDescricao( descricao);
    }

    @GetMapping ( value = "/encontrarTodosPorDescricaoEm" )
    @ResponseBody
    public List<ItemDTO> encontrarTodosPorDescricaoEm( @RequestBody List<String> descricao ){
        return service.findAllByDescricaoIn( descricao );
    }
}