package br.com.dbccompany.lotr.Controller;
import br.com.dbccompany.lotr.DTO.AnaoDTO;
import br.com.dbccompany.lotr.Entity.AnaoEntity;
import br.com.dbccompany.lotr.Enum.StatusEnum;
import br.com.dbccompany.lotr.Service.AnaoService;
import br.com.dbccompany.lotr.Tools.Exceptions.NotFoundException_Anao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@Controller
@RequestMapping(value = "/api/anao")
public class AnaoController {

    @Autowired
    private AnaoService service;

    @GetMapping(value = "/salvar/{nome}")
    @ResponseBody
    public AnaoDTO salvarAnao(@PathVariable String nome) {
        AnaoEntity anao = new AnaoEntity(nome);
        return service.salvarAnao(anao);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<Object> editarAnao(@RequestBody AnaoDTO anao, @PathVariable Integer id) {
        try {
            return new ResponseEntity<>( service.editarAnao(anao.converter(), id), HttpStatus.ACCEPTED);
        } catch( NotFoundException_Anao e ) {
            System.err.println( e.getMessage() );
            return new ResponseEntity<>( e.getMessage(), HttpStatus.NOT_FOUND );
        }
    }

    @GetMapping(value = "/")
    @ResponseBody
    public List<AnaoDTO> buscarTodosAnoes() {
        return service.findAllAnao();
    }

    @GetMapping(value = "/{nome}")
    @ResponseBody
    public AnaoDTO buscarAnaoPorNome(@PathVariable String nome) {
        return service.findAnaoByNome(nome);
    }

    @GetMapping(value = "/{experiencia}")
    @ResponseBody
    public AnaoDTO buscarAnaoPorExperiencia(@PathVariable Integer experiencia) {
        return service.findAnaoByExperiencia(experiencia);
    }

    @GetMapping(value = "/buscarTodos/{experiencia}")
    @ResponseBody
    public List<AnaoDTO> buscarTodosAnoesPorExperiencia(@PathVariable Integer experiencia) {
        return service.findAllAnaoByExperiencia(experiencia);
    }

    @GetMapping(value = "/{vida}")
    @ResponseBody
    public AnaoDTO buscarAnaoPorVida(@PathVariable Double vida) {
        return service.findAnaoByVida(vida);
    }

    @GetMapping(value = "/buscarTodos/{vida}")
    @ResponseBody
    public List<AnaoDTO> buscarTodosAnoesPorVida(@PathVariable Double vida) {
        return service.findAllAnaoByVida(vida);
    }

    @GetMapping(value = "/qntDano/{qntDano}")
    @ResponseBody
    public AnaoDTO buscarAnaoPorQntDano(@PathVariable Double qntDano) {
        return service.findAnaoBydanoRecebido(qntDano);
    }

    @GetMapping(value = "/buscarTodos/qntDano/{qntDano}")
    @ResponseBody
    public List<AnaoDTO> buscarTodosAnoesPorQntDano(@PathVariable Double qntDano) {
        return service.findAllAnaoByDanoRecebido(qntDano);
    }

    @GetMapping(value = "/qntExpAtaque/{qntExpAtaque}")
    @ResponseBody
    public AnaoDTO buscarAnaoPorQntExperienciaPorAtaque(@PathVariable Integer qntExpAtaque) {
        return service.findAnaoByExpPorAtaq(qntExpAtaque);
    }

    @GetMapping(value = "/buscarTodos/qntExpAtaque/{qntExpAtaque}")
    @ResponseBody
    public List<AnaoDTO> buscarTodosAnoesPorQntExperienciaPorAtaque(@PathVariable Integer qntExpAtaque) {
        return service.findAllAnaoByExperiencia(qntExpAtaque);
    }

    @GetMapping(value = "/{status}")
    @ResponseBody
    public AnaoDTO buscarAnaoPorStatus(@PathVariable StatusEnum status) {
        return service.findAnaoByStatus(status);
    }

    @GetMapping(value = "/buscarTodos/{status}")
    @ResponseBody
    public List<AnaoDTO> buscarTodosAnoesPorStatus(@PathVariable StatusEnum status) {
        return service.findAllAnaoByStatus(status);
    }

}
