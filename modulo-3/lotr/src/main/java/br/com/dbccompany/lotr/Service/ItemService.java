package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.ItemDTO;
import br.com.dbccompany.lotr.LotrApplication;
import br.com.dbccompany.lotr.Repository.ItemRepository;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import br.com.dbccompany.lotr.Tools.Exceptions.InvalidArgument_Item;
import br.com.dbccompany.lotr.Tools.Exceptions.NotFoundException_Item;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ItemService {

    @Autowired
    private ItemRepository repository;

    private Logger logger = LoggerFactory.getLogger(LotrApplication.class);

    RestTemplate restTemplate = new RestTemplate();
    String url = "http://localhost:8081/salvar/logs";

    public List<ItemDTO> trazerTodosOsItens(){
        return this.convertList( (List<ItemEntity>) repository.findAll() );
    }

    @Transactional( rollbackFor = Exception.class)
    public ItemDTO save( ItemDTO item ) throws InvalidArgument_Item {
        return this.saveAndEdit( item.converter() );
    }

    @Transactional( rollbackFor = Exception.class)
    public ItemDTO edit( ItemDTO item, Integer id ) throws InvalidArgument_Item {
        ItemEntity itemEntity = item.converter();
        itemEntity.setId(id);
        return this.saveAndEdit( itemEntity );
    }

    private ItemDTO saveAndEdit( ItemEntity item ) throws InvalidArgument_Item {
        ItemEntity itemNovo = repository.save( item );
        return new ItemDTO(itemNovo);
    }

    public ItemDTO buscarPorId( Integer id) throws NotFoundException_Item {
        try {
            Optional<ItemEntity> item = repository.findById(id);
            return new ItemDTO(repository.findById(id).get());
        }catch(Exception e){
            logger.error("No Items here, Maybe you did drop it somewhere else ?");
            restTemplate.postForObject(url, e.getMessage(), Object.class);
            throw new NotFoundException_Item();
        }
    }

    public ItemDTO findByDescricao( String descricao ){
        return new ItemDTO(repository.findByDescricao( descricao));
    };

    public List<ItemDTO> findAllByDescricao( String descricao ){
        return this.convertList( repository.findAllByDescricao( descricao ) );
    }

    public List<ItemDTO> findAllByDescricaoIn( List<String> descricao ){
        return this.convertList( repository.findAllByDescricaoIn( descricao ) );
    }

    private List<ItemDTO> convertList( List<ItemEntity> entidades){
        ArrayList<ItemDTO> itensNovo = new ArrayList<ItemDTO>();
        for( ItemEntity entidade:entidades){
            itensNovo.add( new ItemDTO( entidade ) );
        }
        return itensNovo;
    }

}
