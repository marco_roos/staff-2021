import java.util.*;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PaginadorInventarioTest
{
    @Test
    public void pularLimitarComUmItem()
    {
        Inventario mochila = new Inventario();
        Item arco = new Item( 1, "Arco" );
        mochila.adicionarItem( arco );
        PaginadorInventario paginador = new PaginadorInventario( mochila );
        paginador.pular(0);
        ArrayList<Item> returnedItemList = paginador.limitar(1);
        assertEquals( arco, returnedItemList.get(0) );
    }
    
    @Test
    public void pularLimitarComDoisItem()
    {
        Inventario mochila = new Inventario();
        Item arco = new Item( 1, "Arco" );
        Item flecha = new Item( 2, "Flecha" );
        mochila.adicionarItem( arco );
        mochila.adicionarItem( flecha );
        PaginadorInventario paginador = new PaginadorInventario( mochila );
        paginador.pular(0);
        ArrayList<Item> returnedItemList = paginador.limitar(2);
        assertEquals( arco, returnedItemList.get(0) );
        assertEquals( flecha , returnedItemList.get(1) );
    }
       
    @Test
    public void pularLimitarForaDosLimites()
    {
        Inventario mochila = new Inventario();
        Item arco = new Item( 1, "Arco" );
        Item flecha = new Item( 2, "Flecha" );
        mochila.adicionarItem( arco );
        mochila.adicionarItem( flecha );
        PaginadorInventario paginador = new PaginadorInventario( mochila );
        paginador.pular(1);
        ArrayList<Item> returnedItemList = paginador.limitar(3);
        assertEquals( flecha , returnedItemList.get(0) );
    }
}
