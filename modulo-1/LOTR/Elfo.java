public class Elfo extends Personagem{
    protected int indexFlecha = 0;
    private static int qtdElfos;
    
    public Elfo( String nome ) {
        super(nome);
        this.vida = 100.0;
        this.mochila.adicionarItem( new Item( 2, "Flecha" ) );
        this.mochila.adicionarItem( new Item( 1, "Arco" ) );
        this.receivedDamage = 0.0d;
        Elfo.qtdElfos++;
    }
    
    public void finalize() throws Throwable
    {
        Elfo.qtdElfos--;
    }
    
    public static int getQuantidade()
    {
        return Elfo.qtdElfos;
    }
    
    public Item getFlecha()
    {
        return this.mochila.obterItemNaPosicao(indexFlecha);
    }
    
    public void atiraFlecha( Anao anao ) {
        if( getFlecha().getQuantidade() > 0 )
        {
            getFlecha().setQuantidade( getFlecha().getQuantidade()-1 );
            this.experiencia += expPorAtaque;
            this.damage( receivedDamage );
            if( anao != null )
                anao.damage( 10.0d );
        }
    }

}