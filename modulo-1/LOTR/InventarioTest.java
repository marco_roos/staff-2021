import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class InventarioTest
{ 
    @Test
    public void adiciona2Itens()
    {
        Inventario mochila = new Inventario();
        mochila.adicionarItem( new Item( 2, "Erva Medicinal") );
        mochila.adicionarItem( new Item( 1, "Adaga Elfica") );
        assertEquals( 2, mochila.obterItemNaPosicao(0).getQuantidade() );
        assertEquals( 1, mochila.obterItemNaPosicao(1).getQuantidade() );
        assertEquals( "Erva Medicinal", mochila.obterItemNaPosicao(0).getDescricao() );
        assertEquals( "Adaga Elfica", mochila.obterItemNaPosicao(1).getDescricao() );
    }
    
    @Test
    public void remove3itens()
    {
        Inventario mochila = new Inventario();
        mochila.adicionarItem( new Item( 2, "Erva Medicinal") );
        mochila.adicionarItem( new Item( 10, "Flecha Envenenada") );
        mochila.adicionarItem( new Item( 1, "Adaga Elfica") );
        mochila.adicionarItem( new Item( 1, "Arco da Floresta") );
        mochila.adicionarItem( new Item( 20, "Pao") );
        mochila.removerItemNaPosicao( 0 );
        mochila.removerItemNaPosicao( 1 );
        mochila.removerItemNaPosicao( 2 );
        assertEquals( 2, mochila.getSlots() );
    }
    
    @Test
    public void adiciona5ItensRetornaAsDescricoesEmUmaString()
    {
        Inventario mochila = new Inventario();
        mochila.adicionarItem( new Item( 2, "Erva Medicinal") );
        mochila.adicionarItem( new Item( 10, "Flecha Envenenada") );
        mochila.adicionarItem( new Item( 1, "Adaga Elfica") );
        mochila.adicionarItem( new Item( 1, "Arco da Floresta") );
        mochila.adicionarItem( new Item( 20, "Pao") );
        assertEquals( "Erva Medicinal,Flecha Envenenada,Adaga Elfica,Arco da Floresta,Pao"
        , mochila.getDescricoesTodosItens() );
    }
    
    @Test
    public void encontraItemMaisAbundante()
    {
        Inventario mochila = new Inventario();
        mochila.adicionarItem( new Item( 2, "Erva Medicinal") );
        mochila.adicionarItem( new Item( 10, "Flecha Envenenada") );
        mochila.adicionarItem( new Item( 1, "Adaga Elfica") );
        mochila.adicionarItem( new Item( 20, "Pao") );
        mochila.adicionarItem( new Item( 1, "Arco da Floresta") );
        assertEquals( mochila.obterItemNaPosicao(3), mochila.getItemMaisAbundante() );
    }
    
    @Test
    public void procuraItemMaisAbundanteEmMochilaVazia()
    {
        Inventario mochila = new Inventario();
        mochila.adicionarItem( new Item( 2, "Erva Medicinal") );
        mochila.removerItemNaPosicao(0);
        assertNull( mochila.getItemMaisAbundante() );
    }
    
    @Test
    public void EncontraItemPorDescricao()
    {
        Inventario mochila = new Inventario();
        mochila.adicionarItem( new Item( 2, "Erva Medicinal") );
        mochila.adicionarItem( new Item( 10, "Flecha Envenenada") );
        mochila.adicionarItem( new Item( 1, "Adaga Elfica") );
        mochila.adicionarItem( new Item( 1, "Arco da Floresta") );
        mochila.adicionarItem( new Item( 20, "Pao") );
        assertEquals( mochila.obterItemNaPosicao(4), mochila.obterItemPelaDescricao("Pao"));
    }
    
    @Test
    public void inverteInventario()
    {
        Inventario mochila = new Inventario();
        mochila.adicionarItem( new Item( 2, "Erva Medicinal") );
        mochila.adicionarItem( new Item( 10, "Flecha Envenenada") );
        mochila.adicionarItem( new Item( 1, "Adaga Elfica") );
        mochila.adicionarItem( new Item( 20, "Pao") );
        mochila.adicionarItem( new Item( 1, "Arco da Floresta") );
        
        ArrayList<Item> mochilaInversa = mochila.inverterInventario();

        assertEquals( mochila.obterItemPelaDescricao("Arco da Floresta")
        , mochilaInversa.get(0) );
        assertEquals( mochila.obterItemPelaDescricao("Pao")
        , mochilaInversa.get(1) );
        assertEquals( mochila.obterItemPelaDescricao("Adaga Elfica")
        , mochilaInversa.get(2) );
        assertEquals( mochila.obterItemPelaDescricao("Flecha Envenenada")
        , mochilaInversa.get(3) );
        assertEquals( mochila.obterItemPelaDescricao("Erva Medicinal")
        , mochilaInversa.get(4) );
    }
    
    @Test
    public void organizaItensPorQuantidade()
    {
        Inventario mochila = new Inventario();
        mochila.adicionarItem( new Item( 2, "Erva Medicinal") );
        mochila.adicionarItem( new Item( 10, "Flecha Envenenada") );
        mochila.adicionarItem( new Item( 1, "Adaga Elfica") );
        mochila.adicionarItem( new Item( 20, "Pao") );
        mochila.adicionarItem( new Item( 1, "Arco da Floresta") );
        mochila.ordenarItensQuantidadeAscendente();
        
        assertEquals( mochila.obterItemNaPosicao(0).getQuantidade(), 
        1 );
        assertEquals( mochila.obterItemNaPosicao(1).getQuantidade(), 
        1 );
        assertEquals( mochila.obterItemNaPosicao(2).getQuantidade(), 
        2 );
        assertEquals( mochila.obterItemNaPosicao(3).getQuantidade(), 
        10 );
        assertEquals( mochila.obterItemNaPosicao(4).getQuantidade(), 
        20 );
        
    }
    
    @Test
    public void organizaItensPorQuantidadeEmOrdemAscendente()
    {
        Inventario mochila = new Inventario();
        mochila.adicionarItem( new Item( 2, "Erva Medicinal") );
        mochila.adicionarItem( new Item( 10, "Flecha Envenenada") );
        mochila.adicionarItem( new Item( 1, "Adaga Elfica") );
        mochila.adicionarItem( new Item( 20, "Pao") );
        mochila.adicionarItem( new Item( 1, "Arco da Floresta") );
        mochila.ordenarItens(TipoOrdenacao.ASC);
        
        assertEquals( mochila.obterItemNaPosicao(0).getQuantidade(), 
        1 );
        assertEquals( mochila.obterItemNaPosicao(1).getQuantidade(), 
        1 );
        assertEquals( mochila.obterItemNaPosicao(2).getQuantidade(), 
        2 );
        assertEquals( mochila.obterItemNaPosicao(3).getQuantidade(), 
        10 );
        assertEquals( mochila.obterItemNaPosicao(4).getQuantidade(), 
        20 );
        
    }
    
    @Test
    public void organizaItensPorQuantidadeEmOrdemDescendente()
    {
        Inventario mochila = new Inventario();
        mochila.adicionarItem( new Item( 2, "Erva Medicinal") );
        mochila.adicionarItem( new Item( 10, "Flecha Envenenada") );
        mochila.adicionarItem( new Item( 1, "Adaga Elfica") );
        mochila.adicionarItem( new Item( 20, "Pao") );
        mochila.adicionarItem( new Item( 1, "Arco da Floresta") );
        mochila.ordenarItens(TipoOrdenacao.DESC);
        
        assertEquals( mochila.obterItemNaPosicao(0).getQuantidade(), 
        20 );
        assertEquals( mochila.obterItemNaPosicao(1).getQuantidade(), 
        10 );
        assertEquals( mochila.obterItemNaPosicao(2).getQuantidade(), 
        2 );
        assertEquals( mochila.obterItemNaPosicao(3).getQuantidade(), 
        1 );
        assertEquals( mochila.obterItemNaPosicao(4).getQuantidade(), 
        1 );
        
    }
}
