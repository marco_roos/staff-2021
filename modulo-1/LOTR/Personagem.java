public class Personagem
{
    protected Inventario mochila;
    protected String nome;
    protected Double vida;
    protected Double receivedDamage;
    protected Status status;
    protected int experiencia = 0;
    protected int expPorAtaque;
    
    public Personagem( String nome )
    {
        this.nome = nome;
        mochila = new Inventario();
        status = Status.RECEM_CRIADO;
        expPorAtaque = 1;
        receivedDamage = 0.0d;
    }
    
    public String getNome()
    {
        return this.nome;
    }
    
    public void setNome( String nome )
    {
        this.nome = nome;
    }
    
    public int getExperiencia()
    {
        return this.experiencia;
    }
    
    public Status getStatus()
    {
        return this.status;
    }
    
    public double getVida()
    {
         return this.vida;
    }
        
    public double damageLimit( double vida, double damage )
    {
        if( vida > damage )
            return damage;
        return vida;
    }
        
    public void damage( double damage )
    {
        if( status != Status.MORTO )
        {
            this.vida -= damageLimit( this.vida, damage );
            status = this.vida > 0 ? Status.SOFREU_DANO : Status.MORTO;
        }
    }
    
    public Inventario getInventario()
    {
        return this.mochila;
    }
    
    public void ganharItem( Item item )
    {
        this.mochila.adicionarItem(item);
    }
    
    public void perderItem( Item item )
    {
        if( item != null )
        {
            this.mochila.removerItem( item );
        }
    }
}
