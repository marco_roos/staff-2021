import java.util.*;

public class ExercitoElfo
{
    private final static ArrayList<Class> TIPOS_PERMITIDOS = new ArrayList<>(
        Arrays.asList( ElfoVerde.class, ElfoNoturno.class ) );
        
    private ArrayList<Elfo> elfos;
    private HashMap< Status, ArrayList<Elfo> > porStatus = new HashMap<>();
    private HashMap< Class, ArrayList<Elfo> > vivosPorClasse = new HashMap<>();
    
    public ExercitoElfo()
    {
        this.elfos = new ArrayList<>();
    }
    
    public void alistarElfo( Elfo elfo )
    {
        if( TIPOS_PERMITIDOS.contains( elfo.getClass()) )
        {
            elfos.add( elfo );
            
            ArrayList<Elfo> elfoDeStatus = porStatus.get( elfo.getStatus() );
            if( elfoDeStatus == null )
            {
                elfoDeStatus = new ArrayList<>();
                porStatus.put( elfo.getStatus(), elfoDeStatus );
            }
            elfoDeStatus.add( elfo );
        }
    }
    
    public ArrayList<Elfo> buscarLista()
    {
        return this.elfos;
    }
    
    public ArrayList<Elfo> buscarElfosTipo( Status status )
    {
        return this.porStatus.get( status );
    }
    
    public void inicializarVivosPorClasse()
    {
        vivosPorClasse.put( TIPOS_PERMITIDOS.get(0), new ArrayList<>() );
        vivosPorClasse.put( TIPOS_PERMITIDOS.get(1), new ArrayList<>() );
    }
    
    public void ordenarClasses()
    {
        inicializarVivosPorClasse();
        for( Elfo elfo : elfos )
        {
            if( elfo.getStatus() != Status.MORTO && elfo.getFlecha().getQuantidade() > 0 )
            {
                vivosPorClasse.get(elfo.getClass()).add(elfo);
            }
        }
    }
    
    public ArrayList<Elfo> getOrdemDeAtaque(ArrayList<Elfo> atacantes)
    {
        ArrayList<Elfo> ordenado = new ArrayList<>();
        for( Elfo elfo : atacantes )
        {
            if( atacantes.getClass() == TIPOS_PERMITIDOS.get(0) 
            && elfo.getStatus() != Status.MORTO 
            && elfo.getFlecha().getQuantidade() > 0 )
                ordenado.add( elfo );
        }
        for( Elfo elfo : atacantes )
        {
            if( atacantes.getClass() == TIPOS_PERMITIDOS.get(1)
            && elfo.getStatus() != Status.MORTO 
            && elfo.getFlecha().getQuantidade() > 0 )
                ordenado.add( elfo );
        }
        return ordenado;
    }
    
    public ArrayList<Elfo> getOrdemAlternadaDeAtaque( Class primeiroAtacante )
    {
        ArrayList<Elfo> ordenado = new ArrayList<>();
        ordenarClasses();
        int qtdElfosVerdes = this.vivosPorClasse.get( TIPOS_PERMITIDOS.get(0) ).size();
        int qtdElfosNoturnos = this.vivosPorClasse.get( TIPOS_PERMITIDOS.get(1) ).size();
        int limitador = qtdElfosVerdes > qtdElfosNoturnos ? qtdElfosNoturnos : qtdElfosVerdes;
        
        for( int i = 0; i < limitador; i++ )
        {
            if( primeiroAtacante == TIPOS_PERMITIDOS.get(0) )
            {
                ordenado.add(this.vivosPorClasse.get(TIPOS_PERMITIDOS.get(0)).get(i));
                ordenado.add(this.vivosPorClasse.get(TIPOS_PERMITIDOS.get(1)).get(i));
            }
            else
            {
                ordenado.add(this.vivosPorClasse.get(TIPOS_PERMITIDOS.get(1)).get(i));
                ordenado.add(this.vivosPorClasse.get(TIPOS_PERMITIDOS.get(0)).get(i));
            }
        }
        return ordenado;
    }
    
    public ArrayList<Elfo> organizaPorFlechas(ArrayList<Elfo> lista)
    {
        Elfo temp;
        ArrayList<Elfo> ordenado = lista;
        for( int i = 0; i < ordenado.size(); i++)
        {
            for( int j = 0; j < ordenado.size() - 1; j++)
            {
                if( ordenado.get(j).getFlecha().getQuantidade() 
                < ordenado.get(j+1).getFlecha().getQuantidade() )
                {
                    temp = ordenado.get(j);
                    ordenado.set( j, ordenado.get(j+1) );
                    ordenado.set( j+1, temp );
                }
            }
        }
        return ordenado;
    }
    
    public ArrayList<Elfo> getOrdemPorFlechaDeAtaque()
    {
        ordenarClasses();
        ArrayList<Elfo> ordenado = new ArrayList<>();
        int quantidadeElfosVerdes = vivosPorClasse.get( ElfoVerde.class ).size();
        int limitadorNoturnos = 0;
        
        while( (double)limitadorNoturnos/(quantidadeElfosVerdes+limitadorNoturnos) <= 0.3 )
        {
            limitadorNoturnos++;
            if( (double)limitadorNoturnos/(quantidadeElfosVerdes+limitadorNoturnos) <= 0.3 )
                ordenado.add( vivosPorClasse.get( ElfoNoturno.class ).get(limitadorNoturnos-1) );
        }
        for( int i = 0; i < quantidadeElfosVerdes; i++ )
        {
            ordenado.add( vivosPorClasse.get( ElfoVerde.class ).get(i) );
        }

        return ordenado;
    }
    
    public void Atacar( Anao anao, ArrayList<Elfo> ordem )
    {
        for( Elfo elfo : ordem )
        {
            if( anao.getStatus() == Status.MORTO )
                break;
            elfo.atiraFlecha( anao );
        }
    }
}
