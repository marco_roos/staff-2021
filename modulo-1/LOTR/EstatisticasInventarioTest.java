import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class EstatisticasInventarioTest
{
    @Test
    public void calculaMediaDeUmItem()
    {
        Inventario mochila = new Inventario();
        Item pao = new Item( 5, "Pao" );
        mochila.adicionarItem( pao );
        EstatisticasInventario estatisticas = new EstatisticasInventario( mochila );
        double media = estatisticas.calcularMedia();
        assertEquals( 5, media, 1e-8 );
    }
    
    @Test
    public void calculaMediaDeDoisItem()
    {
        Inventario mochila = new Inventario();
        Item pao = new Item( 5, "Pao" );
        Item flechas = new Item( 11, "Flechas" );
        mochila.adicionarItem( pao );
        mochila.adicionarItem( flechas );
        EstatisticasInventario estatisticas = new EstatisticasInventario( mochila );
        double media = estatisticas.calcularMedia();
        assertEquals( 8, media, 1e-8 );
    }
    
    @Test
    public void calculaMediaSemItens()
    {
        Inventario mochila = new Inventario();
        EstatisticasInventario estatisticas = new EstatisticasInventario( mochila );
        double media = estatisticas.calcularMedia();
        assertTrue( Double.isNaN(media) );
    }
    
    @Test
    public void calculaMedianaDeItensPares()
    {
        Inventario mochila = new Inventario();
        Item pao = new Item( 10, "Pao" );
        Item flechas = new Item( 16, "Flechas" );
        mochila.adicionarItem( pao );
        mochila.adicionarItem( flechas );
        EstatisticasInventario estatisticas = new EstatisticasInventario( mochila );
        double mediana = estatisticas.calcularMediana();
        assertEquals( 13, mediana, 1e-8 );
    }
    
    @Test
    public void calculaMedianaDeItensImpares()
    {
        Inventario mochila = new Inventario();
        Item pao = new Item( 10, "Pao" );
        Item pocao = new Item( 25, "Pocao" );
        Item flechas = new Item( 16, "Flechas" );
        mochila.adicionarItem( pao );
        mochila.adicionarItem( pocao );
        mochila.adicionarItem( flechas );
        EstatisticasInventario estatisticas = new EstatisticasInventario( mochila );
        double mediana = estatisticas.calcularMediana();
        assertEquals( 25, mediana, 1e-8 );
    }
    
    @Test
    public void contaQunatosItensTemAcimaDaMedia()
    {
        Inventario mochila = new Inventario();
        Item arco = new Item( 1, "Arco" );
        Item elmo = new Item( 1, "Elmo" );
        Item pao = new Item( 8, "Pao" );
        Item pocao = new Item( 4, "Pocao" );
        Item flechas = new Item( 16, "Flechas" );
        mochila.adicionarItem( arco );
        mochila.adicionarItem( elmo );
        mochila.adicionarItem( pao );
        mochila.adicionarItem( pocao );
        mochila.adicionarItem( flechas );
        EstatisticasInventario estatisticas = new EstatisticasInventario( mochila );
        int quantidade = estatisticas.qtdItensAcimaDaMedia();
        assertEquals( 2, quantidade );
    }
}
