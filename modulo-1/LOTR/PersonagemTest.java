import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class PersonagemTest
{
    @Test
    public void serGanhaDoisItens()
    {
        Personagem ser = new Personagem("Chutulu");
        Item pocao = new Item( 4, "Pocao");
        Item pao = new Item( 1, "Adaga" );
        ser.ganharItem( pocao );
        ser.ganharItem( pao );
        assertTrue( ser.getInventario().getItens().contains( pocao ) );
        assertTrue( ser.getInventario().getItens().contains( pao ) );
    }
    
    @Test
    public void serPerdeDoisItens()
    {
        Personagem ser = new Personagem("Chutulu");
        Item pocao = new Item( 4, "Pocao");
        Item pao = new Item( 1, "Adaga" );
        ser.ganharItem( pocao );
        ser.ganharItem( pao );
        assertTrue( ser.getInventario().getItens().contains( pocao ) );
        assertTrue( ser.getInventario().getItens().contains( pao ) );
        ser.perderItem( pao );
        assertTrue( ser.getInventario().getItens().contains( pocao ) );
        assertTrue( !ser.getInventario().getItens().contains( pao ) );
    }
    
    @Test
    public void serPerdeItemQueNaoPossui()
    {
        Personagem ser = new Personagem("Chutulu");
        Item pocao = new Item( 4, "Pocao");
        Item pao = new Item( 1, "Adaga" );
        ser.perderItem( pocao );
        ser.perderItem( pao );
        assertTrue( !ser.getInventario().getItens().contains( pocao ) );
        assertTrue( !ser.getInventario().getItens().contains( pao ) );
    }
}
