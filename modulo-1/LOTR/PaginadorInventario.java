import java.util.*;

public class PaginadorInventario
{
   private Inventario mochila;
   private int marcador;
   
    public PaginadorInventario( Inventario inventario )
    {
        this.mochila = inventario;
        marcador = 0;
    }
    
    public void pular( int index )
    {
        this.marcador = index > 0 ? index : 0;
    }
    
    private int limiteDeMostrar( int mostrar, int limite)
    {
        return mostrar < limite ? mostrar : limite;
    }
    
    public ArrayList<Item> limitar( int mostrar )
    {
        ArrayList<Item> lista = new ArrayList<Item>();
        int mostrarTotal = limiteDeMostrar( this.marcador + mostrar, this.mochila.getSlots() );     
        for( int i = this.marcador; i < mostrarTotal; i++ )
        {
            lista.add( this.mochila.obterItemNaPosicao(i) );
        }
        
        return lista;
    }
}
