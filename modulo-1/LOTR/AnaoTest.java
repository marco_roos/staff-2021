import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class AnaoTest
{
    @Test
    public void anaoTem110VidaAoNascer()
    {
        Anao anao = new Anao( "Gimli" );
        assertEquals( 110, anao.getVida(), 0.000001d );
        assertEquals( Status.RECEM_CRIADO, anao.getStatus() );
    }
    
    @Test
    public void anaoSofreDanoSobra50Vida()
    {
        Anao anao = new Anao( "Gimli" );
        anao.damage(60.0d);
        assertEquals( 50, anao.getVida(), 0.000001d );
        assertEquals( Status.SOFREU_DANO, anao.getStatus() );
    }
    
    @Test
    public void anaoSofreDanoMorre()
    {
        Anao anao = new Anao( "Gimli" );
        anao.damage(150.0d);
        assertEquals( 0, anao.getVida(), 0.000001d );
        assertEquals( Status.MORTO, anao.getStatus() );
    }
    
    @Test
    public void anaoGanhaDoisItens()
    {
        Anao anao = new Anao("Gimli");
        Item pocao = new Item( 4, "Pocao");
        Item pao = new Item( 1, "Adaga" );
        anao.ganharItem( pocao );
        anao.ganharItem( pao );
        assertTrue( anao.getInventario().getItens().contains( pocao ) );
        assertTrue( anao.getInventario().getItens().contains( pao ) );
    }
    
    @Test
    public void anaoPerdeDoisItens()
    {
        Anao anao = new Anao("Legolas");
        Item pocao = new Item( 4, "Pocao");
        Item pao = new Item( 1, "Adaga" );
        anao.ganharItem( pocao );
        anao.ganharItem( pao );
        assertTrue( anao.getInventario().getItens().contains( pocao ) );
        assertTrue( anao.getInventario().getItens().contains( pao ) );
        anao.perderItem( pao );
        assertTrue( anao.getInventario().getItens().contains( pocao ) );
        assertTrue( !anao.getInventario().getItens().contains( pao ) );
    }
}
