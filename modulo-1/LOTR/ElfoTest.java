import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoTest
{
    @Test
    public void exemploMudancaValoresItem() {
        Elfo elfo = new Elfo( "Legolas" );
        Item flecha = elfo.getFlecha();
        flecha.setQuantidade( 1000 );
        assertEquals( 1000, flecha.getQuantidade() );
    }
    
    @Test
    public void elfoNasceuCom2Flechas()
    {
        Elfo elfo = new Elfo( "Legolas" );
        Item flecha = elfo.getFlecha();
        assertEquals( 2, flecha.getQuantidade());
    }
    
    @Test
    public void elfoAtiraFlechaPerdeUmaUnidadeGanhaXP() {
        Elfo elfo = new Elfo( "Legolas" );
        Anao anao = new Anao( "Gimli" );
        elfo.atiraFlecha( anao );
        assertEquals( 1, elfo.getFlecha().getQuantidade() );
        assertEquals( 1, elfo.getExperiencia());
    }
    
    @Test
    public void elfoAtiraTresVezesGanhaDoisXP() {
        Elfo elfo = new Elfo( "Legolas" );
        Anao anao = new Anao( "Gimli" );
        for( int i = 0; i < 3; i++)
        {
            elfo.atiraFlecha( anao );
        }
        assertEquals( 0, elfo.getFlecha().getQuantidade() );
        assertEquals( 2, elfo.getExperiencia() );
    }
    
    @Test
    public void elfoAtiraFlechaAnaoTemEscudo() {
        Elfo elfo = new Elfo( "Legolas" );
        Anao anao = new Anao( "Gimli" );
        elfo.atiraFlecha( anao );
        assertEquals( 100, anao.getVida(), 1e-8 );
        anao.equiparEscudo();
        elfo.atiraFlecha( anao );
        assertEquals( 95, anao.getVida(), 1e-8 );
    }
    
    @Test
    public void elfoAtiraFlechaAnaoTentaEquiparEscudoMasNaoPossuiItem() {
        Elfo elfo = new Elfo( "Legolas" );
        Anao anao = new Anao( "Gimli" );
        elfo.atiraFlecha( anao );
        assertEquals( 100, anao.getVida(), 1e-8 );
        anao.perderItem( anao.getInventario().obterItemNaPosicao(0) );
        anao.equiparEscudo();
        elfo.atiraFlecha( anao );
        assertEquals( 90, anao.getVida(), 1e-8 );
    }
    
    @Test
    public void elfoAtiraVinteFlechasAnaoMorre()
    {
        Elfo elfo = new Elfo( "Legolas" );
        Anao anao = new Anao( "Gimli" );
        
        elfo.getFlecha().setQuantidade( 18 );
        
        for( int i = 0; i < 20; i++)
        {
            elfo.atiraFlecha( anao );
        }
        assertEquals( 0, elfo.getFlecha().getQuantidade() );
        assertEquals( 18, elfo.getExperiencia() );
        assertEquals( 0.0d , anao.getVida(), 0.000001d  );
    }
    
    @Test
    public void elfoGanhaDoisItens()
    {
        Elfo elfo = new Elfo("Legolas");
        Item pocao = new Item( 4, "Pocao");
        Item pao = new Item( 1, "Adaga" );
        elfo.ganharItem( pocao );
        elfo.ganharItem( pao );
        assertTrue( elfo.getInventario().getItens().contains( pocao ) );
        assertTrue( elfo.getInventario().getItens().contains( pao ) );
    }
    
    @Test
    public void elfoPerdeDoisItens()
    {
        Elfo elfo = new Elfo("Legolas");
        Item pocao = new Item( 4, "Pocao");
        Item pao = new Item( 1, "Adaga" );
        elfo.ganharItem( pocao );
        elfo.ganharItem( pao );
        assertTrue( elfo.getInventario().getItens().contains( pocao ) );
        assertTrue( elfo.getInventario().getItens().contains( pao ) );
        elfo.perderItem( pao );
        assertTrue( elfo.getInventario().getItens().contains( pocao ) );
        assertTrue( !elfo.getInventario().getItens().contains( pao ) );
    }
    
    @Test
    public void instancia10Elfos()
    {
        Elfo elfo1 = new Elfo("Legolas");
        ElfoDaLuz elfo2 = new ElfoDaLuz( "Legolas" );
        ElfoVerde elfo3 = new ElfoVerde("Legolas");
        ElfoNoturno elfo4 = new ElfoNoturno("Legolas");
        Elfo elfo5 = new Elfo("Legolas");
        ElfoDaLuz elfo6 = new ElfoDaLuz( "Legolas" );
        ElfoVerde elfo7 = new ElfoVerde("Legolas");
        ElfoNoturno elfo8 = new ElfoNoturno("Legolas");
        ElfoVerde elfo9 = new ElfoVerde("Legolas");
        ElfoNoturno elfo10 = new ElfoNoturno("Legolas");
        assertEquals( 10, Elfo.getQuantidade() );
    }
    
    @After
    public void tearDown()
    {
        System.gc();
    }
}
