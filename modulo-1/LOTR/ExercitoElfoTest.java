import java.util.*;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ExercitoElfoTest
{
    @Test
    public void exercitoRecebeuElfosCertos()
    {
        ExercitoElfo exercito = new ExercitoElfo();
        ElfoVerde elfoVerde = new ElfoVerde( "tchiun tchuin" );
        ElfoNoturno elfoNoturno = new ElfoNoturno( "tchiu flain" );
        ElfoDaLuz elfoLuz = new ElfoDaLuz( "jaiminho" );
        Elfo elfo = new Elfo( "quico" );
        exercito.alistarElfo( elfoVerde );
        exercito.alistarElfo( elfoNoturno );
        exercito.alistarElfo( elfoLuz );
        exercito.alistarElfo( elfo );
        assertTrue( exercito.buscarLista().contains( elfoVerde ) );
        assertTrue( exercito.buscarLista().contains( elfoNoturno ) );
        assertTrue( !exercito.buscarLista().contains( elfoLuz ) );
        assertTrue( !exercito.buscarLista().contains( elfo ) );
    }
    
    @Test
    public void exercitoBuscaListaDeElfosDeDeterminadoStatus()
    {
        Anao anao = new Anao("ze ze");
        ExercitoElfo exercito = new ExercitoElfo();
        ElfoVerde elfoVerde = new ElfoVerde( "tchiun tchuin" );
        ElfoNoturno elfoNoturno = new ElfoNoturno( "tchiu flain" );
        ElfoDaLuz elfoLuz = new ElfoDaLuz( "jaiminho" );
        Elfo elfo = new Elfo( "quico" );
        elfoVerde.atiraFlecha( anao );
        exercito.alistarElfo( elfoVerde );
        exercito.alistarElfo( elfoNoturno );
        exercito.alistarElfo( elfoLuz );
        exercito.alistarElfo( elfo );
        assertTrue( !exercito.buscarElfosTipo(Status.RECEM_CRIADO).contains( elfoVerde ) );
        assertTrue( exercito.buscarElfosTipo(Status.RECEM_CRIADO).contains( elfoNoturno ) );
        assertTrue( !exercito.buscarElfosTipo(Status.RECEM_CRIADO).contains( elfoLuz ) );
        assertTrue( !exercito.buscarElfosTipo(Status.RECEM_CRIADO).contains( elfo ) );
    }
    
    private ArrayList<ElfoVerde> gerarElfosVerdes( int quantia )
    {
        ArrayList<ElfoVerde> elfos = new ArrayList<>();
        for( int i = 0; i < quantia; i++ )
        {
            elfos.add( new ElfoVerde( "Elfo Verde " + i ) );
        }
        return elfos;
    }
    
    private ArrayList<ElfoNoturno> gerarElfosNoturnos( int quantia )
    {
        ArrayList<ElfoNoturno> elfos = new ArrayList<>();
        for( int i = 0; i < quantia; i++ )
        {
            elfos.add( new ElfoNoturno( "Elfo Verde " + i ) );
        }
        return elfos;
    }
    
    private ExercitoElfo gerarExercito( )
    {
        ArrayList<ElfoVerde> elfosVerdes = gerarElfosVerdes(10);
        ArrayList<ElfoNoturno> elfosNoturnos = gerarElfosNoturnos(10);
        ExercitoElfo exercito = new ExercitoElfo();
        for( ElfoNoturno elfo : elfosNoturnos )
        {
            exercito.alistarElfo( elfo );
        }
        for( ElfoVerde elfo : elfosVerdes )
        {
            exercito.alistarElfo( elfo );
        }
        return exercito;
    }
    
    @Test
    public void ataqueOrdenado()
    {
        ExercitoElfo exercito = gerarExercito();
        ArrayList<Elfo> ordenado = exercito.getOrdemDeAtaque( exercito.buscarLista() );
        boolean isNoturno = false;
        for( Elfo elfo : ordenado )
        {
            if(isNoturno)
            {
                System.out.println( "Esperando Noturno, get " + elfo.getClass() );
                assertEquals( ElfoNoturno.class, elfo.getClass() );
            }
            else
            {
                if( elfo.getClass() == ElfoNoturno.class )
                {
                    isNoturno = true;
                }
                else
                {
                    System.out.println( "Esperando Verde, get " + elfo.getClass() );
                    assertEquals( ElfoVerde.class, elfo.getClass() );
                }
            }
        }
    }
    
    @Test
    public void ataqueAlternado()
    {
        ExercitoElfo exercito = gerarExercito();
        ArrayList<Elfo> ordenado = exercito.getOrdemAlternadaDeAtaque( ElfoVerde.class );
        boolean isNoturno = false;
        for( Elfo elfo : ordenado )
        {
            if(isNoturno)
            {
                assertEquals( ElfoNoturno.class, elfo.getClass() );
            }
            else
            {
                assertEquals( ElfoVerde.class, elfo.getClass() );
            }
            isNoturno = !isNoturno;
        }
    }
    
    @Test
    public void exercitoAtacaAnaoMorre()
    {
        Anao anao = new Anao( "Gimli" );
        ExercitoElfo exercito = gerarExercito();
        ArrayList<Elfo> ordenado = exercito.getOrdemAlternadaDeAtaque( ElfoVerde.class );
        exercito.Atacar( anao, ordenado );
        assertEquals( Status.MORTO, anao.getStatus() );
    }
    
    @Test
    public void ordenaExercitoPorFlechasCom30PorCentoNoturno()
    {
        ExercitoElfo exercito = gerarExercito();
        ArrayList<Elfo> ordenado = exercito.getOrdemPorFlechaDeAtaque();
        Elfo lastElfo = null;
        int qtdVerde = 0;
        int qtdNoturno = 0;;
        double proporcao;
        
        for( Elfo elfo : ordenado )
        {
            if( elfo.getClass() == ElfoVerde.class )
            {
                qtdVerde++;
            }
            else
            {
                qtdNoturno++;
            }
            if( lastElfo != null )
                assertTrue( lastElfo.getFlecha().getQuantidade() 
                >= elfo.getFlecha().getQuantidade()  );
            lastElfo = elfo;
        }
        System.out.println( "Elfos Noturnos = " + qtdNoturno );
        System.out.println( "Elfos Verdes = " + qtdVerde );
        proporcao = (double)qtdNoturno/(qtdNoturno + qtdVerde);
        assertTrue( 0.30 > proporcao );
    }
}
