public class Anao extends Personagem{
    private boolean escudo;
        
    public Anao( String nome ) {
        super( nome );
        this.vida = 110.0;
        this.escudo = false;
        this.ganharItem( new Item( 1, "Escudo" ) );
        receivedDamage = 10.0d;
    }
 
    public void equiparEscudo( )
    {
        if( this.getInventario().obterItemPelaDescricao("Escudo") != null )
        {
            this.escudo = !escudo;
        }
    }
        
    public void damage( double damage )
    {
        damage /= this.escudo ? 2 : 1;
        if( status != Status.MORTO )
        {
            this.vida -= damageLimit( this.vida, damage );
            status = this.vida > 0 ? Status.SOFREU_DANO : Status.MORTO;
        }
    }
}
