import java.util.*;

public class EstatisticasInventario
{
    private Inventario mochila;
    
    public EstatisticasInventario( Inventario inventario )
    {
        this.mochila = inventario;
    }
    
    public double calcularMedia()
    {
        double total = 0;
        if( this.mochila.getSlots() > 0  )
        {
            for( int i = 0; i < this.mochila.getSlots(); i++ )
            {
                total += this.mochila.obterItemNaPosicao(i).getQuantidade();
            }
            total /= this.mochila.getSlots();
            return total;
        }
        return Double.NaN;
    }
    
    private boolean isPair()
    {
        return this.mochila.getSlots() % 2 == 0;
    }
    
    public double calcularMediana()
    {
        if( this.mochila.getSlots() > 0 )
        {
            if(isPair())
            {
                double valueA, valueB;
                valueA = this.mochila.obterItemNaPosicao( this.mochila.getSlots()/2 )
                .getQuantidade();
                valueB = this.mochila.obterItemNaPosicao( (this.mochila.getSlots()/2) - 1 )
                .getQuantidade();
                return (valueA+valueB)/2;
            }   
            double quantidade = this.mochila.obterItemNaPosicao( (this.mochila.getSlots() - 1)/2 )
            .getQuantidade();
            return quantidade;
        }
        return Double.NaN;
    }
    
    public int qtdItensAcimaDaMedia()
    {
        int total = 0;
        double media = calcularMedia();
        for( int i = 0; i < this.mochila.getSlots(); i++ )
        {
            if( this.mochila.obterItemNaPosicao(i).getQuantidade() > media  )
                total++;
        }        
        return total;
    }
    
    
}
