import java.util.*;

public class Inventario
{
    private ArrayList<Item> itens;  
    
    public Inventario()
    {
        itens = new ArrayList<>();
    }
    
    public ArrayList<Item> getItens()
    {
        return this.itens;
    }
    
    public int getSlots()
    {
        return this.itens.size();
    }
    
    public void adicionarItem(Item item)
    {
            this.itens.add(item);
    }
    
    public Item obterItemNaPosicao(int index)
    {
        return index < getSlots() ? itens.get(index) : null;
    }
    
    public Item obterItemPelaDescricao( String descricao )
    {
        for( Item objeto : itens )
        {
            if( objeto.getDescricao().equals(descricao) )
            {
                return objeto;
            }
        }
        return null;
    }

    private boolean validarItemNaPosicao( int index )
    {
        return index < getSlots(); 
    }
    
    public void removerItem( Item item )
    {
        if( this.itens.contains(item)  )
            itens.remove(item);
    }
    
    
    public void removerItemNaPosicao( int index )
    {
        if(validarItemNaPosicao(index))
            itens.remove(index);
    }
    
    public void removerItemPorDescricao( String Descricao )
    {
        itens.remove( obterItemPelaDescricao( Descricao ) );
    }
    
    public String getDescricoesTodosItens() 
    {
        StringBuilder listaItens = new StringBuilder();
        for(int i = 0; i < getSlots(); i++)
        {
            listaItens.append( obterItemNaPosicao(i).getDescricao() + "," );
        }
        listaItens.deleteCharAt( listaItens.length() - 1 );
        return listaItens.toString();
    }
    
    public Item getItemMaisAbundante()
    {
        int index = 0;
        int quantidade = 0;
        boolean isMochilaEmpty = true;
        
        for( int i = 0; i < getSlots(); i++ )
        {
            if( obterItemNaPosicao(i) != null )
            {
                isMochilaEmpty = false;
                if( obterItemNaPosicao(i).getQuantidade() > quantidade )
                {
                    quantidade = obterItemNaPosicao(i).getQuantidade();
                    index = i;
                }
            }
        }
        
        return this.itens.size() > 0 && !isMochilaEmpty ? obterItemNaPosicao(index) : null;
    }
    
    public ArrayList<Item> inverterInventario( )
    {
        ArrayList<Item> inverso = new ArrayList<>();
        for( int i = this.getSlots() - 1; i >= 0; i-- )
        {
            inverso.add( this.obterItemNaPosicao(i) );
        }
        return inverso;
    }
    
    private void trocar( int pos, ArrayList<Item> ordenado, TipoOrdenacao tipo )
    {
        if( tipo == TipoOrdenacao.ASC )
        {
            if( (pos+1) <  getSlots() && 
            ordenado.get(pos).getQuantidade() > ordenado.get(pos+1).getQuantidade()  )
            {
                Item temporario = obterItemNaPosicao(pos);
                ordenado.set( pos, ordenado.get(pos+1));
                ordenado.set( pos+1, temporario);
            }
        }
        else
        {
            if( (pos+1) <  getSlots() && 
            ordenado.get(pos).getQuantidade() < ordenado.get(pos+1).getQuantidade()  )
            {
                Item temporario = obterItemNaPosicao(pos);
                ordenado.set( pos, ordenado.get(pos+1));
                ordenado.set( pos+1, temporario);
            }
        }
    }
        
    public void ordenarItensQuantidadeAscendente()
    {
        ArrayList<Item> ordenado = this.itens;
        for( int j = 0; j < getSlots(); j++ )
        {
            for( int i = 0; i < getSlots() - 1; i++ )
            {
                    trocar( i, ordenado, TipoOrdenacao.ASC );
            }
        }
        this.itens = ordenado;
    }
    
    public void ordenarItens( TipoOrdenacao tipo )
    {
        ArrayList<Item> ordenado = this.itens;
        Item temporario;
        for( int j = 0; j < getSlots(); j++ )
        {
            for( int i = 0; i < getSlots() - 1; i++ )
            {
                if( tipo == TipoOrdenacao.ASC )
                {
                    trocar( i, ordenado, TipoOrdenacao.ASC );
                }
                else
                {
                    trocar( i, ordenado, TipoOrdenacao.DESC );
                }
            }
        }
        this.itens = ordenado;
    }
}
