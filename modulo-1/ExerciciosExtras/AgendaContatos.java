import java.util.*;

public class AgendaContatos
{
    private HashMap< String, String > contatos;
    
    public AgendaContatos()
    {
        contatos = new LinkedHashMap<>();
    }
    
    public void adicionar( String nome, String numero )
    {
        contatos.put( nome, numero );
    }
    
    public String buscarPeloNome( String nome )
    {
        return contatos.get(nome);
    }
    
    public String buscarPeloTelefone( String numero )
    {
        for(  HashMap.Entry<String, String> par : contatos.entrySet() )
        {
            if( par.getValue().equals( numero ) )
                return par.getKey();
        }
        return null;
    }
       
    public String csv()
    {
        StringBuilder csv = new StringBuilder();
        String separador = System.lineSeparator();
        
        for( HashMap.Entry<String, String> par : contatos.entrySet() )
        {
            String chave = par.getKey();
            String valor = par.getValue();
            String contato = String.format( "%s, %s%s", chave, valor, separador );
            csv.append(contato);
        }
        return csv.toString();
    }
}
