import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class AgendaContatosTest
{
    @Test
    public void adicionarContatos()
    {
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar( "Andresito", "0800 1234" );
        agenda.adicionar( "Brunao", "3333 3333" );
        assertEquals( "0800 1234", agenda.buscarPeloNome("Andresito") );
        assertEquals( "3333 3333", agenda.buscarPeloNome("Brunao") );
    }
    
    @Test
    public void CVS()
    {
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar( "Brunao", "3333 3333" );
        agenda.adicionar( "Andresito", "0800 1234" );
        agenda.adicionar( "Guilherme", "0800 9696" );
        agenda.adicionar( "Carlitos", "0800 1018" );
        assertEquals( 
        "Andresito, 0800 1234\nBrunao, 3333 3333\nCarlitos, 0800 1018\nGuilherme, 0800 9696"
        , agenda.csv() );
    }
}
