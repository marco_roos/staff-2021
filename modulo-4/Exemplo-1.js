/*
===================================================================================================================
   VEM SER - DBC ( Módulo 4 - Exemplo 1 / Criação e Busca de coleções e dados )
   
===================================================================================================================
*/

/*

** Cria uma nova coleção
db.createCollection("nome_da_colecao");

**Encontra tudo de uma determinada coleção e mostra em forma de texto
db.nome_da_colecao.find();

**Encontra tudo de uma determinada coleção e mostra em forma de tabela
db.banco.find().pretty();

**Cria uma tabela para ser inserida em uma coleção ( uma variável, como que um objeto )
umaTabela = { 
        codigo:0001,
        nome:"alfa",
        agencia: [{
            nome:"web"
        },{
            nome:"web1"
        }
]};

**Insere a tabela criada anteriormente
db.nome_da_colecao.insert( umArray );
*/    
    

    