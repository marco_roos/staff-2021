/*
===================================================================================================================
   VEM SER - DBC ( Módulo 4 - Exemplo 1 / Updates )
   
===================================================================================================================
*/

/*
    $set      = substitui o valor de um campo
    $set { campo : valor }
    
    $unset    = apaga um campo
    $unset { campo : "" }
    
    $inc      = incrementa o campo por um valor especificado
    $inc { campo : incremento }
    
    $mul      = multiplica o campo por um valor especificado
    $mul { campo : multiplicador }
    
    $rename
    $rename { campo : novo_nome_do_campo }


**Atualiza os dados de um objeto, nesse caso, mudando o valor de um campo
db.banco.update( { _id: ObjectId("60bfaa8a83d283dadd47e4fb") }, { $set: { nome: "alfa 2" } } );

**Deleta um único documento específico de uma coleção 
db.banco.deleteOne({ nome: "alfa 2" });
*/



