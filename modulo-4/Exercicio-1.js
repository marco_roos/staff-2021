/*
===================================================================================================================
   VEM SER - DBC ( Módulo 4 - Exercício 1 / Arquivo JSON básico )
   
    Enunciado:
    Pegar o arquivo de criação dos bancos e agencias como enderços do sql ( Módulo 2 ) e criar todos em formato 
    noSQL sem relacionamentos.
===================================================================================================================
*/


enderecoAgencia = [  
    {
        logradouro: 'Rua Testando',
        numero: 55,
        complemento: 'Loja 1',
        bairro: 'NA',
        cidade: 'NA',
        estado: 'NA',
        pais: 'Brasil'
    },
    {
        logradouro: 'Rua Testando',
        numero: 55,
        complemento: 'Loja 2',
        bairro: 'NA',
        cidade: 'NA',
        estado: 'NA',
        pais: 'Brasil'
    },
    {
        logradouro: 'Rua Testando',
        numero: 55,
        complemento: 'Loja 3',
        bairro: 'NA',
        cidade: 'NA',
        estado: 'NA',
        pais: 'Brasil'
    },
    {
        logradouro: 'Rua Testing',
        numero: 122,
        complemento: '',
        bairro: 'Between Hyde and Powell Streets',
        cidade: 'San Francisco',
        estado: 'California',
        pais: 'EUA'
    },
    {
        logradouro: 'Rua Testing',
        numero: 525,
        complemento: '',
        bairro: 'Croydon',
        cidade: 'Londres',
        estado: 'Boroughs',
        pais: 'England'
    },
    {
        logradouro: 'Rua Testing',
        numero: 525,
        complemento: '',
        bairro: 'Croydon',
        cidade: 'Londres',
        estado: 'Boroughs',
        pais: 'England'
    },
    {
        logradouro: 'Rua do meio',
        numero: 2233,
        complemento: '',
        bairro: 'Qualquer',
        cidade: 'Itu',
        estado: 'São Paulo',
        pais: 'Brasil'
    },
    {
        logradouro: 'Rua Caminito',
        numero: 222,
        complemento: '',
        bairro: 'Caminito',
        cidade: 'Bueno Aires',
        estado: 'Buernos Aires',
        pais: 'Argentina'
    }
]

enderecoCliente = [
    {
        logradouro: 'Rua andresito',
        numero: 1,
        complemento: '',
        bairro: 'sao andre',
        cidade: 'andrelandia',
        estado: 'andre do norte',
        pais: 'andrestão'
    },
    {
        logradouro: 'Rua Brunito',
        numero: 2,
        complemento: '',
        bairro: 'sao bruno',
        cidade: 'brunolandia',
        estado: 'bruno do sul',
        pais: 'bunestão'
    },
    {
        logradouro: 'Rua carlito',
        numero: 3,
        complemento: '',
        bairro: 'sao carlos',
        cidade: 'carlandia',
        estado: 'carlos do norte',
        pais: 'carlestão'
    },
    {
        logradouro: 'Rua denisito',
        numero: 4,
        complemento: '',
        bairro: 'sao denis',
        cidade: 'denislandia',
        estado: 'denis do sul',
        pais: 'denistão'
    },
    {
        logradouro: 'Rua emanuelito',
        numero: 5,
        complemento: '',
        bairro: 'sao emanuel',
        cidade: 'emanuelandia',
        estado: 'emanuel do norte',
        pais: 'emanuelão'
    },
    {
        logradouro: 'Rua felipito',
        numero: 6,
        complemento: '',
        bairro: 'sao felipe',
        cidade: 'felipelandia',
        estado: 'felipe do sul',
        pais: 'felipestão'
    },
    {
        logradouro: 'Rua gabrielito',
        numero: 7,
        complemento: '',
        bairro: 'sao gabriel',
        cidade: 'gabrielito',
        estado: 'gabriel do norte',
        pais: 'gabrielão'
    },
    {
        logradouro: 'Rua heriquito',
        numero: 10,
        complemento: '',
        bairro: 'sao heric',
        cidade: 'heriquelandia',
        estado: 'heric do sul',
        pais: 'hericstão'
    },
    {
        logradouro: 'Rua igorsito',
        numero: 11,
        complemento: '',
        bairro: 'sao igor',
        cidade: 'igorlandia',
        estado: 'igor do norte',
        pais: 'igorstão'
    },
    {
        logradouro: 'Rua joaosito',
        numero: 12,
        complemento: '',
        bairro: 'sao joao',
        cidade: 'joelandia',
        estado: 'joao do norte',
        pais: 'joastão'
    }
]

movimentacoes = [
    {
        tipo : 'crédito',
        valor : 10000.00
    },
    {
        tipo : 'débito',
        valor : 150.00
    },
    {
        tipo : 'crédito',
        valor : 3000.00
    },
    {
        tipo : 'crédito',
        valor : 5600.00
    },
    {
        tipo : 'débito',
        valor : 500.00
    }
]

clientes = [
    {
        cpf: '123.456.789-09',
        nome: 'andre pelé',
        estado_civil: 'solteiro',
        data_nascimento: '01/02/1993',
        endereco: enderecoCliente[0],
        movimentacao : [ movimentacoes[0], movimentacoes[4] ]
    }, 
    {
        cpf: '123.456.789-10',
        nome: 'bruno nulo',
        estado_civil: 'casado',
        data_nascimento: '02/03/1994',
        endereco: enderecoCliente[1],
        movimentacao : [ movimentacoes[0], movimentacoes[1] ]
    },
    {
        cpf: '123.456.789-11',
        nome: 'carlos talos',
        estado_civil: 'união estável',
        data_nascimento: '03/04/1995',
        endereco: enderecoCliente[2],
        movimentacao : [ movimentacoes[2], movimentacoes[4] ]
    },
    {
        cpf: '123.456.789-12',
        nome: 'denis tenis',
        estado_civil: 'solteiro',
        data_nascimento: '04/05/1996',
        endereco: enderecoCliente[3],
        movimentacao : [ movimentacoes[0], movimentacoes[3] ]
    },
    {
        cpf: '123.456.789-13',
        nome: 'emanuel pastel',
        estado_civil: 'casado',
        data_nascimento: '05/06/1997',
        endereco: enderecoCliente[4],
        movimentacao : [ movimentacoes[1], movimentacoes[2] ]
    },
    {
        cpf: '123.456.789-14',
        nome: 'felipe lipe',
        estado_civil: 'casado',
        data_nascimento: '06/07/1998',
        endereco: enderecoCliente[5],
        movimentacao : [ movimentacoes[1], movimentacoes[3] ]
    },
    {
        cpf: '123.456.789-15',
        nome: 'gabriel copel',
        estado_civil: 'solteiro',
        data_nascimento: '07/08/1999',
        endereco: enderecoCliente[6],
        movimentacao : [ movimentacoes[1], movimentacoes[2], movimentacoes[4] ]
    },
    {
        cpf: '123.456.789-16',
        nome: 'hector lector',
        estado_civil: 'casado',
        data_nascimento: '08/09/1990',
        endereco: enderecoCliente[7],
        movimentacao : [ movimentacoes[0], movimentacoes[3], movimentacoes[4] ]
    },
    {
        cpf: '123.456.789-17',
        nome: 'Igor dao',
        estado_civil: 'viúvo',
        data_nascimento: '09/10/1991',
        endereco: enderecoCliente[8],
        movimentacao : [ movimentacoes[1], movimentacoes[3], movimentacoes[4] ]
    },
    {
        cpf: '123.456.789-18',
        nome: 'joão bolão',
        estado_civil: 'união estável',
        data_nascimento: '10/11/1992',
        endereco: enderecoCliente[9],
        movimentacao : [ movimentacoes[0], movimentacoes[2], movimentacoes[3] ]
    }
]

gerentes = [
    {
        dados_pessoais: clientes[0],
        codigo_funcionario: '00001',
        tipo : 'GG'
    },
    {
        dados_pessoais: clientes[2],
        codigo_funcionario: '00002',
        tipo : 'GC'
    },
    {
        dados_pessoais: clientes[4],
        codigo_funcionario: '00003',
        tipo : 'GC'
    },
    {
        dados_pessoais: clientes[6],
        codigo_funcionario: '00004',
        tipo : 'GC'
    }
]

consolidacoes = [
    {
        codigo: '00001',
        saldo: 10000.00,
        saques: 5000.00,
        depositos: 25000.00,
        correstinstas: 10
    },
    {
        codigo: '00002',
        saldo: -150.00,
        saques: 25000.00,
        depositos: 5.00,
        correstinstas: 10
    },
    {
        codigo: '00003',
        saldo: 1500.00,
        saques: 152.00,
        depositos: 36554.00,
        correstinstas: 10
    },
    {
        codigo: '00004',
        saldo: -560.00,
        saques: 856.00,
        depositos: 4521.00,
        correstinstas: 10
    },
    {
        codigo: '00005',
        saldo: 12584.00,
        saques: 73524.00,
        depositos: 1235.00,
        correstinstas: 10
    },
    {
        codigo: '00006',
        saldo: 1254.00,
        saques: 36434.00,
        depositos: -4532.00,
        correstinstas: 10
    },
    {
        codigo: '00007',
        saldo: 1235.00,
        saques: 12354.00,
        depositos: 13236.00,
        correstinstas: 10
    }
]

contas = [
    {
        codigo_conta: '00001',
        tipo: 'PJ',
        clientes: [ clientes[0], clientes[1], clientes[2] ],
        gerente: gerentes[0]
    },
    {
        codigo_conta: '00002',
        tipo: 'conjunta',
        clientes: [ clientes[3], clientes[4] ],
        gerente: gerentes[1]
    },
    {
        codigo_conta: '00003',
        tipo: 'conjunta',
        clientes: [ clientes[5], clientes[6] ],
        gerente: gerentes[2]
    },
    {
        codigo_conta: '00004',
        tipo: 'conjunta',
        clientes: [ clientes[7], clientes[8] ],
        gerente: gerentes[3]
    }
]

agencias = [
    {
        nome: 'Web',
        codigo: '0001',
        endereco: enderecoAgencia[0],
        consolidacao: consolidacoes[0],
        contas : [ contas[0], contas[3] ]
    },
    {
        nome: 'California',
        codigo: '0002',
        endereco: enderecoAgencia[3],
        consolidacao: consolidacoes[3],
        contas : [ contas[1] ]
    },
    {
        nome: 'Londres',
        codigo: '0101',
        endereco: enderecoAgencia[4],
        consolidacao: consolidacoes[4],
        contas : [ contas[2] ]
    },
    {
        nome: 'Web',
        codigo: '0001',
        endereco: enderecoAgencia[1],
        consolidacao: consolidacoes[1],
        contas : [ contas[0], contas[1] ]
    },
    {
        nome: 'Web',
        codigo: '0001',
        endereco: enderecoAgencia[2],
        consolidacao: consolidacoes[2],
        contas : [ contas[2], contas[3] ]
    },
    {
        nome: 'Itu',
        codigo: '8761',
        endereco: enderecoAgencia[5],
        consolidacao: consolidacoes[5],
        contas : [ contas[0] ]
    },
    {
        nome: 'Hermana',
        codigo: '4567',
        endereco: enderecoAgencia[6],
        consolidacao: consolidacoes[6],
        contas : [ contas[1] ]
    }
]

bancos = [
    {
        nome: 'alfa',
        codigo: '001',
        agencias: [ agencias[0], agencias[1], agencias[2]]
    },
    {
        nome: 'beta',
        codigo: '241',
        agencias: [ agencias[3]]
    },
    {
        nome: 'omega',
        codigo: '307',
        agencias: [ agencias[4], agencias[5], agencias[6]]
    }
]

db.createCollection("banco")

db.banco.insert(bancos[0])
db.banco.insert(bancos[1])
db.banco.insert(bancos[2])

db.banco.find().sort( { "agencias.consolidacao.saldo" : -1 } )


//db.banco.find().pretty()
//db.banco.find().sort({ agencias.consolidacao.saldo: -1 });

//db.banco.drop();
