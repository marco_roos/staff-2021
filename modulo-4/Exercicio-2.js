/*
===================================================================================================================
   VEM SER - DBC ( Módulo 4 - Exercício 2 / Dados Normalizados )
   
    O funcionamento apropriado deste exercício fica submetido a execução de cada trecho responsável por criar
determinada coleção e o preenchimento adequado dos trechos a seguir com a identidade das linhas criadas das 
coleções anteriores.
===================================================================================================================
*/


/*
db.createCollection( "Pais" );
db.Pais.insert( [
    {
        nome: "Brasil"
    },
    {
        nome: "Argentina"
    },
    {
        nome: "EUA"
    },
    {
        nome: "Londres"
    },
    {
        nome: "Clientestão"
    } ]
);
db.Pais.find();


db.createCollection( "Estado" );
db.Estado.insert([
    {
        nome: "NA",
        pais : ObjectId("60bfd7571484c00768943453")
    },
    {
        nome: "São Paulo",
        pais : ObjectId("60bfd7571484c00768943453")
    },
    {
        nome: "Buenos Aires",
        pais : ObjectId("60bfd7571484c00768943454")
    },
    {
        nome: "California",
        pais : ObjectId("60bfd7571484c00768943455")
    },
    {
        nome: "Boroughs",
        pais : ObjectId("60bfd7571484c00768943456")
    },
    {
        nome: "Cliente do SUl",
        pais : ObjectId("60bfd7571484c00768943457")
    }
]);
db.Estado.find();


db.createCollection( "Cidade" );
db.Cidade.insert([
    {
        nome: "NA",
        estado : ObjectId("60bfef311484c00768943458")
    },
    {
        nome: "Itu",
        estado : ObjectId("60bfef311484c00768943459")
    },
    {
        nome: "Buenos Aires",
        estado : ObjectId("60bfef311484c0076894345a")
    },
    {
        nome: "San Francisco",
        estado : ObjectId("60bfef311484c0076894345b")
    },
    {
        nome: "Londres",
        estado : ObjectId("60bfef311484c0076894345c")
    },
    {
        nome: "Clientelândia",
        estado : ObjectId("60bfef311484c0076894345d")
    }
]);
db.Cidade.find();


db.createCollection( "Bairro" );
db.Bairro.insert([
{
        nome: "NA",
        cidade : ObjectId("60bff4fc1484c00768943460")
    },
    {
        nome: "Qualquer",
        cidade : ObjectId("60bff4fc1484c00768943461")
    },
    {
        nome: "Caminito",
        cidade : ObjectId("60bff4fc1484c00768943462")
    },
    {
        nome: "Between Hyde and Powell Streets",
        cidade : ObjectId("60bff4fc1484c00768943463")
    },
    {
        nome: "Croydon",
        cidade : ObjectId("60bff4fc1484c00768943464")
    },
    {
        nome: "vila clientão",
        cidade : ObjectId("60bff4fc1484c00768943465")
    } 
]);
db.Bairro.find();


db.createCollection( "Endereco" );
db.Endereco.insert([
    {
        logradouro: "Rua Testando",
        numero: 55,
        complemento: 'Loja 1',
        cidade : ObjectId("60bff8011484c00768943467")
    },
    {
        logradouro: "Rua Testando",
        numero: 55,
        complemento: 'Loja 2',
        cidade : ObjectId("60bff8011484c00768943467")
    },
    {
        logradouro: "Rua Testando",
        numero: 55,
        complemento: 'Loja 3',
        cidade : ObjectId("60bff8011484c00768943467")
    },
    {
        logradouro: "Rua do meio",
        numero: 2233,
        //complemento: '',
        cidade : ObjectId("60bff8011484c00768943468")
    },
    {
        logradouro: "Rua caminito",
        numero: 222,
        //complemento: '',
        cidade : ObjectId("60bff8011484c00768943469")
    },
    {
        logradouro: "Rua testing",
        numero: 122,
        //complemento: '',
        cidade : ObjectId("60bff8011484c0076894346a")
    },
    {
        logradouro: "Rua testing",
        numero: 525,
        //complemento: '',
        cidade : ObjectId("60bff8011484c0076894346b")
    },
    {
        logradouro: "Rua clientela",
        numero: 666,
        //complemento: '',
        cidade : ObjectId("60bff8011484c0076894346c")
    }
]);
db.Endereco.find();


db.createCollection( "Movimentacao" );
db.Movimentacao.insert( [
    {
        tipo: "credito",
        valor: 1250.00
    },
    {
        tipo: "debito",
        valor: 590.50
    },
    {
        tipo: "debito",
        valor: 158.25
    } ]
);  
db.Movimentacao.find();  


db.createCollection( "Cliente" );
db.Cliente.insert([
    {
        cpf: '123.456.789-09',
        nome: 'andre pelé',
        estado_civil: 'solteiro',
        data_nascimento: '01/02/1993',
        endereco: ObjectId("60bffa5a1484c00768943474"),
        movimentacoes : [ ObjectId("60bffbb61484c00768943475") ,ObjectId("60bffbb61484c00768943477")]
    },
    {
        cpf: '123.456.789-10',
        nome: 'bruno nulo',
        estado_civil: 'casado',
        data_nascimento: '02/03/1994',
        endereco: ObjectId("60bffa5a1484c00768943474"),
        movimentacoes : [ ObjectId("60bffbb61484c00768943475") ,ObjectId("60bffbb61484c00768943477")]
    },
    {
        cpf: '123.456.789-11',
        nome: 'carlos talos',
        estado_civil: 'união estável',
        data_nascimento: '03/04/1995',
        endereco: ObjectId("60bffa5a1484c00768943474"),
        movimentacoes : [ ObjectId("60bffbb61484c00768943475") ,ObjectId("60bffbb61484c00768943477")]
    },
    {
        cpf: "123.456.789-12",
        nome: 'denis tenis',
        estado_civil: 'solteiro',
        data_nascimento: '04/05/1996',
        endereco: ObjectId("60bffa5a1484c00768943474"),
        movimentacoes : [ ObjectId("60bffbb61484c00768943475") ,ObjectId("60bffbb61484c00768943476")]
    },
    {
        cpf: "123.456.789-13",
        nome: 'emanuel pastel',
        estado_civil: 'casado',
        data_nascimento: '05/06/1997',
        endereco: ObjectId("60bffa5a1484c00768943474"),
        movimentacoes : [ ObjectId("60bffbb61484c00768943476") ,ObjectId("60bffbb61484c00768943477")]
    },
    {
        cpf: "123.456.789-14",
        nome: 'felipe lipe',
        estado_civil: 'casado',
        data_nascimento: '06/07/1998',
        endereco: ObjectId("60bffa5a1484c00768943474"),
        movimentacoes : [ ObjectId("60bffbb61484c00768943475") ,ObjectId("60bffbb61484c00768943476")]
    },
    {
        cpf: "123.456.789-15",
        nome: 'gabriel copel',
        estado_civil: 'solteiro',
        data_nascimento: '07/08/1999',
        endereco: ObjectId("60bffa5a1484c00768943474"),
        movimentacoes : [ ObjectId("60bffbb61484c00768943475") ,ObjectId("60bffbb61484c00768943477")]
    },
    {
        cpf: "123.456.789-16",
        nome: 'hector lector',
        estado_civil: 'casado',
        data_nascimento: '08/09/1990',
        endereco: ObjectId("60bffa5a1484c00768943474"),
        movimentacoes : [ ObjectId("60bffbb61484c00768943476") ,ObjectId("60bffbb61484c00768943477")]
    },
    {
        cpf: "123.456.789-17",
        nome: 'Igor dao',
        estado_civil: 'viúvo',
        data_nascimento: '09/10/1991',
        endereco: ObjectId("60bffa5a1484c00768943474"),
        movimentacoes : [ ObjectId("60bffbb61484c00768943475") ,ObjectId("60bffbb61484c00768943476")]
    },
    {
        cpf: "123.456.789-18",
        nome: 'joão bolão',
        estado_civil: 'união estável',
        data_nascimento: '10/11/1992',
        endereco: ObjectId("60bffa5a1484c00768943474"),
        movimentacoes : [ ObjectId("60bffbb61484c00768943475") ,ObjectId("60bffbb61484c00768943477")]
    }
]);
db.Cliente.find(); 


db.createCollection( "Gerente" );
db.Gerente.insert([
    {
        dados_pessoais : ObjectId("60bffd581484c00768943479"),
        cod_funcionario : 0001,
        tipo : 'GG'
    },
    {
        dados_pessoais : ObjectId("60bffd581484c0076894347b"),
        cod_funcionario : 0002,
        tipo : 'GC'
    },
    {
        dados_pessoais : ObjectId("60bffd581484c0076894347d"),
        cod_funcionario : 0003,
        tipo : 'GC'
    },
    {
        dados_pessoais : ObjectId("60bffd581484c0076894347f"),
        cod_funcionario : 0004,
        tipo : 'GC'
    }
]);
db.Gerente.find(); 


db.createCollection( "Conta" );
db.Conta.insert([
    {
        tipo: "PJ",
        clientes: [ ObjectId("60bffd581484c00768943479"), ObjectId("60bffd581484c0076894347a"), ObjectId("60bffd581484c0076894347b") ],
        gerentes: ObjectId("60bfffa31484c00768943483")
    },
    {
        tipo: "Conjunta",
        clientes: [ ObjectId("60bffd581484c0076894347c"), ObjectId("60bffd581484c0076894347d") ],
        gerentes: ObjectId("60bfffa31484c00768943484")
    },
    {
        tipo: "Conjunta",
        clientes: [ ObjectId("60bffd581484c0076894347e"), ObjectId("60bffd581484c0076894347f") ],
        gerentes: ObjectId("60bfffa31484c00768943485")
    },
    {
        tipo: "Conjunta",
        clientes: [ ObjectId("60bffd581484c00768943480"), ObjectId("60bffd581484c00768943481") ],
        gerentes: ObjectId("60bfffa31484c00768943486")
    }
]);
db.Conta.find();


db.createCollection( "Consolidacao" );
db.Consolidacao.insert([
    {
        saldo: 135800.00,
        saques: 25600.00,
        depositos: 160500.00,
        correntistas: 10
    },
    {
        saldo: 75727.00,
        saques: 250.00,
        depositos: 7527.00,
        correntistas: 10
    },
    {
        saldo: 21278.00,
        saques: 25600.00,
        depositos: 160500.00,
        correntistas: 10
    },
    {
        saldo: 29752.00,
        saques: 252797.00,
        depositos: 160500.00,
        correntistas: 10
    },
    {
        saldo: 27975.00,
        saques: 49275.00,
        depositos: 729752.00,
        correntistas: 10
    },
    {
        saldo: 7966.00,
        saques: 279255.00,
        depositos: 279545.00,
        correntistas: 10
    },
    {
        saldo: 967353.00,
        saques: 7527979.00,
        depositos: 272752.00,
        correntistas: 10
    }
]);
db.Consolidacao.find();


db.createCollection( "Agencia" );
db.Agencia.insert([
    {
        nome: "Web1",
        codigo: "0001",
        endereco : ObjectId("60bffa5a1484c0076894346d"),
        consolidacao : ObjectId("60c002681484c0076894348d"),
        contas: [ ObjectId("60c000df1484c00768943488"), ObjectId("60c000df1484c00768943489"), ObjectId("60c000df1484c0076894348a"), ObjectId("60c000df1484c0076894348b") ]
    },
    {
        nome: "California",
        codigo: "0002",
        endereco : ObjectId("60bffa5a1484c00768943472"),
        consolidacao : ObjectId("60c002681484c0076894348e"),
        contas: [ ObjectId("60c000df1484c00768943488"), ObjectId("60c000df1484c00768943489"), ObjectId("60c000df1484c0076894348a"), ObjectId("60c000df1484c0076894348b") ]
    },
    {
        nome: "Londres",
        codigo: "0101",
        endereco : ObjectId("60bffa5a1484c00768943473"),
        consolidacao : ObjectId("60c002681484c0076894348f"),
        contas: [ ObjectId("60c000df1484c00768943488"), ObjectId("60c000df1484c00768943489"), ObjectId("60c000df1484c0076894348a"), ObjectId("60c000df1484c0076894348b") ]
    },
    {
        nome: "Web2",
        codigo: "0001",
        endereco : ObjectId("60bffa5a1484c0076894346e"),
        consolidacao : ObjectId("60c002681484c00768943490"),
        contas: [ ObjectId("60c000df1484c00768943488"), ObjectId("60c000df1484c00768943489"), ObjectId("60c000df1484c0076894348a"), ObjectId("60c000df1484c0076894348b") ]
    },
    {
        nome: "Web3",
        codigo: "0001",
        endereco : ObjectId("60bffa5a1484c0076894346f"),
        consolidacao : ObjectId("60c002681484c00768943491"),
        contas: [ ObjectId("60c000df1484c00768943488"), ObjectId("60c000df1484c00768943489"), ObjectId("60c000df1484c0076894348a"), ObjectId("60c000df1484c0076894348b") ]
    },
    {
        nome: "Itu",
        codigo: "8761",
        endereco : ObjectId("60bffa5a1484c00768943470"),
        consolidacao : ObjectId("60c002681484c00768943492"),
        contas: [ ObjectId("60c000df1484c00768943488"), ObjectId("60c000df1484c00768943489"), ObjectId("60c000df1484c0076894348a"), ObjectId("60c000df1484c0076894348b") ]
    },
    {
        nome: "Hermana",
        codigo: "4567",
        endereco : ObjectId("60bffa5a1484c00768943471"),
        consolidacao : ObjectId("60c002681484c00768943493"),
        contas: [ ObjectId("60c000df1484c00768943488"), ObjectId("60c000df1484c00768943489"), ObjectId("60c000df1484c0076894348a"), ObjectId("60c000df1484c0076894348b") ]
    }
]);
db.Agencia.find();


db.createCollection( "Banco" );
db.Banco.insert([
    {
        nome: "alfa",
        codigo: "001",
        agencias: [ ObjectId("60c0050a1484c00768943495"), ObjectId("60c0050a1484c00768943496"), ObjectId("60c0050a1484c00768943497") ]
    },
    {
        nome: "beta",
        codigo: "241",
        agencias: [ ObjectId("60c0050a1484c00768943498") ]
    },
    {
        nome: "omega",
        codigo: "307",
        agencias: [ ObjectId("60c0050a1484c00768943499"), ObjectId("60c0050a1484c0076894349a"), ObjectId("60c0050a1484c0076894349b") ]
    }
]);
db.Banco.find();
*/


/*
Em caso de necessidade de deletar uma das coleções

db.Pais.drop();
db.Estado.drop();
db.Cidade.drop();
db.Bairro.drop();
db.Endereco.drop();
db.Movimentacao.drop();
db.Cliente.drop();
db.Gerente.drop();
db.Conta.drop();
db.Consolidacao.drop();
db.Agencia.drop();
db.Banco.drop();
*/












