import React from 'react';
import Button from '../../Middler/Button';
import '../../../Container/css/base.css';
import { Link } from 'react-router-dom';


const Submit = ({ metodo, nome, link }) => 
  <React.Fragment>
    <Button  className="button" type="text" htmlType="submit" onClick={ metodo } >
      { link ? <Link to={ link } >{ nome }</Link> : nome }
    </Button>
  </React.Fragment>

export default Submit;