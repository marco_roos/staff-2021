import React from 'react';
import Card from '../Middler/Card'
import Meta from '../Middler/Meta'
import Avatar from '../Middler/Avatar'
import ButtonUI from '../ButtonUI';
import { AntDesignOutlined } from '@ant-design/icons';



const CardPacoteUi = ({ id, valor }) =>
  <React.Fragment >
    <Card className="card">
      <Meta
        avatar={
          <Avatar 
          size={{ xs: 24, sm: 32, md: 40, lg: 64, xl: 80, xxl: 100 }}
          icon={<AntDesignOutlined />} 
          />
        }
        title= {id}
        description={`valor: ${valor} R$`}
      />
      <ButtonUI
            nome={"Detalhes"}
            link={`/pacotes/${id}`}
          />
    </Card>
  </React.Fragment>

export default CardPacoteUi;