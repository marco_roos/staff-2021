import ButtonUI from '../ButtonUI';
import Form from '../Middler/Form';
import Input from '../Middler/Input';
import Submit from '../Middler/Submit';
import React, { useState } from 'react';
import 'antd/dist/antd.css';
import '../../Container/css/base.css';
import { Result } from 'antd';

const EspacoFormUI = ({ metodoCadastrar }) => {

  const [redirect, setRedirect] = useState(false);

  const onFinish = (values) => {
    metodoCadastrar(values);
    setRedirect(true);
  };

  return (
    (redirect === false) ?
      (<React.Fragment>
        <div className="form">
          <Form onFinish={onFinish}>
            <Form.Item label="Nome" name="nome">
              <Input />
            </Form.Item>
            <Form.Item label="Quantidade de pessoas" name="qtdPessoas">
              <Input />
            </Form.Item>
            <Form.Item label="Valor" name="valor">
              <Input />
            </Form.Item>
            <Submit nome={"Cadastrar"}/>
            <Form.Item>
            <ButtonUI nome={"Voltar"} link={"/cadastrar"} />
            </Form.Item>
          </Form>
        </div>
      </React.Fragment>)
      :
      (<React.Fragment>
        <Result
          status="success"
          title="Os dados foram corretamente enviados ao servidor"
        />
        <ButtonUI nome={"Voltar"} link={"/cadastrar"} />
      </React.Fragment>)
  )

}

export default EspacoFormUI;