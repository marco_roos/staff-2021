import React from 'react';
import "../../Container/css/base.css"



const Header = ({ page }) =>
  <React.Fragment>
    <div className="Header" >
      <h1>Coworking { page } &nbsp; &nbsp; [Marco Antônio M. Roos]</h1>
    </div>
  </React.Fragment>

export default Header;