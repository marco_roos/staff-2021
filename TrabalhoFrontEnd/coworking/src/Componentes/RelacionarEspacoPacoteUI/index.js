import React, { useState } from 'react';
import Input from '../Middler/Input';
import Submit from '../Middler/Submit';
import ButtonUI from '../ButtonUI';
import Select from '../Middler/Select';
import Option from '../Middler/Option';
import { Form, Result } from 'antd';


const RelacionarEspacoPacoteUI = ({ cadastrar }) => {

  const [redirect, setRedirect] = useState(false);

  const onFinish = (values) => {
    console.log(values)
    cadastrar(values);
    setRedirect(true);
  };

  return (
    (redirect === false) ?
      (<React.Fragment>
        <div className="form">
          <Form onFinish={onFinish}>
            <Form.Item label="Id Pacote" name="idPacote">
              <Input />
            </Form.Item>
            <Form.Item label="Id Espaço" name="idEspaco">
              <Input />
            </Form.Item>
            <Form.Item label="Tipo Contratacao" name="tipoContratacao">
              <Select placeholder="Selecione uma opção" allowClear>
                <Option value="minutos">Minutos</Option>
                <Option value="horas">Horas</Option>
                <Option value="turnos">Turnos</Option>
                <Option value="diarias">Diárias</Option>
                <Option value="semanas">Semanas</Option>
                <Option value="meses">Mensal</Option>
              </Select>
            </Form.Item>
            <Form.Item label="Quantidade" name="quantidade">
              <Input />
            </Form.Item>
            <Form.Item label="Prazo" name="prazo">
              <Input />
            </Form.Item>
            <Submit nome={"Relacionar"} />
            <Form.Item className="BotaoCadastrar-css">
              <ButtonUI nome={"Voltar"} link={"/cadastrar"} />
            </Form.Item>
          </Form>
        </div>
      </React.Fragment >)
      :
      (<React.Fragment>
        <Result status="success" title="Os dados foram corretamente enviados ao servidor" />
        <ButtonUI nome={"Voltar"} link={"/cadastrar"} />
      </React.Fragment>)
  )

}

export default RelacionarEspacoPacoteUI;