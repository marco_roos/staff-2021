import ButtonUI from '../ButtonUI';
import Form from '../Middler/Form';
import Input from '../Middler/Input';
import Submit from '../Middler/Submit';
import Select from '../Middler/Select'
import Option from '../Middler/Option'
import React, { useState } from 'react';
import { Result } from 'antd';

const ContratacaoEspacoUI = ({ cadastrar }) => {

    const [redirect, setRedirect] = useState(false);

    const onFinish = (values) => {
        console.log(values)
        cadastrar(values);
        setRedirect(true);
    };

    return (
        (redirect === false) ?
            (<React.Fragment>
                <div className="form">
                    <Form onFinish={onFinish}>
                        <Form.Item label="Id Espaco" name="idEspaco">
                            <Input />
                        </Form.Item>
                        <Form.Item label="Id Cliente" name="idCliente">
                            <Input />
                        </Form.Item>
                        <Form.Item label="Tipo Contratacao" name="tipoContratacao">
                            <Select
                                placeholder="Selecione uma opção"
                                allowClear
                            >
                                <Option value="minutos">Minutos</Option>
                                <Option value="horas">Horas</Option>
                                <Option value="turnos">Turnos</Option>
                                <Option value="diarias">Diárias</Option>
                                <Option value="semanas">Semanas</Option>
                                <Option value="meses">Mensal</Option>
                            </Select>
                        </Form.Item>
                        <Form.Item label="Quantidade" name="quantidade">
                            <Input />
                        </Form.Item>
                        <Form.Item label="Desconto" name="desconto">
                            <Input />
                        </Form.Item>
                        <Form.Item label="Prazo" name="prazo">
                            <Input />
                        </Form.Item>
                        <Submit nome={"Contratar"}/>
                        <Form.Item>
                            <ButtonUI nome={"Voltar"} link={"/contratar"}/>
                        </Form.Item>
                    </Form>
                </div>
            </React.Fragment >)
            :
            (<React.Fragment>
                <Result
                    status="success"
                    title="Os dados foram corretamente enviados ao servidor"
                />
                <ButtonUI nome={"Voltar"} link={"/contratar"} />
            </React.Fragment>)
    )

}

export default ContratacaoEspacoUI;