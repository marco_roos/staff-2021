import React from 'react';

import Card from '../Middler/Card';
import Meta from '../Middler/Meta';
import Avatar from '../Middler/Avatar';
import { UserOutlined } from '@ant-design/icons';
import '../../Container/css/base.css';


const CardClienteUI = ({ nome, id, cpf, dataNascimento, celular, email }) =>
  <React.Fragment>
    <Card className="card">
      <Meta
        avatar={
          <Avatar 
          size={{ xs: 24, sm: 32, md: 40, lg: 64, xl: 80, xxl: 100 }}
          icon={<UserOutlined />} 
          />
        }
        title= {nome}
        description={`id: ${id} 
cpf: ${cpf} 
data de nascimento: ${dataNascimento} 
celular: ${celular} 
email: ${email}`} 
      />
    </Card>
  </React.Fragment>

export default CardClienteUI;