import React from 'react';
import Card from '../Middler/Card';
import Meta from '../Middler/Meta'
import Avatar from '../Middler/Avatar'
import { AntDesignOutlined } from '@ant-design/icons';
import '../../Container/css/base.css';


const CardEspacoUI = ({ nome, id, valor, qtdPessoas }) =>
  <React.Fragment>
    <Card className="card">
      <Meta
        avatar={
          <Avatar 
          size={{ xs: 24, sm: 32, md: 40, lg: 64, xl: 80, xxl: 100 }}
          icon={<AntDesignOutlined />} 
          />
        }
        title= {nome}
        description={`id: ${id}
valor: ${valor} R$
quantidade de pessoas: ${qtdPessoas}`}
      />
    </Card>
  </React.Fragment>

export default CardEspacoUI;