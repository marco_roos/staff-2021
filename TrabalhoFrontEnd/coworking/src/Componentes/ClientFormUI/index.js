import React, { useState } from 'react';
import Form from '../Middler/Form';
import Input from '../Middler/Input';
import BotaoUI from '../ButtonUI';
import Submit from '../Middler/Submit';
import { DatePicker, Result } from 'antd';
import 'antd/dist/antd.css';
import '../../Container/css/base.css';



const ClientFormUI = ({ cadastrar }) => {

  const [redirect, setRedirect] = useState(false);

  const onFinish = (values) => {
    cadastrar(values);
    setRedirect(true);
  };

  return (
    (redirect === false) ?
      (<React.Fragment>
        <div className="form">
          <Form onFinish={onFinish}>
            <Form.Item tooltip="Nome completo do cliente é obrigatório!" required label="Nome" name="nome">
              <Input />
            </Form.Item>
            <Form.Item tooltip="CPF do cliente é obrigatório!" required label="CPF" name="cpf">
              <Input />
            </Form.Item>
            <Form.Item label="Email" name="email">
              <Input />
            </Form.Item>
            <Form.Item label="Telefone" name="telefone">
              <Input />
            </Form.Item>
            <Form.Item tooltip="Data de nascimento é obrigatório!" required label="Data de nascimento" name="dataNascimento">
              <DatePicker />
            </Form.Item>
            <Submit nome={"Cadastrar"} />
            <BotaoUI nome={"Voltar"} link={"/cadastrar"} />
          </Form>
        </div>
      </React.Fragment>) :
      (<React.Fragment>
        <Result
          status="success"
          title="Os dados foram corretamente enviados ao servidor"
        />
        <BotaoUI nome={"Voltar"} link={"/cadastrar"} />
      </React.Fragment>)
  )
}

export default ClientFormUI;