import { Result } from 'antd';
import React, { useState } from 'react';

import ButtonUI from '../ButtonUI';
import Form from '../Middler/Form';
import Input from '../Middler/Input';
import Submit from '../Middler/Submit';

const ContratacaoPacoteUI = ({ cadastrar }) => {

  const [redirect, setRedirect] = useState(false);

  const onFinish = (values) => {
    console.log(values)
    cadastrar(values);
    setRedirect(true);
  };

  return (
    (redirect === false) ?
      (<React.Fragment>
          <div className="form">
        <Form onFinish={onFinish}>
          <Form.Item label="Id Cliente" name="idCliente">
            <Input />
          </Form.Item>
          <Form.Item label="Id Pacote" name="idPacote">
            <Input />
          </Form.Item>
          <Form.Item label="Quantidade" name="quantidade">
            <Input />
          </Form.Item>
          <Submit nome={"Contratar"}/>
          <Form.Item>
          <ButtonUI nome={"Voltar"} link={"/contratar"}/>
          </Form.Item>
        </Form>
        </div>
      </React.Fragment >)
      :
      (<React.Fragment>
        <Result
            status="success"
            title="Os dados foram corretamente enviados ao servidor"
        />
        <ButtonUI nome={"Voltar"} link={"/contratar"} />
    </React.Fragment>)
  )

}

export default ContratacaoPacoteUI;