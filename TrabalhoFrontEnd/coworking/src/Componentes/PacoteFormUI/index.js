import React, { useState } from 'react';
import Form from '../Middler/Form';
import Input from '../Middler/Input';
import BotaoUI from '../ButtonUI';
import Submit from '../Middler/Submit';
import { Result } from 'antd';
import 'antd/dist/antd.css';
import '../../Container/css/base.css';



const PacoteFormUI = ({ metodoCadastrar }) => {

  const [redirect, setRedirect] = useState(false);

  const onFinish = (values) => {
    metodoCadastrar(values);
    setRedirect(true);
  };

  return (
    (redirect === false) ?
    ( <React.Fragment>
      <div className="form">
      <Form onFinish={onFinish}>
        <Form.Item label="Valor" name="valor">
          <Input />
        </Form.Item>
        <Submit nome={"Cadastrar"}/>
        <Form.Item className="BotaoCadastrar-css">
        <BotaoUI nome={"Voltar"} link={"/cadastrar"}/>
        </Form.Item>
      </Form>
      </div>
    </React.Fragment> )  :
      (<React.Fragment>
        <Result
          status="success"
          title="Os dados foram corretamente enviados ao servidor"
        />
        <BotaoUI nome={"Voltar"} link={"/cadastrar"} />
      </React.Fragment>)
  )
}

export default PacoteFormUI;