import React, { Component } from 'react';
import Header from '../../Componentes/HeaderUI';
import APIcoworking from '../../Models/APIcoworking';
import ContratacaoEspacoUI from '../../Componentes/ContratacaoEspacoUI';
import EspacoPersistente from '../../Models/EspacoPersistente';
import '../css/base.css';


export default class Cad_ContratacaoEspaco extends Component {

  constructor(props) {
    super(props);
    this.coworkingAPI = new APIcoworking();
  }

  contratar(contratacaoEspaco) {
    const requisicoes = [
      this.coworkingAPI.buscarEspaco(contratacaoEspaco.idEspaco),
      this.coworkingAPI.buscarCliente(contratacaoEspaco.idCliente)
    ]

    Promise.all(requisicoes)
      .then(requisicoes => {
        const contratar = new EspacoPersistente(requisicoes[0], requisicoes[1], contratacaoEspaco)
        this.coworkingAPI.contratarEspaco(contratar);
      })
  }

  render() {
    return (
      <React.Fragment>
        <div className="background" >
          <Header page={"Contratar Espaço"} />
          <ContratacaoEspacoUI
            cadastrar={this.contratar.bind(this)} />
        </div>
      </React.Fragment>
    );
  }
}
