import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import Home from '../Home';
import Cadastro from '../Cadastros';
import CadCliente from '../Cad_Cliente';
import CadEspaco from '../Cad_Espaco';
import CadPacote from '../Cad_Pacote';
import Busca from '../Buscar';
import ListaClientes from '../MostrarClientes';
import ListaEspacos from '../MostrarEspacos';
import ListaPacotes from '../MostrarPacotes';
import RegistrarAcesso from '../RegistrarAcesso';
import ListaAcessos from '../../Container/ListaAcessos';
import Contratar from '../Contratar';
import ContratarEspaco from '../Cad_ContratacaoEspaco';
import ContratarPacote from '../Cad_ContratacaoPacote';
import EspacoPacote from '../Rel_EspacoPacote';

export default class Rotas extends Component {
  render() {
    return (
      <Router>
        <Route path="/" exact component={ Home } />
        <Route path="/cadastrar" exact component={ Cadastro } />
        <Route path="/cadastrar/cliente" exact component={ CadCliente } />
        <Route path="/cadastrar/espaco" exact component={ CadEspaco } />
        <Route path="/cadastrar/pacote" exact component={ CadPacote } />
        <Route path="/cadastrar/espaco_pacote" exact component={ EspacoPacote } />
        <Route path="/contratar" exact component={ Contratar } />
        <Route path="/contratar/espaco" exact component={ ContratarEspaco } />
        <Route path="/contratar/pacote" exact component={ ContratarPacote } />
        <Route path="/buscar" exact component={ Busca } />
        <Route path="/buscar/clientes" exact component={ ListaClientes } />
        <Route path="/buscar/espacos" exact component={ ListaEspacos } />
        <Route path="/buscar/pacotes" exact component={ ListaPacotes } />
        <Route path="/NovoAcesso" exact component={ RegistrarAcesso } />
        <Route path="/buscar/acessos" exact component={ ListaAcessos } />
      </Router>
    );
  }
}
