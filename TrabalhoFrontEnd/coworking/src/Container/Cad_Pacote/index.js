import React, { Component } from 'react';
import Header from '../../Componentes/HeaderUI';
import PacoteFormUI from '../../Componentes/PacoteFormUI';
import APIcoworking from '../../Models/APIcoworking';
import '../css/base.css'


export default class Cad_Pacote extends Component {

    constructor(props){
        super(props)
        this.coworkingAPI = new APIcoworking();
      }
    
      cadastrarPacote( pacote ) {
        this.coworkingAPI.cadastrarPacote( pacote );
      }
    
    render() {
        return (
            <React.Fragment>
                <div className="background" >
                    <Header page={"Cadastrar Pacote"} />
                    <PacoteFormUI
                  metodoCadastrar={ this.cadastrarPacote.bind(this) }/>
                </div>
            </React.Fragment>
        );
    }
}
