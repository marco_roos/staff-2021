import React, { Component } from 'react';
import Header from '../../Componentes/HeaderUI';
import EspacoFormUI from '../../Componentes/EspacoFormUI'; 
import APIcoworking from '../../Models/APIcoworking';
import '../css/base.css';


export default class Cad_Espaco extends Component {

    constructor(props){
        super(props);
        this.coworkingAPI = new APIcoworking();
      }
    
      cadastrarEspaco( espaco ) {
        this.coworkingAPI.cadastrarEspaco( espaco );
      }

    render() {
        return (
            <React.Fragment>
                <div className="background" >
                    <Header page={"Cadastrar Espaço"} />
                    <EspacoFormUI
                  metodoCadastrar={ this.cadastrarEspaco.bind(this) }/>
                </div>
            </React.Fragment>
        );
    }
}
