import Header from '../../Componentes/HeaderUI';
import React from 'react';
import ButtonUI from '../../Componentes/ButtonUI';
import '../css/base.css'


function Busca() {
    return (
        <React.Fragment>
            <div className="background" >
            <Header page={"Buscar"}/>
                <ButtonUI nome={"ACESSOS"} link={"/buscar/#"} />
                <ButtonUI nome={"PAGAMENTOS"} link={"/buscar/#"} />
                <ButtonUI nome={"CLIENTES"} link={"/buscar/clientes"} />
                <ButtonUI nome={"PACOTES"} link={"/buscar/pacotes"} />
                <ButtonUI nome={"ESPAÇOS"} link={"/buscar/espacos"} />
                <ButtonUI nome={"VOLTAR"} link={"/"} />
            </div>
        </React.Fragment>
    );
}

export default Busca;