import Header from '../../Componentes/HeaderUI';
import React from 'react';
import ButtonUI from '../../Componentes/ButtonUI';
import '../css/base.css';


function Cadastros() {
    return (
        <React.Fragment>
            <div className="background" >
                <Header page={"Contratar"}/>
                <ButtonUI nome={"PACOTE"} link={"/contratar/pacote"} />
                <ButtonUI nome={"ESPAÇO"} link={"/contratar/espaco"} />
                <ButtonUI nome={"VOLTAR"} link={"/"} />
            </div>
        </React.Fragment>
    );
}

export default Cadastros;
