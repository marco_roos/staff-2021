import React, { Component } from 'react';
import ButtonUI from '../../Componentes/ButtonUI';
import CardEspacoUI from '../../Componentes/CardEspacoUI';
import Header from '../../Componentes/HeaderUI';
import APIcoworking from '../../Models/APIcoworking';
import ListaEspacos from '../../Models/ListaEspacos';
import '../css/base.css'


export default class MostrarEspacos extends Component {

    constructor(props) {
        super(props)
        this.coworkingApi = new APIcoworking();
        this.state = {}
    }

    componentDidMount() {
        this.coworkingApi.listarEspacos()
            .then(lista => {
                this.setState(state => { return { ...state, listaEspacos: new ListaEspacos(lista) } })
            })
    }

    render() {
        const { listaEspacos } = this.state;
        return (

            !listaEspacos ?
                (
                    <div className="background" >
                    <Header page={"Buscar Espaços"} />
                    <h2> Não foi possível encontrar espaços </h2>
                    </div>
                ) : (
                    <React.Fragment>
                        <div className="background" >
                            <Header page={"Buscar Espaços"} />
                            <div className="form">
                            {
                                listaEspacos.espacos && (
                                    listaEspacos.espacos.map((espaco, i) => {
                                        return <CardEspacoUI
                                        key={i}
                                        nome={espaco.nome}
                                        id={espaco.id}
                                        valor={espaco.valor}
                                        qtdPessoas={espaco.qtdPessoas}/>
                                    })
                                )
                            }
                            </div>
                            <ButtonUI nome={"Voltar"} link={"/buscar/"} />
                        </div>
                    </React.Fragment>
                )
        );
    }
}