import React from 'react';
import ButtonUI from '../../Componentes/ButtonUI'
import Header from '../../Componentes/HeaderUI'
import '../css/base.css'


function Home() {
    return (
        <React.Fragment>
            <div className="background" >
                <Header page={"Home"} />
                <ButtonUI nome={"BUSCAR"} link={"/buscar"} />
                <ButtonUI nome={"CADASTRAR"} link={"/cadastrar"} />
                <ButtonUI nome={"CONTRATAR"} link={"/contratar"} />
            </div>
        </React.Fragment>
    );
}

export default Home;