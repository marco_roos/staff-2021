import React, { Component } from 'react';
import '../../Models/APIcoworking';
import ClientFormUI from '../../Componentes/ClientFormUI';
import Header from '../../Componentes/HeaderUI';
import '../css/base.css';
import ClientePersistente from '../../Models/ClientePersistente';
import APIcoworking from '../../Models/APIcoworking';


export default class Cad_Cliente extends Component {

    constructor(props) {
        super(props);
        this.coworkingAPI = new APIcoworking();
    }

    cadastrarCliente(cliente) {
        let novoCliente = new ClientePersistente(cliente);
        this.coworkingAPI.cadastrarCliente(novoCliente);
    }

    render() {
        return (
            <React.Fragment>
                <div className="background" >
                    <Header page={"Cadastrar Cliente"} />
                    <ClientFormUI cadastrar={this.cadastrarCliente.bind(this)} />
                </div>
            </React.Fragment>
        );
    }
}
