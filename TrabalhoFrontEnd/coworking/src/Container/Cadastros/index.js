import Header from '../../Componentes/HeaderUI';
import React from 'react';
import ButtonUI from '../../Componentes/ButtonUI';
import '../css/base.css';


function Cadastros() {
    return (
        <React.Fragment>
            <div className="background" >
                <Header page={"Cadastrar"}/>
                <ButtonUI nome={"ACESSO"} link={"/cadastrar/#"} />
                <ButtonUI nome={"PAGAMENTO"} link={"/cadastrar/#"} />
                <ButtonUI nome={"CLIENTE"} link={"/cadastrar/cliente"} />
                <ButtonUI nome={"PACOTE"} link={"/cadastrar/pacote"} />
                <ButtonUI nome={"ESPAÇO"} link={"/cadastrar/espaco"} />
                <ButtonUI nome={"RELACIONAR ESPAÇO-PACOTE"} link={"/cadastrar/espaco_pacote"} />
                <ButtonUI nome={"VOLTAR"} link={"/"} />
            </div>
        </React.Fragment>
    );
}

export default Cadastros;
