import React, { Component } from 'react';
import ButtonUI from '../../Componentes/ButtonUI';
import CardPacoteUI from '../../Componentes/CardPacoteUI';
import Header from '../../Componentes/HeaderUI';
import APIcoworking from '../../Models/APIcoworking';
import ListaPacotes from '../../Models/ListaPacotes';
import '../css/base.css'


export default class MostrarEspacos extends Component {

    constructor(props) {
        super(props)
        this.coworkingApi = new APIcoworking();
        this.state = {}
    }

    componentDidMount() {
        this.coworkingApi.listarEspacos()
            .then(lista => {
                this.setState(state => { return { ...state, listaPacotes: new ListaPacotes(lista) } })
            })
    }

    render() {
        const { listaPacotes } = this.state;
        return (

            !listaPacotes ?
                (
                    <div className="background" >
                        <Header page={"Buscar Pacotes"} />
                        <h2> Não foi possível encontrar pacotes </h2>
                    </div>
                ) : (
                    <React.Fragment>
                        <div className="background" >
                            <Header page={"Buscar Pacotes"} />
                            <div className="form">
                                {
                                    listaPacotes.pacotes && (
                                        listaPacotes.pacotes.map((pacote, i) => {
                                            return <CardPacoteUI
                                                key={i}
                                                id={pacote.id}
                                                valor={pacote.valor} />
                                        })
                                    )
                                }
                            </div>
                            <ButtonUI nome={"Voltar"} link={"/buscar/"} />
                        </div>
                    </React.Fragment>
                )
        );
    }
}