import React, { Component } from 'react';
import ButtonUI from '../../Componentes/ButtonUI';
import CardClienteUI from '../../Componentes/CardClienteUI';
import Header from '../../Componentes/HeaderUI';
import APIcoworking from '../../Models/APIcoworking';
import ListaClientes from '../../Models/ListaClientes';
import '../css/base.css'


export default class MostrarClientes extends Component {

    constructor(props) {
        super(props)
        this.coworkingApi = new APIcoworking();
        this.state = {}
    }

    componentDidMount() {
        this.coworkingApi.listarClientes()
            .then(lista => {
                this.setState(state => { return { ...state, listaClientes: new ListaClientes(lista) } })
            })
    }

    render() {
        const { listaClientes } = this.state;
        return (

            !listaClientes ?
                (
                    <div className="background" >
                    <Header page={"Buscar Clientes"} />
                    <h2> Não foi possível encontrar clientes </h2>
                    </div>
                ) : (
                    <React.Fragment>
                        <div className="background" >
                            <Header page={"Buscar Clientes"} />
                            <div className="form">
                            {
                                listaClientes.clientes && (
                                    listaClientes.clientes.map((cliente, i) => {
                                        return <CardClienteUI
                                            key={i}
                                            nome={cliente.nome}
                                            id={cliente.id}
                                            cpf={cliente.cpf}
                                            dataNascimento={cliente.dataNascimento}
                                            email={cliente.email}
                                            celular={cliente.celular} />
                                    })
                                )
                            }
                            </div>
                            <ButtonUI nome={"Voltar"} link={"/buscar/"} />
                        </div>
                    </React.Fragment>
                )

        );
    }
}