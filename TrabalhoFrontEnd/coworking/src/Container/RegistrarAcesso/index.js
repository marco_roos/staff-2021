import Header from '../../Componentes/HeaderUI';
import React from 'react';
import '../css/base.css';


function RegistrarAcesso() {
    return (
        <React.Fragment>
            <div className="background" >
                <Header page={"Registro de Acesso"}/>
                Registrar acesso
            </div>
        </React.Fragment>
    );
}

export default RegistrarAcesso;
