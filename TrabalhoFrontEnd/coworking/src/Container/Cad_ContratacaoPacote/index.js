import React, { Component } from 'react';
import APIcoworking from '../../Models/APIcoworking';
import ContratacaoPacoteUI from '../../Componentes/ContratacaoPacoteUI';
import PacotePersistente from '../../Models/PacotePersitente';
import Header from '../../Componentes/HeaderUI';
import '../css/base.css';


export default class Cad_ContratacaoPacote extends Component {

    constructor(props) {
        super(props)
        this.coworkingAPI = new APIcoworking();
    }

    contratar(contratacaoPacote) {
        const requisicoes = [
            this.coworkingAPI.buscarCliente(contratacaoPacote.idCliente),
            this.coworkingAPI.buscarPacote(contratacaoPacote.idPacote)
        ]

        Promise.all(requisicoes)
            .then(requisicoes => {
                const contratar = new PacotePersistente(requisicoes[0], requisicoes[1], contratacaoPacote)
                this.coworkingAPI.contratarPacote(contratar);
            })
    }


    render() {

        return (
            <React.Fragment>
                <div className="background" >
                    <Header page={"Contratar Pacote"} />
                    <ContratacaoPacoteUI
                        cadastrar={this.contratar.bind(this)} />
                </div>
            </React.Fragment >
        );
    }
}