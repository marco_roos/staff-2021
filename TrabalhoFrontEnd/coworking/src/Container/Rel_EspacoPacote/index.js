import React, { Component } from 'react';
import Header from '../../Componentes/HeaderUI';
import APIcoworking from '../../Models/APIcoworking';
import RelacionarEspacoPacoteUI from '../../Componentes/RelacionarEspacoPacoteUI';
import EspacoPacotePersistente from '../../Models/EspacoPacotePersistente';
import '../css/base.css';


export default class Rel_EspacoPacote extends Component {

    constructor(props) {
        super(props);
        this.coworkingAPI = new APIcoworking();
    }

    Rel_EspacoPacote(espacoPacote) {
        const requisicoes = [
            this.coworkingAPI.buscarEspaco(espacoPacote.idEspaco),
            this.coworkingAPI.buscarPacote(espacoPacote.idPacote),
        ]

        Promise.all(requisicoes).then(requisicoes => {
            let espacoPacotePersistente = new EspacoPacotePersistente(requisicoes[0], requisicoes[1], espacoPacote);
            console.log(requisicoes[0])
            console.log(requisicoes[1])
            console.log(espacoPacotePersistente)
            this.coworkingAPI.relacionarEspacoAoPacote(espacoPacotePersistente);
        })
    }


    render() {
        return (
            <React.Fragment>
                <div className="background" >
                    <Header page={"Relacionar Espaço - Pacote"} />
                    <RelacionarEspacoPacoteUI
                        cadastrar={this.Rel_EspacoPacote.bind(this)} />
                </div>
            </React.Fragment>
        );
    }
}
