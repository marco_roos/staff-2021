import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Rotas from './Container/Rotas';


ReactDOM.render(
    <Rotas />,
  document.getElementById('root')
);

