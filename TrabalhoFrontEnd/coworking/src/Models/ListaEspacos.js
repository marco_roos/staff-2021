import Espaco from "./EspacoParaLista";

export default class ListaPacotes {
  constructor( lista ) {
    this.espacos = lista.map( espaco => new Espaco( espaco ) );
  }

}