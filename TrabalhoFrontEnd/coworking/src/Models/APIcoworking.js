import axios from 'axios';
import { getToken } from "../Security";
import { login } from '../Security';

const api = axios.create({
  baseURL: "http://localhost:8080/"
});

api.interceptors.request.use(async config => {
  const token = getToken();
  if (token) {
    config.headers.Authorization = `Bearer ${token}`;
  }
  return config;
});

export default class APIcoworking {

  async efetuarLogin({ username, password }) {
    const response = await api.post( "login", { username, password } );
    try{
      const headerAuthorization = response.headers.authorization.split(' ');
      const token = headerAuthorization[1];
      login(token);
    } catch( err ) {
      console.log(`Não foi possível fazer a autenticação.`);
    }
  }
  
  listarPacotes() {
    return api.get( `api/pacotes/` ).then( e => e.data );
  }

  buscarPacote( id ) {
    return api.get( `api/pacotes/${id}` ).then( e => e.data );
  }

  cadastrarPacote( pacote ) {
    return api.post( `api/pacotes/salvar`, pacote );
  }

  listarEspacos() {
    return api.get( `api/espaco/` ).then( e => e.data );
  }

  buscarEspaco( id ) {
    return api.get( `api/espaco/${id}` ).then( e => e.data );
  }

  cadastrarEspaco( espaco ) {
    return api.post( `api/espaco/salvar`, espaco );
  }

  listarClientes() {
    return api.get( `api/cliente/` ).then( e => e.data );
  }

  buscarCliente( id ) {
    return api.get( `api/cliente/${id}` ).then( e => e.data );
  }

  cadastrarCliente( cliente ) {
    return api.post( `api/cliente/salvar`, cliente );
  }

  relacionarEspacoAoPacote( espacoPacote ) {
    return api.post( `api/espaco_pacote/salvar`, espacoPacote );
  }

  buscarEspacoPacote( idPacote ) {
    return api.get( `api/espaco_pacote/pacote/${idPacote}`).then( e => e.data );
  }

  contratarEspaco( espaco ) {
    return api.post( `api/contratacao/salvar`, espaco );
  }

  contratarPacote( clientePacote ) {
    return api.post( `api/cliente_pacote/salvar`, clientePacote );
  }

  pagar( pagamento ) {
    return api.post( `api/pagamentos/salvar`, pagamento );
  }

  buscarContratacao( idContratacao ) {
    return api.get( `api/contratacao/${idContratacao}`).then( e => e.data );
  }

  buscarClientePacote( idClientePacote ) {
    return api.get( `api/cliente_pacote/${idClientePacote}`).then( e => e.data );
  }

  listarContratacoes() {
    return api.get( `api/contratacao/` ).then( e => e.data );
  }

  listarContratacoesPacote() {
    return api.get( `api/cliente_pacote/` ).then( e => e.data );
  }

  listarPagamentos() {
    return api.get( `api/pagamentos/` ).then( e => e.data );
  }

  registrarAcesso( acesso ) {
    return api.post( `api/acessos/salvar`, acesso );
  }

  listarAcessos() {
    return api.get( `api/acessos/` ).then( e => e.data );
  }

}