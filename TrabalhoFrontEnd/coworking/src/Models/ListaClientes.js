import ClienteParaLista from './ClienteParaLista';

export default class ListaPacotes {
  constructor( arrayDeClientes ) {
    this.clientes = arrayDeClientes.map( cliente => new ClienteParaLista( cliente ) );
  }

}