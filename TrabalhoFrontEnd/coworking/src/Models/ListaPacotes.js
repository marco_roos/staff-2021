import Pacote from "./PacoteParaLista";

export default class ListaPacotes {
  constructor( lista ) {
    this.pacotes = lista.map( pacote => new Pacote( pacote ) );
  }

}