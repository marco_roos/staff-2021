class Elfo {
    constructor( nome ) {
        this.nome = nome;
    }

    matarElfo() {
        this.status = "Morto";
    }

    get estaMorto() {
        return this.status = "Morto";
    }

    atacarComFlecha() {
        setTimeout(() => {
            console.log("Atacou");
        }, 2000);
    }

}

let legolas = new Elfo("Legolas");
legolas.matarElfo();
console.log(legolas.estaMorto);
legolas.atacarComFlecha();