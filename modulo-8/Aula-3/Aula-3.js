/** ===================== DESTRUCTION ======================== **/

let objeto = {
    nome : 'tonhão',
    idade : 26, 
    altura : 1.86
}

const { nome, altura } = objeto;

const arrayTeste = [ 'Gustavo', 'Kevin', 'Victor', 'Arthur' ];

let [, pos2,,pos4] = arrayTeste;

let a = 1;
let b = 3;

[a,b] = [b,a];

/** ===================== SPREAD OPERATOR ======================== **/

let arraySpread = [1, 77, 83, 42];
console.log( {...arraySpread} );
console.log( ...arraySpread );
console.log( ..."Meu Nome" );
console.log( [..."Meu Nome"] );