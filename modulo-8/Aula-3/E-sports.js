/*
Exercício 01

Vamos criar um sistema para uma liga local (não importa se é esporte ou e-Sports), 
na a primeira entrega que o cliente quer ver é a possibilidade de criar os times 
(nome, tipo de esporte, status, liga que joga), adicionar os jogadores, 
buscar os jogadores por nome e por número (todos jogadores devem ter um número de camiseta).
*/

class time {

    constructor ( nome, esport, status, liga ) {
        this._nome = nome;
        this._esport = esport;
        this._status = status;
        this._liga = liga;
        this._jogadores = [];
    }

    getNome() {
        return this._nome;
    }

    getEsport() {
        return this._esport;
    }

    getStatus() {
        return this._status;
    }

    getLiga() {
        return this._liga;
    }

    getJogadoresNome(nome) {
       return this._jogadores.find( jogador => jogador.nome == nome );
    }

    getJogadoresNumero(numero) {
        return this._jogadores.find( jogador => jogador.numero == numero );
    }

    setNome(nome) {
        this._nome = nome;
    }

    setEsport(esport) {
        this._esport = esport;
    }

    setStatus(status) {
        this._status = status;
    }

    setLiga(liga) {
        this._liga = liga;
    }

    addJogador(nome, numero) {
        this._jogadores.push( { nome, numero } );
    } 

}

let times = [];

times.push( new time( "lolzeros", "LOL", "Perdendo feio", "Losers" ) );
times[0].addJogador( "amdrezito", 5 );
times[0].addJogador( "Bruneco", 7 );
times[0].addJogador( "Calitos", 10 );

console.log( times[0].getNome() );
console.log( times[0].getEsport() );
console.log( times[0].getJogadoresNome("Bruneco") );
console.log( times[0].getJogadoresNumero(10) );