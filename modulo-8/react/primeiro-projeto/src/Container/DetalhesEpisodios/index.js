import React, { Component } from 'react';

import EpisodioApi from '../../models/EpisodioAPI';
import EpisodioUi from '../../Components/EpisodioUi';

export default class DetalhesEpisodios extends Component {
  constructor( props ) {
    super(props);
    this.episodioApi = new EpisodioApi();
    this.state = {
      detalhes: null
    }
  }

  componentDidMount() {
    const episodioId = this.props.match.params.id;
    const requisicoes = [
      this.episodioApi.buscarDetalhes( episodioId ),
      this.episodioApi.buscarNota( episodioId )
    ];

    Promise.all( requisicoes )
      .then( respostas => {
        this.setState( {
          detalhes: respostas[0],
          objNotas: respostas[1]
        })
      })
  }

  render() {
    const { episodio } = this.props.location.state;
    const { detalhes, objNotas } = this.state;

    return (
      <React.Fragment>
        <header className="App-header">
          <EpisodioUi episodio={ episodio }/>
          {
            detalhes && (
              <React.Fragment>
                <p>{ detalhes[0].sinopse }</p>
                <span>{ new Date( detalhes[0].dataEstreia ).toLocaleDateString() }</span>
                <span>IMDb: { detalhes[0].notaImdb * 0.5 }</span>
                <span>Sua Nota: { objNotas ? objNotas[0].nota : 'N/D' }</span>
              </React.Fragment>
            )
          } 
        </header>
      </React.Fragment>
    )
  }

}