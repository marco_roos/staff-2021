import React from 'react';
import { Link } from 'react-router-dom';

import BotaoUi from '../../Components/BotaoUi';
import ListaEpisodios from '../../Components/ListaEpisodios';

const ListaAvaliacoes = props => {
  const { listaEpisodios } = props.location.state;

  return (
    <React.Fragment>
      <BotaoUi classe="preto" link="/" nome="Página Inicial" />
      <ListaEpisodios listaEpisodios={ listaEpisodios.avaliados } />
    </React.Fragment>
  );
} 

export default ListaAvaliacoes;