import axios from 'axios';

const endereco = 'http://localhost:9000/api';

export default class EpisodioAPI {
  buscar() {
    return axios.get( `${ endereco }/episodios` ).then( e => e.data );
  }

  registrarNota( { nota, episodioId } ) {
    return axios.post( `${ endereco }/notas`, { nota, episodioId } );
  }

  buscarNota( id ) {
    return axios.get( `${ endereco }/notas?episodioId=${ id }` ).then( e => e.data );
  }

  buscarDetalhes( id ) {
    return axios.get( `${ endereco }/episodios/${ id }/detalhes` ).then( e => e.data );
  }

  buscarTodasNotas() {
    return axios.get( `${ endereco }/notas` ).then( e => e.data );
  }
}