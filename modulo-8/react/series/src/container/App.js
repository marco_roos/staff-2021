import { Component } from 'react';
import Series from '../models/Series';
import './App.css';

export default class App extends Component {
  constructor( props ) {
    super( props );
    this.listaSeries = new Series();
  }

  render() {
    console.log( this.listaSeries.invalidas() );
    console.log( this.listaSeries.filtrarPorAno( 2017 )  );
    console.log( this.listaSeries.procurarPorNome("Marcos")  );
    console.log( this.listaSeries.mediaDeEpisodios() );
    console.log( this.listaSeries.totalSalarios(0) );
    console.log( this.listaSeries.queroGenero("Caos"));
    console.log( this.listaSeries.queroTitulo("The"));
    console.log( this.listaSeries.creditos(0) );
    console.log( this.listaSeries.hashtag() );
    return (
      <div className="App">
        <h1> Séries </h1>
        <h2> Inválidas </h2>
      </div>
    );
  }
}
