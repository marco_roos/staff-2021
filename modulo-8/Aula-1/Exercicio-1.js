/** -----------------------------------------EXERCICIO 1--------------------------------------------- **/
let circulo = { 
    raio: 5,
    tipoCalculo:'A'
 };

function calcularCirculo( {raio, tipoCalculo} ) {
    return tipoCalculo == 'A' ? Math.PI*raio*raio : 2*raio*Math.PI;
}

console.log( "área e circunferência de 5" );
console.log( calcularCirculo( circulo ) );
circulo.tipoCalculo = 'C'
console.log( calcularCirculo( circulo ) );


/** -----------------------------------------EXERCICIO 2--------------------------------------------- **/

function naoBissexto( ano ) {
    return (ano % 400 == 0) || (ano % 4 == 0 && ano % 100 != 0) ? false : true;
}

console.log( "\nSe 2016 é bissexto, se 2017 é bissexto" );
console.log( naoBissexto(2016) );
console.log( naoBissexto(2017) );



/** -----------------------------------------EXERCICIO 3--------------------------------------------- **/

function somarPares( lista ) {
    let sum = lista[0];
    for( let i = 1; i < lista.length; i++ )
    {
        if( i % 2 == 0 ) {
            sum += lista[i];
        }
    }
    return sum;
}

console.log( "\nsoma de números que estejam em endereço par ( 2.5, 523, 2.5, 456, 2.5, 1562, 2.5 )" );
console.log( somarPares( [2.5, 523, 2.5, 456, 2.5, 1562, 2.5] ) );



/** -----------------------------------------EXERCICIO 4--------------------------------------------- **/

/* Minha Solução
function adicionar( a ) {
    return function ( b ) { return a + b; };
}
*/

/** Correção **/
let adicionar = valor1 => valor2 => valor1 + valor2;

console.log( "\nSoma diferentona ( 5 )( 5 )" );
console.log( adicionar( 5 )( 5 ) );



/** -----------------------------------------EXERCICIO 5--------------------------------------------- **/

/* Minha Solução
function formatar( valor ) {
    valor *= 100;
    let preciso = Math.ceil(valor).toFixed(2);
    preciso /= 100;
    str = "R$" + preciso.toLocaleString('en');
    return str;
}

console.log( "\nFormatação do valor ( 485456462.2458 )" );
console.log( formatar( 485456462.2458 ) );
*/

