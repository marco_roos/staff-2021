console.log("acessou o arquivo .js");

/** VAR **/

console.log(nome);
var nome = "Tony";
var nome = "Tony roos";
console.log(nome);

/** LET **/

let nome1 = "tony1";

    {
        let nome1 = "tony1 roos"
        console.log(nome1);
    }

    console.log(nome1);

/** CONST **/

const pessoa = {
    nome: "tony pessoa"
};

Object.freeze(pessoa);

pessoa.nome = "tony pessoa constantemente sendo alterada"

console.log(pessoa.nome);

/** COMO AS VARIÁVEIS FUNCIONAM **/

let soma = 1 + 2;
let soma1 = "1" + 2 + 3;
let soma2 = 1 + "2" + 3;
let soma3 = 1 + 2 + "3";

console.log(soma);
console.log(soma1);
console.log(soma2);
console.log(soma3);

/** FUNÇÕES **/

let nomeFuncao = "marco antônio";
let idadeFuncao = 31;
let semestre = 5;
let notas = [10.0, 3, 5, 9];


function funcaoCriarAluno( nomeFuncao, idadeFuncao, semestre, notas = [] ) {
    const aluno = {
        nome: nomeFuncao,
        idade:idadeFuncao,
        semestre:semestre,
        nota: notas
    };
    
    //Factory -> Design Pattern
    function aprovadoOrReprovado( notas ) {
        if( notas.length == 0 ){
            return "Sem Notas";
        }
    
        let somatoria = 0;
        for(let i = 0; i<notas.length; i++){
            somatoria += notas[i];
        }
    
        return ( somatoria / notas.length ) > 7.0 ? "Aprovado" : "Reprovado";
    }

    aluno.status = aprovadoOrReprovado( notas );
    console.log(aluno);
    return aluno;
}



funcaoCriarAluno( nomeFuncao, idadeFuncao, semestre, notas );