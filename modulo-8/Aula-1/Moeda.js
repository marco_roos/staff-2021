const moedas = ( function () {
    function imprimirMoeda( parametros ) {

        function arredondar( numero, precisao  = 2 ) {
            const fator = Math.pow( 10, precisao );
            return Math.ceil( numero * fator ) / fator;
        }

        const {
            numero,
            separadorMilhar,
            separadorDecimal,
            colocarMoeda,
            colocarNegativo
        } = parametros;

        const qtdCasasMilhares = 3;
        let StringBuffer = [];
        let parteQueDecimal = arredondar( Math.abs(numero) % 1 );
        let parteInteira = Math.trunc(numero);
        let parteInteiraString = Math.abs( parteInteira ).toString();
        let tamanhoParteInteira = parteInteiraString.length;

        let contador = 1;
        while(parteInteiraString.length > 0) {
            if( contador % qtdCasasMilhares == 0 && parteInteiraString.length > 3 ) {
                StringBuffer.push( `${ separadorMilhar }${ parteInteiraString.slice( tamanhoParteInteira - contador ) }` );
                parteInteiraString = parteInteiraString.slice( 0, tamanhoParteInteira - contador);
            } else if (parteInteiraString.length <= qtdCasasMilhares ){
                StringBuffer.push( parteInteiraString );
                parteInteiraString = '';
            }
            contador++;
        }
        StringBuffer.push( parteInteiraString );

        let decimalString = parteQueDecimal.toString().replace( '0.', '' ).padStart(2, 0);
        const numeroFormatado = `${StringBuffer.reverse().join('') }${separadorDecimal}${decimalString}`;

        return parteInteira >= 0 ? colocarMoeda(numeroFormatado) : colocarNegativo(numeroFormatado);
    }

    return {
        imprimirBRL: (numero) =>
            imprimirMoeda({
                numero,
                separadorDecimal: ',',
                separadorMilhar: '.',
                colocarMoeda: numeroFormatado => `R$ ${numeroFormatado}`,
                colocarNegativo: numeroFormatado => `- R$ ${numeroFormatado}`
            })
    };
} )()

console.log( "\nFormatação valor BRL" );
console.log( moedas.imprimirBRL( 3498.99 ) );
console.log( moedas.imprimirBRL( -3498.99 ) );
console.log( moedas.imprimirBRL( 2313477.0135 ) );