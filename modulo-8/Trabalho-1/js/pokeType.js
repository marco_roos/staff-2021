function typeColor( type ) {
  switch ( type ) {
    case 'bug':
      return 'lightgreen';
    case 'electric':
      return 'goldenrod';
    case 'fire':
      return 'orangered';
    case 'grass':
      return 'green';
    case 'normal':
      return 'gray';
    case 'rock':
      return 'sienna';
    case 'dark':
      return 'black';
    case 'fairy':
      return 'pink';
    case 'flying':
      return 'lightblue';
    case 'ground':
      return 'sienna';
    case 'poison':
      return 'purple';
    case 'steel':
      return 'grey';
    case 'dragon':
      return 'violet';
    case 'fighting':
      return 'orange';
    case 'ghost':
      return 'violetblue';
    case 'ice':
      return 'lightblue';
    case 'psychic':
      return 'palevioletred';
    case 'water':
      return 'blue';
    default:
      return null;
  }
}

class Poketype { // eslint-disable-line no-unused-vars
  refreshTypes( typeList ) {
    const ul = document.getElementById( 'typeList' );

    while ( ul.firstChild ) {
      ul.removeChild( ul.firstChild );
    }

    for ( let i = 0; i < typeList.length; i += 1 ) {
      const li = document.createElement( 'li' );
      li.className = 'pokeType';
      li.appendChild( document.createTextNode( typeList[i].type.name ) );
      li.setAttribute( 'id', `type${ i }` );
      ul.appendChild( li );
      li.style.backgroundColor = typeColor( typeList[i].type.name );
    }
  }
}
