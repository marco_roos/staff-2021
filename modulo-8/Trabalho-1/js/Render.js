class Render { // eslint-disable-line no-unused-vars
  constructor() {
    this._bars = new StatusBar(); // eslint-disable-line no-undef
    this._pokeTypes = new Poketype(); // eslint-disable-line no-undef
  }

  renderizar( pokemon ) {
    const dadosPokemon = document.getElementById( 'dadosPokemon' );
    const nome = dadosPokemon.querySelector( '.nome' );
    nome.innerHTML = pokemon.nome;

    const imagem = dadosPokemon.querySelector( '.thumb' );
    imagem.src = pokemon.imagem;

    const altura = dadosPokemon.querySelector( '.altura' );
    altura.innerHTML = pokemon.altura;

    const peso = dadosPokemon.querySelector( '.peso' );
    peso.innerHTML = pokemon.peso;

    const hp = dadosPokemon.querySelector( '.hp' );
    hp.innerHTML = pokemon.hp;

    const atk = dadosPokemon.querySelector( '.atk' );
    atk.innerHTML = pokemon.atk;

    const def = dadosPokemon.querySelector( '.def' );
    def.innerHTML = pokemon.def;

    const spAtk = dadosPokemon.querySelector( '.spAtk' );
    spAtk.innerHTML = pokemon.spAtk;

    const spDef = dadosPokemon.querySelector( '.spDef' );
    spDef.innerHTML = pokemon.spDef;

    const spd = dadosPokemon.querySelector( '.spd' );
    spd.innerHTML = pokemon.spd;

    this._bars.refreshAllBars( pokemon.hp,
      pokemon.atk,
      pokemon.def,
      pokemon.spAtk,
      pokemon.spDef,
      pokemon.spd );

    this._pokeTypes.refreshTypes( pokemon.typeList );
  }
}
