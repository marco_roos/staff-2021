package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Entity.Enum.Tipo_ContratacaoEnum;
import br.com.dbccompany.coworking.Entity.EspacoEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@Profile("test")
public class ContratacaoRepositoryTest {

    @Autowired
    private ContratacaoRepository repository;
    @Autowired
    private EspacoRepository repositoryEspaco;
    @Autowired
    private ClienteRepository repositoryCliente;

    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    /*@Test
    public void salvarContratacao() throws ParseException {
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("escritorio");
        espaco.setValor(20.00);
        espaco.setQtdPessoas(5);
        repositoryEspaco.save(espaco);
        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome("PedraDura");
        cliente.setCpf("12345678912");
        cliente.setData_nascimento(sdf.parse("25/09/2000"));
        repositoryCliente.save(cliente);
        //-----------//
        ContratacaoEntity contratacao = new ContratacaoEntity();
        contratacao.setEspaco(espaco);
        contratacao.setCliente(cliente);
        contratacao.setTipo_contratacao(Tipo_ContratacaoEnum.HORAS);
        contratacao.setQuantidade(5);
        contratacao.setDesconto(10.0);
        contratacao.setPrazo(5);
        repository.save(contratacao);
        //-----------//
        assertEquals(contratacao, repository.findAllByCliente(cliente).get(0));
        assertEquals(contratacao, repository.findAllByEspaco(espaco).get(0));
        assertEquals(contratacao, repository.findById(contratacao.getId()));
        assertEquals(contratacao, repository.findAllByClienteId(cliente.getId()).get(0));
        assertEquals(contratacao, repository.findAllByEspacoId(espaco.getId()).get(0));
    }*/

}
