package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.Enum.Tipo_ContratacaoEnum;
import br.com.dbccompany.coworking.Entity.EspacoEntity;
import br.com.dbccompany.coworking.Entity.Espacos_X_PacotesEntity;
import br.com.dbccompany.coworking.Entity.PacotesEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@Profile("test")
public class Espacos_X_PacotesRepositoryTest {

    @Autowired
    private Espacos_X_PacotesRepository repository;
    @Autowired
    private PacotesRepository repositoryPacote;
    @Autowired
    private EspacoRepository repositoryEspaco;

    @Test
    public void salvarEspacoXPacote(){
        PacotesEntity pacote = new PacotesEntity();
        pacote.setValor(200.00);
        repositoryPacote.save(pacote);
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("escritorio");
        espaco.setValor(20.00);
        espaco.setQtdPessoas(5);
        repositoryEspaco.save(espaco);
        Espacos_X_PacotesEntity espacoPacote = new Espacos_X_PacotesEntity();
        espacoPacote.setEspaco(espaco);
        espacoPacote.setPacote(pacote);
        espacoPacote.setTipo_contratacao(Tipo_ContratacaoEnum.DIARIAS);
        espacoPacote.setPrazo(5);
        espacoPacote.setQuantidade(2);
        repository.save(espacoPacote);
        assertEquals(espacoPacote, repository.findAllByEspacoId(espaco.getId()).get(0));
        assertEquals(espacoPacote, repository.findAllByPacoteId(pacote.getId()).get(0));
    }

}
