package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Entity.Tipo_ContatoEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@Profile("test")
public class ClienteRepositoryTest {

    @Autowired
    private ClienteRepository repository;
    @Autowired
    private Tipo_ContatoRepository repositoryTipoContato;

    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    @Test
    public void salvarCliente() throws ParseException {
        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome("PedraDura");
        cliente.setCpf("12345678912");
        cliente.setData_nascimento(sdf.parse("25/09/2000"));
        repository.save(cliente);
        assertEquals(cliente, repository.findByCpf(cliente.getCpf()));
    }

    @Test
    public void salvarClienteComContato() throws ParseException {
        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome("PedraDura");
        cliente.setCpf("12345678912");
        cliente.setData_nascimento(sdf.parse("25/09/2000"));
        ContatoEntity contato = new ContatoEntity();
        Tipo_ContatoEntity tipoContato = new Tipo_ContatoEntity();
        tipoContato.setNome("telefone");
        repositoryTipoContato.save(tipoContato);
        contato.setTipo_contato(tipoContato);
        contato.setValor("23451234");
        contato.setCliente(cliente);
        List<ContatoEntity> contatos = new ArrayList<>();
        contatos.add(contato);
        cliente.setContatos(contatos);
        repository.save(cliente);
        assertEquals(cliente, repository.findByContatos(contato) );
    }

}
