package br.com.dbccompany.coworking.Exception;

public class DadosInvalidos extends Exception{

    private String mensagem;

    public DadosInvalidos(String mensagem){
        this.mensagem = mensagem;
    }

    public String getMensagem() {
        return mensagem;
    }
}
