package br.com.dbccompany.coworking.DTO;

public class AcessoSaldoDTO {
    private String saldo;

    public String getSaldo() {
        return saldo;
    }

    public void setSaldo(String saldo) {
        this.saldo = saldo;
    }
}
