package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.Clientes_X_PacotesEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface Cliente_X_PacotesRepository extends CrudRepository<Clientes_X_PacotesEntity, Integer> {

    List<Clientes_X_PacotesEntity> findAll();
    Clientes_X_PacotesEntity findById(int id);
    List<Clientes_X_PacotesEntity> findAllByClienteId(int id_cliente);
    List<Clientes_X_PacotesEntity> findAllByPacoteId(int id_pacote);

}
