package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.ContatoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClienteRepository extends CrudRepository<ClienteEntity, Integer> {

    ClienteEntity findById(int id);
    ClienteEntity findByCpf(String cpf);
    ClienteEntity findByContatos(ContatoEntity contato);
    List<ClienteEntity> findAll();

}
