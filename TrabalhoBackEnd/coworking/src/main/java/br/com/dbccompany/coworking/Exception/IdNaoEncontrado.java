package br.com.dbccompany.coworking.Exception;

public class IdNaoEncontrado extends DadosInvalidos{

    public IdNaoEncontrado(){
        super("id nao encontrado");
    }

}
