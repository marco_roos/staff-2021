package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.Clientes_X_PacotesEntity;
import br.com.dbccompany.coworking.Entity.Espacos_X_PacotesEntity;
import br.com.dbccompany.coworking.Entity.PacotesEntity;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class PacotesDTO {

    private Integer id;
    private Double valor;
    //private List<Clientes_X_PacotesDTO> clientePacotes;
    //private List<Espacos_X_PacotesDTO> espacoPacotes;

    public PacotesDTO() {
    }

    public PacotesDTO(PacotesEntity pacote){
        this.id = pacote.getId();
        this.valor = pacote.getValor();
        //this.clientePacotes = converterListaParaDTOClientePacote( pacote.getClientesPacotes() );
        //this.espacoPacotes = converterListaParaDTOEspacoPacote( pacote.getEspacosPacotes() );
    }

    public PacotesEntity converter() throws ParseException {
        PacotesEntity pacote = new PacotesEntity();
        pacote.setId(this.id);
        pacote.setValor(this.valor);
        /*if(clientePacotes != null){
            pacote.setClientesPacotes(this.converterListaParaEntityClientePacote());
        }
        if (espacoPacotes != null){
            pacote.setEspacosPacotes(this.converterListaParaEntityEspacoPacote());
        }*/
        return pacote;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    /*public List<Clientes_X_PacotesDTO> getClientePacotes() {
        return clientePacotes;
    }

    public void setClientePacotes(List<Clientes_X_PacotesDTO> clientePacotes) {
        this.clientePacotes = clientePacotes;
    }

    public List<Espacos_X_PacotesDTO> getEspacoPacotes() {
        return espacoPacotes;
    }

    public void setEspacoPacotes(List<Espacos_X_PacotesDTO> espacoPacotes) {
        this.espacoPacotes = espacoPacotes;
    }*/

    private List<Clientes_X_PacotesDTO> converterListaParaDTOClientePacote(List<Clientes_X_PacotesEntity> lista){
        if (lista != null){
            List<Clientes_X_PacotesDTO> listaDTO = new ArrayList<>();
            if(!lista.isEmpty()){
                for(Clientes_X_PacotesEntity list : lista){
                    listaDTO.add(new Clientes_X_PacotesDTO(list));
                }
            }
            return listaDTO;
        }
        return null;
    }

    private List<Espacos_X_PacotesDTO> converterListaParaDTOEspacoPacote(List<Espacos_X_PacotesEntity> lista){
        if(lista != null) {
            List<Espacos_X_PacotesDTO> listaDTO = new ArrayList<>();
            if(!lista.isEmpty()){
                for(Espacos_X_PacotesEntity list : lista){
                    listaDTO.add(new Espacos_X_PacotesDTO(list));
                }
            }
            return listaDTO;
        }
        return null;
    }

    /*private List<Clientes_X_PacotesEntity> converterListaParaEntityClientePacote() throws ParseException {
        if(clientePacotes != null){
            List<Clientes_X_PacotesEntity> list = new ArrayList<>();
            for(Clientes_X_PacotesDTO listaDTO : clientePacotes){
                list.add(listaDTO.converter());
            }
            return list;
        }
        return null;
    }

    private List<Espacos_X_PacotesEntity> converterListaParaEntityEspacoPacote() throws ParseException {
        if(espacoPacotes != null) {
            List<Espacos_X_PacotesEntity> list = new ArrayList<>();
            for(Espacos_X_PacotesDTO listaDTO : espacoPacotes){
                list.add(listaDTO.converter());
            }
            return list;
        }
        return null;
    }*/

}
