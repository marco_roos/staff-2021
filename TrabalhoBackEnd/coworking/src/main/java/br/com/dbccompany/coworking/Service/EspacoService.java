package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.ClienteDTO;
import br.com.dbccompany.coworking.DTO.EspacoDTO;
import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.EspacoEntity;
import br.com.dbccompany.coworking.Exception.DadosInvalidos;
import br.com.dbccompany.coworking.Exception.IdNaoEncontrado;
import br.com.dbccompany.coworking.Exception.ObjetoNaoEncontrado;
import br.com.dbccompany.coworking.Repository.EspacoRepository;
import br.com.dbccompany.coworking.Util.LogUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class EspacoService {

    @Autowired
    private EspacoRepository repository;

    @Transactional( rollbackFor = Exception.class)
    public EspacoDTO salvar(EspacoEntity espaco ) throws DadosInvalidos {
        try{
            EspacoEntity espacoNovo = repository.save(espaco);
            return new EspacoDTO(espacoNovo);
        }catch (Exception e){
            LogUtil.lanca400(e);
            throw new DadosInvalidos(e.getMessage());
        }
    }

    @Transactional( rollbackFor = Exception.class)
    public EspacoDTO editar( EspacoEntity espaco, int id ) throws DadosInvalidos {
        try{
            espaco.setId(id);
            return new EspacoDTO(this.salvarEEditar( espaco ));
        }catch (Exception e){
            LogUtil.lanca400(e);
            throw new DadosInvalidos(e.getMessage());
        }
    }

    private EspacoEntity salvarEEditar( EspacoEntity espaco ){
        return repository.save( espaco );
    }

    public void delete(int id) throws IdNaoEncontrado {
        try{
            repository.deleteById(id);
        }catch (Exception e){
            LogUtil.lanca404(e);
            throw new IdNaoEncontrado();
        }
    }

    public EspacoDTO trazerPorId(int id) throws IdNaoEncontrado {
        try{
            return new EspacoDTO( repository.findById(id) );
        }catch (Exception e){
            LogUtil.lanca404(e);
            throw new IdNaoEncontrado();
        }
    }

    public EspacoDTO trazerPorNome(String nome) throws DadosInvalidos {
        try{
            return new EspacoDTO( repository.findByNome(nome) );
        }catch (Exception e){
            LogUtil.lanca404(e);
            throw new DadosInvalidos(e.getMessage());
        }
    }

    public List<EspacoDTO> trazerTodos() throws ObjetoNaoEncontrado {
        try{
            return converterListaParaDTO(repository.findAll());
        }catch (Exception e){
            LogUtil.lanca400(e);
            throw new ObjetoNaoEncontrado(e.getMessage());
        }
    }

    private List<EspacoDTO> converterListaParaDTO(List<EspacoEntity> espaco){
        List<EspacoDTO> listaDTO = new ArrayList<>();
        for(EspacoEntity espacoAux : espaco){
            listaDTO.add( new EspacoDTO(espacoAux) );
        }
        return listaDTO;
    }

}
