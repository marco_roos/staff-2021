package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class Clientes_X_PacotesEntity {

    @Id
    @SequenceGenerator( name = "CLIENTE_PACOTE_SEQ", sequenceName = "CLIENTE_PACOTE_SEQ")
    @GeneratedValue( generator = "CLIENTE_PACOTE_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne
    private ClienteEntity cliente;

    @ManyToOne
    private PacotesEntity pacote;

    @Column(nullable = false)
    private Integer quantidade;

    @OneToMany(mappedBy = "clientesPacotes")
    private List<PagamentosEntity> pagamento;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<PagamentosEntity> getPagamento() {
        return pagamento;
    }

    public void setPagamento(List<PagamentosEntity> pagamento) {
        this.pagamento = pagamento;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }

    public PacotesEntity getPacote() {
        return pacote;
    }

    public void setPacote(PacotesEntity pacote) {
        this.pacote = pacote;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }
}
