package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Service.ContatoService;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContatoRepository extends CrudRepository<ContatoEntity, Integer> {

    List<ContatoEntity> findAll();
    ContatoEntity findById(int id);
    List<ContatoEntity> findAllByValor(String valor);

}
