package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.Tipo_ContatoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface Tipo_ContatoRepository extends CrudRepository<Tipo_ContatoEntity, Integer> {

    Tipo_ContatoEntity findById(int id);
    Tipo_ContatoEntity findByNome(String nome);

}
