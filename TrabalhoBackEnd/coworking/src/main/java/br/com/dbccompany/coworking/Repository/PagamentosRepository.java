package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.PagamentosEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PagamentosRepository extends CrudRepository<PagamentosEntity, Integer> {

    List<PagamentosEntity> findAll();
    PagamentosEntity findById(int id);
    PagamentosEntity findByClientesPacotesId(int id_clietePacote);
    PagamentosEntity findByContratacaoId(int id_contratacao);

}
