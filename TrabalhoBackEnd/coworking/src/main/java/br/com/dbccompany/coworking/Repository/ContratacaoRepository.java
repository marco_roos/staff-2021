package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Entity.EspacoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContratacaoRepository extends CrudRepository<ContratacaoEntity, Integer> {

    List<ContratacaoEntity> findAll();
    ContratacaoEntity findById(int id);
    List<ContratacaoEntity> findAllByEspaco(EspacoEntity espaco);
    List<ContratacaoEntity> findAllByCliente(ClienteEntity cliente);
    List<ContratacaoEntity> findAllByEspacoId(int id_espaco);
    List<ContratacaoEntity> findAllByClienteId(int id_cliente);

}
