package br.com.dbccompany.coworking.Exception;

public class ObjetoNaoEncontrado extends Exception{

    private String mensagem;

    public ObjetoNaoEncontrado(String mensagem){
        super(mensagem);
    }

    public String getMensagem() {
        return mensagem;
    }
}
