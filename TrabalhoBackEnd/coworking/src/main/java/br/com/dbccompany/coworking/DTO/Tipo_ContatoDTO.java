package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.Tipo_ContatoEntity;

public class Tipo_ContatoDTO {

    private Integer id;
    private String nome;

    public Tipo_ContatoDTO() {
    }

    public Tipo_ContatoDTO(Tipo_ContatoEntity tipoContato){
        this.id = tipoContato.getId();
        this.nome = tipoContato.getNome();
    }

    public Tipo_ContatoEntity converter(){
        Tipo_ContatoEntity tipoContato = new Tipo_ContatoEntity();
        tipoContato.setId(this.id);
        tipoContato.setNome(this.nome);
        return tipoContato;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
