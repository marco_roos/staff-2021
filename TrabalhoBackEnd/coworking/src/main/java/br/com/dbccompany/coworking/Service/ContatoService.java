package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.ContatoDTO;
import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Exception.DadosInvalidos;
import br.com.dbccompany.coworking.Exception.IdNaoEncontrado;
import br.com.dbccompany.coworking.Exception.ObjetoNaoEncontrado;
import br.com.dbccompany.coworking.Repository.ContatoRepository;
import br.com.dbccompany.coworking.Util.LogUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class ContatoService {

    @Autowired
    private ContatoRepository repository;

    @Transactional( rollbackFor = Exception.class)
    public ContatoDTO salvar(ContatoEntity contato ) throws DadosInvalidos {
        try{
            ContatoEntity contatoNovo = repository.save(contato);
            return new ContatoDTO(contatoNovo);
        }catch (Exception e){
            LogUtil.lanca400(e);
            throw new DadosInvalidos(e.getMessage());
        }
    }

    @Transactional( rollbackFor = Exception.class)
    public ContatoDTO editar( ContatoEntity contato, int id ) throws DadosInvalidos {
        try{
            contato.setId(id);
            return new ContatoDTO(this.salvarEEditar( contato ));
        }catch (Exception e){
            LogUtil.lanca400(e);
            throw new DadosInvalidos(e.getMessage());
        }
    }

    private ContatoEntity salvarEEditar( ContatoEntity contato ){
        return repository.save( contato );
    }

    public void delete(int id) throws IdNaoEncontrado {
        try {
            repository.deleteById(id);
        }catch (Exception e){
            LogUtil.lanca404(e);
            throw new IdNaoEncontrado();
        }
    }

    public List<ContatoDTO> trazerTodos() throws ObjetoNaoEncontrado {
        try{
            return converterListaParaDTO(repository.findAll());
        }catch (Exception e){
            LogUtil.lanca400(e);
            throw  new ObjetoNaoEncontrado(e.getMessage());
        }
    }

    public ContatoDTO trazerPorId(int id) throws IdNaoEncontrado {
        try{
            return new ContatoDTO(repository.findById(id));
        }catch (Exception e){
            LogUtil.lanca404(e);
            throw  new IdNaoEncontrado();
        }
    }

    public List<ContatoDTO> trazerPorValor(String valor) throws DadosInvalidos {
        try{
            return converterListaParaDTO(repository.findAllByValor(valor));
        }catch (Exception e){
            LogUtil.lanca404(e);
            throw new DadosInvalidos(e.getMessage());
        }
    }

    private List<ContatoDTO> converterListaParaDTO(List<ContatoEntity> contato){
        List<ContatoDTO> listaDTO = new ArrayList<>();
        for(ContatoEntity contatoAux : contato){
            listaDTO.add( new ContatoDTO(contatoAux) );
        }
        return listaDTO;
    }
}
