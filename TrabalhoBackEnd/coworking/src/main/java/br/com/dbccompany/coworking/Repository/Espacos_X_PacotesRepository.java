package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.Espacos_X_PacotesEntity;
import br.com.dbccompany.coworking.Service.Espacos_X_PacotesService;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface Espacos_X_PacotesRepository extends CrudRepository<Espacos_X_PacotesEntity, Integer> {

    List<Espacos_X_PacotesEntity> findAll();
    Espacos_X_PacotesEntity findById(int id);
    List<Espacos_X_PacotesEntity> findAllByEspacoId(int id_espaco);
    List<Espacos_X_PacotesEntity> findAllByPacoteId(int id_pacote);

}
