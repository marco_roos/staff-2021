package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.PacotesDTO;
import br.com.dbccompany.coworking.Entity.PacotesEntity;
import br.com.dbccompany.coworking.Exception.DadosInvalidos;
import br.com.dbccompany.coworking.Exception.IdNaoEncontrado;
import br.com.dbccompany.coworking.Exception.ObjetoNaoEncontrado;
import br.com.dbccompany.coworking.Repository.PacotesRepository;
import br.com.dbccompany.coworking.Util.LogUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class PacotesService {

    @Autowired
    private PacotesRepository repository;

    @Transactional( rollbackFor = Exception.class)
    public PacotesDTO salvar(PacotesEntity pacote ) throws DadosInvalidos {
        try{
            PacotesEntity pacoteNovo = repository.save(pacote);
            return new PacotesDTO(pacoteNovo);
        }catch (Exception e){
            LogUtil.lanca400(e);
            throw new DadosInvalidos(e.getMessage());
        }
    }

    @Transactional( rollbackFor = Exception.class)
    public PacotesDTO editar( PacotesEntity pacote, int id ) throws DadosInvalidos {
        try{
            pacote.setId(id);
            return new PacotesDTO(this.salvarEEditar( pacote ));
        }catch (Exception e){
            LogUtil.lanca400(e);
            throw new DadosInvalidos(e.getMessage());
        }
    }

    private PacotesEntity salvarEEditar( PacotesEntity pacote ){
        return repository.save( pacote );
    }

    public void delete(int id) throws IdNaoEncontrado {
        try{
            repository.deleteById(id);
        }catch (Exception e){
            LogUtil.lanca404(e);
            throw new IdNaoEncontrado();
        }
    }

    public List<PacotesDTO> trazerTodos() throws ObjetoNaoEncontrado {
        try{
            return converterListaParaDTO( repository.findAll() );
        }catch (Exception e){
            LogUtil.lanca400(e);
            throw new ObjetoNaoEncontrado(e.getMessage());
        }
    }

    public PacotesDTO trazerPorId(int id) throws IdNaoEncontrado {
        try{
            return new PacotesDTO( repository.findById(id) );
        }catch (Exception e){
            LogUtil.lanca404(e);
            throw new IdNaoEncontrado();
        }
    }

    private List<PacotesDTO> converterListaParaDTO(List<PacotesEntity> pacotes){
        List<PacotesDTO> listaDTO = new ArrayList<>();
        for(PacotesEntity pacotesAux : pacotes){
            listaDTO.add( new PacotesDTO(pacotesAux) );
        }
        return listaDTO;
    }
}
