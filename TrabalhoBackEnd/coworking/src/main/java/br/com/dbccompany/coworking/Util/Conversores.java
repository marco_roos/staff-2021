package br.com.dbccompany.coworking.Util;

import br.com.dbccompany.coworking.Entity.Enum.Tipo_ContratacaoEnum;
import br.com.dbccompany.coworking.Entity.Enum.Tipo_PagamentoEnum;

public class Conversores {

    public static Tipo_ContratacaoEnum conversorTipoContratacaoParaEnum(String tipoContratacaoString){
        if(tipoContratacaoString.equals( "minutos" )){
            return Tipo_ContratacaoEnum.MINUTOS;
        }else if(tipoContratacaoString.equals( "horas" )){
            return Tipo_ContratacaoEnum.HORAS;
        }else if(tipoContratacaoString.equals( "turnos" )){
            return Tipo_ContratacaoEnum.TURNOS;
        }else if(tipoContratacaoString.equals( "diarias" )){
            return Tipo_ContratacaoEnum.DIARIAS;
        }else if(tipoContratacaoString.equals( "semanas" )){
            return Tipo_ContratacaoEnum.SEMANAS;
        }else if(tipoContratacaoString.equals( "meses" )){
            return Tipo_ContratacaoEnum.MESES;
        }
        return null;
    }

    public static String conversorTipoContratacaoParaString(Tipo_ContratacaoEnum tipoContratacaoEnum){
        if(tipoContratacaoEnum.equals(Tipo_ContratacaoEnum.MINUTOS)){
            return "minutos";
        }else if(tipoContratacaoEnum.equals(Tipo_ContratacaoEnum.HORAS)){
            return "horas";
        }else if(tipoContratacaoEnum.equals(Tipo_ContratacaoEnum.TURNOS)){
            return "turnos";
        }else if(tipoContratacaoEnum.equals(Tipo_ContratacaoEnum.DIARIAS)){
            return "diarias";
        }else if(tipoContratacaoEnum.equals(Tipo_ContratacaoEnum.SEMANAS)){
            return "semanas";
        }else if(tipoContratacaoEnum.equals(Tipo_ContratacaoEnum.MESES)){
            return "meses";
        }
        return null;
    }

    public static Tipo_PagamentoEnum conversorTipoPagamentoParaEnum(String tipoPagamentoString){
        if(tipoPagamentoString.equals( "debito" )){
            return Tipo_PagamentoEnum.DEBITO;
        }else if(tipoPagamentoString.equals( "credito" )){
            return Tipo_PagamentoEnum.CREDITO;
        }else if(tipoPagamentoString.equals( "dinheiro" )){
            return Tipo_PagamentoEnum.DINHEIRO;
        }else if(tipoPagamentoString.equals( "transferencia" )){
            return Tipo_PagamentoEnum.TRANSFERENCIA;
        }
        return null;
    }

    public static String conversorTipoPagamentoParaString(Tipo_PagamentoEnum tipoPagamentoEnum){
        if(tipoPagamentoEnum.equals(Tipo_PagamentoEnum.DEBITO)){
            return "debito";
        }else if(tipoPagamentoEnum.equals(Tipo_PagamentoEnum.CREDITO)){
            return "credito";
        }else if(tipoPagamentoEnum.equals(Tipo_PagamentoEnum.DINHEIRO)){
            return "dinheiro";
        }else if(tipoPagamentoEnum.equals(Tipo_PagamentoEnum.TRANSFERENCIA)) {
            return "transferencia";
        }
        return null;
    }

}
