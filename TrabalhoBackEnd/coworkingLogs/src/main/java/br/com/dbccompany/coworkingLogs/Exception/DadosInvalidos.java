package br.com.dbccompany.coworkingLogs.Exception;

public class DadosInvalidos extends LogException{

    public DadosInvalidos() {
        super("Dados inválidos, nao foi possivel realizar a operação!");
    }
}
