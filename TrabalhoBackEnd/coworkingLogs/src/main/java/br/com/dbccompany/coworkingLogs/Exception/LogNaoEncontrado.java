package br.com.dbccompany.coworkingLogs.Exception;

public class LogNaoEncontrado extends LogException{

    public LogNaoEncontrado(){
        super("Log nao foi encontrado");
    }

}
