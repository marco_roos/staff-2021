package br.com.dbccompany.coworkingLogs.Controller;

import br.com.dbccompany.coworkingLogs.DTO.LogDTO;
import br.com.dbccompany.coworkingLogs.Exception.DadosInvalidos;
import br.com.dbccompany.coworkingLogs.Exception.LogNaoEncontrado;
import br.com.dbccompany.coworkingLogs.Service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/logger")
public class LogController {

    @Autowired
    private LogService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<LogDTO> retornarTodos(){
        return service.trazerTodos();
    }

    @PostMapping( value = "/salvar" )
    @ResponseBody
    public ResponseEntity<LogDTO> salvar(@RequestBody LogDTO log ) {
        try {
            return new ResponseEntity<LogDTO>( service.insert(log.converter()), HttpStatus.ACCEPTED );
        } catch (DadosInvalidos e ) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping( value = "/codigo/{codigo}")
    @ResponseBody
    public ResponseEntity<List<LogDTO>> trazerTodosPorCodigo(@PathVariable String codigo){
        try {
            return new ResponseEntity<List<LogDTO>>( service.buscarTodosPorCodigo(codigo), HttpStatus.ACCEPTED );
        } catch (LogNaoEncontrado e){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping( value = "/tipo/{tipo}")
    @ResponseBody
    public ResponseEntity<List<LogDTO>> trazerTodosPorTipo(@PathVariable String tipo){
        try {
            return new ResponseEntity<List<LogDTO>>( service.buscarTodosPorTipo(tipo), HttpStatus.ACCEPTED );
        } catch (LogNaoEncontrado e){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

}
