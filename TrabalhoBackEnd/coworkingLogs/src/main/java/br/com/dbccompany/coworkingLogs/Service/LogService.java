package br.com.dbccompany.coworkingLogs.Service;

import br.com.dbccompany.coworkingLogs.Collection.LogCollection;
import br.com.dbccompany.coworkingLogs.DTO.LogDTO;
import br.com.dbccompany.coworkingLogs.Exception.DadosInvalidos;
import br.com.dbccompany.coworkingLogs.Exception.LogNaoEncontrado;
import br.com.dbccompany.coworkingLogs.Repository.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class LogService {

    @Autowired
    private LogRepository repository;

    public List<LogDTO> trazerTodos(){
        return converterListaParaDTO( repository.findAll() );
    }

    @Transactional( rollbackFor = Exception.class)
    public LogDTO insert(LogCollection log) throws DadosInvalidos {
        try {
            return new LogDTO(repository.insert(log));
        }catch (Exception e){
            System.err.println("falha ao buscar");
            throw new DadosInvalidos();
        }
    }

    public List<LogDTO> buscarTodosPorCodigo(String codigo) throws LogNaoEncontrado {
        try{
            return converterListaParaDTO(repository.findAllByCodigo(codigo));
        }catch (Exception e){
            System.err.println("falha ao buscar");
            throw new LogNaoEncontrado();
        }
    }

    public List<LogDTO> buscarTodosPorTipo(String tipo) throws LogNaoEncontrado {
        try{
            return converterListaParaDTO(repository.findAllByTipo(tipo));
        }catch (Exception e){
            System.err.println("falha ao buscar");
            throw new LogNaoEncontrado();
        }
    }

    private List<LogDTO> converterListaParaDTO(List<LogCollection> logs){
        ArrayList<LogDTO> logsNovo = new ArrayList<LogDTO>();
        for( LogCollection log : logs){
            logsNovo.add( new LogDTO( log ) );
        }
        return logsNovo;
    }
}
