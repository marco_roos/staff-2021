package br.com.dbccompany.coworkingLogs.DTO;

import br.com.dbccompany.coworkingLogs.Collection.LogCollection;

public class LogDTO {

    private String tipo;
    private String descricao;
    private String date;
    private String codigo;

    public LogDTO() {
    }

    public LogDTO(LogCollection log){
        this.tipo = log.getTipo();
        this.descricao = log.getDescricao();
        this.date = log.getDate();
        this.codigo = log.getCodigo();
    }

    public LogCollection converter(){
        LogCollection log = new LogCollection();
        log.setTipo(this.tipo);
        log.setDescricao(this.descricao);
        log.setDate(this.date);
        log.setCodigo(this.codigo);
        return log;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
}
